<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Controller {

	private static $instance;
	
	/**
	 * Constructor
	 */

	

	public function __construct()
	{
		self::$instance =& $this;
		
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");
	}

	public static function &get_instance()
	{
		return self::$instance;
	}

	public function isAdmin() {
		$this->load->library('session');    
		$admin = $this->session->userdata('admin');
        
        if (is_object($admin)) {
        	return true;
        } else {
			$this->load->helper(array('url'));
			redirect('admin/login', 'refresh');

        }
	}

	public function isLoggedIn($forceLogin = TRUE) {
		$this->load->library('session');    
		$member = $this->session->userdata('member');
        
        if (is_object($member)) {
        	return true;
        } else {
        	if ($forceLogin) {
				$this->load->helper(array('url'));
				redirect('member/register', 'refresh');
			} else {
				return false;
			}
        }
	}	

}
