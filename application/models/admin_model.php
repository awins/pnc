<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Admin_model extends CI_Model_Mod {

    

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'admins';
        $this->scheme = array(
            'id',
            'user_name',
            'password'
        );
    }
    
    
    function get_list($page = 0,$row_per_page = 0)
    {
        
        if ($page == 0 ) {
            $this->db->order_by("id", "desc");
            $query = $this->db->get($this->table);
            return $query->result();
        } else {
            /*$this->load->library(‘pagination’);
            $config['base_url']="http://127.0.0.1/codeigniter/index.php/users/display/";
            $config['total_rows']=$this->Users->getNumUsers();
            $config['per_page']=’5′;
            $this->pagination->initialize($config); */

        }
    }

   
    function login($data) {
        $this->db->where('user_name',$data['user_name']);
        $this->db->where('password',$data['password']);
        $query = $this->db->get($this->table);
        return $query->result();
    }

}

?>