<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Member_model extends CI_Model_Mod {


    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'members';
        $this->scheme = array(
            'id',
            'member_name',
            'address',
            'city',
            'telp',
            'email',
            'user_password',
            'is_active',
            'valid_code'
        );

    }
    
    
    /*function get_list($page = 0,$row_per_page = 0)
    {
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->table);
        return $query->result();
        
    }*/

   
    function login($data) {
        $option = array();
        $option['where'] = array('email' => $data['email'],'user_password' => $data['password'] );
        return $this->get_list($option);

    }

}

?>