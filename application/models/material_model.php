<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Material_model extends CI_Model_Mod {

    var $row_per_page = 20;

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'materials';
        $this->scheme = array(
            'id',
            'brand_id',
            'article_id',
            'color',
            'size',
            'stock',
            'status',
            'reg_price',
            'disc_price',
            'display_gallery',
            'display_pnc',
            'display_mixmatch',
            'display_suggestion',
            'gallery_images',
            'mixmatch_images',
            'suggestion_images',
            'barcode',
            'tematic',
            'price_mode',
            'is_new',
            'is_latest'
        );

    }
    
    
    function get_list($page = 0)
    {
        
        $this->db->select('materials.*,brands.brand_name,articles.title,articles.code,items.item_name,categories.category_name');
        $this->db->from($this->table);
        $this->db->join('brands', 'materials.brand_id = brands.id');
        $this->db->join('articles', 'materials.article_id = articles.id');
        $this->db->join('items', 'articles.item_id = items.id');
        $this->db->join('categories', 'items.category_id = categories.id');
        $this->db->order_by("materials.id", "desc");
        $this->db->limit(20, ($page - 1) * 20);

        $query = $this->db->get();
        return $query->result();
    }

    function get_tematic_list(){
        $this->db->distinct();
        $this->db->select('tematic');
        $this->db->from($this->table);

        $query = $this->db->get();
        return $query->result();

    }


    function get_list_by_page($page_type = NULL, $category_id = 0, $filter_display = 1, $front_page = true , $item_id = 0 )
    {
        
        $this->db->select('materials.*,brand_name,title,code,spec,other_info,item_name,category_id,category_name');
        $this->db->from($this->table);
        $this->db->join('brands', 'materials.brand_id = brands.id');
        $this->db->join('articles', 'materials.article_id = articles.id');
        $this->db->join('items', 'articles.item_id = items.id');
        $this->db->join('categories', 'items.category_id = categories.id');
        $this->db->order_by("categories.id", "asc");

        switch ($page_type) {
            case 'gallery':
                    switch ($filter_display) {
                        case 1 :
                            $this->db->where("materials.display_gallery", 1);
                            break;
                        case 0:
                            $this->db->where("materials.display_gallery <>", 1);
                            break;
                        default:
                            break;
                    }

                    if ($category_id != 0) {
                        $this->db->where("categories.id", $category_id);
                    }

                    if ($item_id != 0) {
                        $this->db->where("items.id", $item_id);
                    }

                break;
            case 'pncOnly':
                    switch ($filter_display) {
                        case 1 :
                            $this->db->where("materials.display_pnc", 1);
                            break;
                        case 0:
                            $this->db->where("materials.display_pnc <>", 1);
                            break;
                        default:
                            break;
                    }

                    if ($category_id != 0) {
                        $this->db->where("categories.id", $category_id);
                    }
                break;
            case 'tematic':
                    
                    $this->db->where("materials.tematic", $category_id);
                    break;

            case 'mixmatch':
                    switch ($filter_display) {
                        case 1 :
                            $this->db->where("materials.display_mixmatch", 1);
                            break;
                        case 0:
                            $this->db->where("materials.display_mixmatch <>", 1);
                            break;
                        default:
                            break;
                    }

                    if ($category_id != 0) {
                        if ($front_page) {
                            if ($category_id == 8 ) {
                                $this->db->where("categories.id", 8 );
                            } else {
                                $this->db->where("categories.id <", 8 );
                            }
                        } else {
                            $this->db->where("categories.id", $category_id);
                        }
                    }
                break;    
            case 'suggestion':
                    switch ($filter_display) {
                        case 1 :
                            $this->db->where("materials.display_suggestion", 1);
                            break;
                        case 0:
                            $this->db->where("materials.display_suggestion <>", 1);
                            break;
                        default:
                            break;
                    }

                    $this->db->where("categories.id", 5);
                
                break;                
            default:
                # code...
                break;
        }


        $result = array();
        
        $query = $this->db->get();
        
        $rows_count = $query->num_rows();
        $result['rows_count'] = $rows_count;

        $pages_count = intval($rows_count / $this->row_per_page);
        
        if ($rows_count % $this->row_per_page > 0 ) {
            $pages_count++;
        }
        $result['pages_count'] = $pages_count;

        $result['data'] = $query->result();

        return $result;
    }

    function get_list_by_category($category_id)
    {
        
            $this->db->select('materials.*,brands.brand_name,articles.title,articles.code,items.item_name,categories.category_name');
            $this->db->from('materials');
            $this->db->join('brands', 'materials.brand_id = brands.id');
            $this->db->join('articles', 'materials.article_id = articles.id');
            $this->db->join('items', 'articles.item_id = items.id');
            $this->db->join('categories', 'items.category_id = categories.id');
            if ($category_id > 0) {
                $this->db->where("categories.id", $category_id);
            }
            $this->db->order_by("materials.id", "desc");
            $query = $this->db->get();
            return $query->result();
    }

    function get_basic_by_id($id) {
        $this->db->where('id' , $id );
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result[0];
    }

    function get_by_id($id) {
        $this->db->select('materials.*,brands.brand_name,articles.title,articles.code,articles.other_info,items.item_name,items.category_id, categories.category_name');
            $this->db->from('materials');
            $this->db->join('brands', 'materials.brand_id = brands.id');
            $this->db->join('articles', 'materials.article_id = articles.id');
            $this->db->join('items', 'articles.item_id = items.id');
            $this->db->join('categories', 'items.category_id = categories.id');
            $this->db->where("materials.id", $id);
            $query = $this->db->get();
            $query = $query->result();
            if (!$query) {
                return false ;
            }
            $query = $query[0];

            $this->load->helper('file');

            $img_path = IMG_PATH  . 'materials/' ;
            $img_dir = IMG_DIR  . 'materials/' ;


            //$rand =  rand();
            // GALLERY THUMBNAIL
            $file = 'T_G_' . $query->id . '.png'; 
            if (file_exists($img_dir . $file) )
            {
                $query->gallery_thumb = $img_path . $file ;//.'?' . $rand;
            } else {
                $query->gallery_thumb = NULL;
            }

            // GALLERY DISPLAY FRONT
            $file = 'F_G_' . $query->id . '.png'; 
            if (file_exists($img_dir . $file) )
            {
                $query->gallery_front = $img_path . $file;// .'?' . $rand;
            } else {
                $query->gallery_front = NULL;
            }

            // GALLERY DISPLAY REAR
            $file = 'R_G_' . $query->id . '.png'; 
            if (file_exists($img_dir . $file))
            {
                $query->gallery_rear = $img_path . $file;// .'?' . $rand;
            } else {
                $query->gallery_rear = NULL;
            }

            // MIXMATCH THUMBNAIL
            $file = 'T_M_' . $query->id . '.png'; 
            if (file_exists($img_dir . $file))
            {
                $query->mixmatch_thumb = $img_path . $file;// .'?' . $rand;
            } else {
                $query->mixmatch_thumb = NULL;
            }

            // MIXMATCH DISPLAY FRONT
            $file = 'F_M_' . $query->id . '.png'; 
            if (file_exists($img_dir . $file))
            {
                $query->mixmatch_front = $img_path . $file ;//.'?' . $rand;
            } else {
                $query->mixmatch_front = NULL;
            }

            // MIXMATCH DISPLAY REAR
            $file = 'R_M_' . $query->id . '.png'; 
            if (file_exists($img_dir . $file))
            {
                $query->mixmatch_rear = $img_path . $file ;//.'?' . $rand;
            } else {
                $query->mixmatch_rear = NULL;
            }

            // SUGGESTION
            $suggestion = $query->suggestion_images;
            $arr_suggestion = array();

            if (strlen($suggestion) > 0) {
                $arr_images = explode(";", $suggestion);
                foreach ($arr_images as $value) {
                    if (file_exists($img_dir . $value))
                    {
                        $arr_suggestion[] = $img_path . $value ;//.'?' . $rand;
                    }
                }

            }
            $query->suggestions = $arr_suggestion;

            $data[] = $query;

            return $data;
    }

    

    function getSizeRange($material_id) {
        $sql = "SELECT DISTINCT size FROM materials WHERE size <> '-' and article_id = (SELECT article_id FROM materials WHERE id = ?)";
        $query = $this->db->query($sql, array($material_id)); 
        return $query->result();
    }

    function getColorRange($material_id) {
        /*$this->db->select('materials.*,brand_name,title,code,spec,other_info,item_name,category_id,category_name');
        $this->db->from($this->table);
        $this->db->join('brands', 'materials.brand_id = brands.id');
        $this->db->join('articles', 'materials.article_id = articles.id');
        $this->db->join('items', 'articles.item_id = items.id');
        $this->db->join('categories', 'items.category_id = categories.id');
        $this->db->order_by("categories.id", "asc");
        $this->db->where("materials.article_id = (select article_id from materials where id =" . $material_id  . ")") ;*/

        $sql = 
        " select materials.*,brand_name,title,`code`,spec,other_info,item_name,category_id,category_name " .
        "FROM ".                   
        "(SELECT materials.id,".
        "materials.brand_id".
        ", categories.id `cat_id`".
        " FROM".
        "  materials" .                                                               
        "  INNER JOIN brands ".
        "      ON (materials.brand_id = brands.id)".
        "  INNER JOIN articles ".
        "      ON (materials.article_id = articles.id)".
        "  INNER JOIN items".
        "  ON (articles.item_id = items.id)".
        "  INNER JOIN categories ".
        "      ON (items.category_id = categories.id)".
        "  WHERE materials.id = " . $material_id . ") p,".
        "  materials".
        "  INNER JOIN brands ".
        "      ON (materials.brand_id = brands.id)".
        "  INNER JOIN articles ".
        "      ON (materials.article_id = articles.id)".
        "  INNER JOIN items".
        "  ON (articles.item_id = items.id)".
        "  INNER JOIN categories ".
        "      ON (items.category_id = categories.id)".
        "   WHERE materials.brand_id = p.brand_id AND categories.id = p.cat_id";

       $query = $this->db->query($sql);
//        $this->db->get($this->table);

        $result = array();
        
        //$query = $this->db->get();
        
        $rows_count = $query->num_rows();
        $result['rows_count'] = $rows_count;

        $result['data'] = $query->result();

        return $result;
    }

    function get_latest_items($limit) {
        $this->db->where('is_latest' , 1);
        $this->db->limit($limit,0);
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
}

?>