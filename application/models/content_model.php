<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Content_model extends CI_Model_Mod {


    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'contents';
        $this->scheme = array(
            'id',
            'section',
            'date',
            'title',
            'home_view',
            'full_content',
            'active'
        );

    }
    
    
    function get_list($type ='ALL')
    {
        if ($type != "ALL") {
            $this->db->where('type', $type );
        }
        $this->db->order_by("date", "desc");
        $query = $this->db->get($this->table);
        return $query->result();
    }

   

    function get_active_item($section = NULL) {
        $this->db->where("active", 1);
        if ($section){
            $this->db->where("section", $section);
        }
        $query = $this->db->get($this->table);
        return $query->result();   
    } 

    function save($data) {
        $arr = $this->getScheme($data);
        if (isset($arr['active'])) {
            $this->db->update($this->table, array('active' => 0 ), array('section' => $arr['section']));
        }
        if (isset($arr['id'])) {
            return $this->db->update($this->table, $arr, array('id' => $arr['id']));
        } else {
            return $this->db->insert($this->table,$arr);
        }
    }

}

?>