<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Contact_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'contacts';
        $this->scheme = array(
            'id',
            'input_dt',
            'name',
            'email',
            'message'
        );
    }

}

?>