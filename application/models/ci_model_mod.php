<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Model_Mod extends CI_Model {


	var $table = NULL;
    var $scheme = array();

	public function getScheme($data) {
		$arr = array();
		foreach ($data as $key => $value) {
            if (in_array($key, $this->scheme)) {
            	$arr[$key] = $value;
            }
        }
        return $arr;
	}

    
    function getCount($option = array()) {
        $this->db->select('*');
        $this->db->from($this->table);
        
        if (isset($option['where'])) {
            foreach ($option['where'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }   

         $query = $this->db->get();
         return $query->num_rows();

    }

	function save($data) {
        $arr = $this->getScheme($data);
        if (isset($arr['id'])) {
            $this->db->update($this->table, $arr, array('id' => $arr['id']));
            return $arr['id'];
        } else {
            $this->db->insert($this->table,$arr);
            return $this->db->insert_id();
        }
    }

    function get_list($option = array()) {
        if (isset($option['select'])) {
            $this->db->select($option['select']);
        } else {
            $this->db->select('*');
        }

        $this->db->from($this->table);
        
        if (isset($option['join'])) {
            foreach ($option['join'] as $key => $value) {
                $this->db->join($key, $value);
            }
        }   
		
		if (isset($option['where'])) {
        	foreach ($option['where'] as $key => $value) {
        		$this->db->where($key,$value);
        	}
        }	

        if (isset($option['order_by'])) {
        	$this->db->order_by($option['order_by']);
        }

        if (isset($option['limit'])) {
            $limit = $option['limit'];
            $page = $limit;

            if (isset($option['page'])) {
                $page = ($option['page'] - 1) * $limit;
            } 
            $this->db->limit($limit,$page);

        }
		        

        
        $query = $this->db->get();
        return $query->result();
        
    }

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id',$id);
        
        $query = $this->db->get();
        $query = $query->result();

        if (count($query) > 0)
        {
            return $query[0];
        } else {
            return NULL;
        }
        
    }

}