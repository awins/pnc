<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Review_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'product_reviews';
        $this->scheme = array(
            'id',
            'member_id',
            'material_id',
            'date',
            'rate',
            'comment'
        );
    }

}

?>