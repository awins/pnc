<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Article_model extends CI_Model_Mod {

    
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'articles';
        $this->scheme = array(
            'id',
            'item_id',
            'title',
            'code',
            'spec',
            'other_info'
        );

    }
    
    
    function get_list($page = 0)
    {
        
        if ($page == 0 ) {
            
            
            $this->db->select('articles.*,items.item_name,categories.category_name');
            $this->db->from('articles');
            $this->db->join('items', 'articles.item_id = items.id');
            $this->db->join('categories', 'items.category_id = categories.id');
            $this->db->order_by("title", "asc");
            $query = $this->db->get();
        } else {
            
            $this->db->select('articles.*,items.item_name,categories.category_name');
            $this->db->from('articles');
            $this->db->join('items', 'articles.item_id = items.id');
            $this->db->join('categories', 'items.category_id = categories.id');
            $this->db->order_by("articles.id", "desc");
            $this->db->limit(20, ($page - 1) * 20);
            $query = $this->db->get();
        }
            return $query->result();
    }

    

    

}

?>