<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Item_model extends CI_Model_Mod {


    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'items';
        $this->scheme = array(
            'id',
            'category_id',
            'item_name'
        );

    }
    
    
    function get_list($page = 0,$row_per_page = 0)
    {
        
        $this->db->select('items.*,categories.category_name');
        $this->db->from('items');
        $this->db->join('categories', 'items.category_id = categories.id');
        $this->db->order_by("categories.id", "asc");
        if ($page == 0 ) {
        } else {
            $this->db->limit(20, ($page - 1) * 20);
        }
            $query = $this->db->get();
            return $query->result();
    }

    

}

?>