<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Sale_model extends CI_Model_Mod {

    var $table_detail = 'sale_items';

    var $scheme_detail = array(
            'id',
            'sales_id',
            'quantity',
            'total_amount',
            'material_id',
            'notes'
        );

    var $status = array(
            'open' => 0,
            'sale_complete' => 1,
            'payment_complete' => 2, 
            'on_delivery' => 3, 
            'received' => 4, 
            'payment_failed' => 5, 
            'delivery_failed' => 6 
        );
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        $this->table = 'sales';
        $this->scheme = array(
            'id',
            'sale_id',
            'payment_code',
            'amount',
            'return_url',
            'member_id',
            'status',
            '195_reff',
            'payment_source',
            'notes',
            'created_dt'
        );
    }
    
    
    function get_by_member($member_id,$status = 'open') {
        $this->db->where("member_id", $member_id);
        $this->db->where("status", $this->status[$status]);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    function get_by_id($id,$status = 'open') {
        $this->db->select('sales.*, sale_items.quantity, sale_items.total_amount, sale_items.material_id, sale_items.notes ');
        $this->db->from($this->table);
        $this->db->join('sale_items', 'sale_items.sales_id = sales.id','left');
        $this->db->where("sales.id", $id);
        if (!is_null($status)) {
            $this->db->where("status", $this->status[$status]);
        }
        $query = $this->db->get();
        return $query->result();
    }





    function save($data,$withDetail = true) {
        $arr = $this->getScheme($data);
        $ret = NULL;
        if (isset($arr['id'])) {
            $this->db->update($this->table, $arr, array('id' => $arr['id']));
        } else {
            $this->db->insert($this->table,$arr);
            $arr['id'] = $this->db->insert_id();
            $ret = $arr['id'];
        }

        if ($withDetail) {
            $amount = 0;
            foreach ($data['detail'] as $value) {
                $value['sales_id'] = $arr['id'];
                $this->insert_detail($value);
                $amount += $value['total_amount'];
            }
            
            $sql = "UPDATE " . $this->table . " SET amount = amount + " . $amount . " where id = " . $arr['id'] ;
            $this->db->query($sql); 
            

        }
        return $ret;
    }

    function update_by_invoice( $data ) {
        $arr = $this->getScheme($data);
        $this->db->update($this->table, $arr, array('sale_id' => $arr['sale_id']));

    }
    
    function insert_detail($data) {
        $this->scheme = $this->scheme_detail;
        $arr = $this->getScheme($data);
        $this->db->insert($this->table_detail,$arr);
        

    }

    function update_detail($data) {
        $this->scheme = $this->scheme_detail;
        $arr = $this->getScheme($data);
        $this->db->update($this->table_detail, $arr, array('sales_id' => $arr['sales_id'],'material_id' => $arr['material_id'] ));

    }

    function delete_detail($data) {
        $this->db->delete($this->table_detail, $data); 
    }

    function get_list($status = NULL,$member_id = NULL )
    {
        
        
        $this->db->select('sales.*, sale_items.quantity, sale_items.total_amount, sale_items.material_id, sale_items.notes,members.member_name,members.address, members.city,members.telp,members.email ');
        $this->db->from($this->table);
        $this->db->join('sale_items', 'sale_items.sales_id = sales.id','left');
        $this->db->join('members', 'sales.member_id = members.id','left');
        if (!is_null($status)) {
            $this->db->where("status", $this->status[$status]);
        }

        if (!is_null($member_id)) {
            $this->db->where("member_id", $member_id);
        }


        $query = $this->db->get();
        return $query->result();
    
    }

}

?>