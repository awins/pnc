<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Brand_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'brands';
        $this->scheme = array(
            'id',
            'brand_name',
            'brand_image',
            'background',
            'address',
            'contact',
            'phone',
            'email',
            'website'
        );
    }
    
    
    function get_list($page = 0,$row_per_page = 0)
    {
        
        if ($page == 0 ) {
            $this->db->order_by("brand_name", "asc");
            $query = $this->db->get($this->table);
        } else {
            $this->db->limit(20, ($page - 1) * 20);
            $this->db->order_by("id", "desc");
            $query = $this->db->get($this->table);
        }
            return $query->result();
    }


}

?>