-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2013 at 08:07 AM
-- Server version: 5.1.67-rel14.3-log
-- PHP Version: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pickench_pnc`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `user_name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `spec` text NOT NULL,
  `other_info` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='merepresentasikan nama pakaian terlepas dari ukuran & warna' AUTO_INCREMENT=107 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `item_id`, `title`, `code`, `spec`, `other_info`) VALUES
(1, 1, 'Ciput ninja', '#6D35', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(2, 1, 'Ciput ninja', '#87FE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(3, 1, 'Ciput ninja', '#6077', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(4, 1, 'Xin - xin', '#0E37', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(5, 1, 'Kalea', '#1A41', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(6, 1, 'Kalea', '#02EB', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(7, 1, 'Ciput arab zipper', '#3CFC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(8, 1, 'Ciput arab zipper', '#3E81', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(9, 1, 'Ciput arab zipper', '#06C5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(10, 1, 'Xin - xin', '#31FC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(11, 1, 'Xin - xin', '#7705', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(12, 1, 'Kalea', '#D288', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(13, 2, 'Body scarf', '#D45C', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(14, 3, 'Head Scarf', '#11F0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(15, 3, 'Head Scarf', '#26F1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(16, 3, 'Head Scarf', '#2723', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(17, 3, 'Head Scarf', '#9615', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(18, 3, 'Head Scarf', '#576A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(19, 3, 'Head Scarf', '#576A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(20, 3, 'Head Scarf', '#52EE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(21, 3, 'Head Scarf', '#64EF', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(22, 3, 'Head Scarf', '#67C7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(23, 3, 'Head Scarf', '#3542', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(24, 3, 'Head Scarf', '#9855', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(25, 3, 'Head Scarf', '#0000', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(26, 4, 'Long sleeves', '#CA75', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(27, 4, 'Long sleeves', '# 0000', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(28, 5, 'Full body', '#9CE8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(29, 6, 'Blouse', '#CCEA', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(30, 6, 'Blouse', '#CE5F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(31, 6, 'Blouse', '#B2CD', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(32, 6, 'Blouse', '#0F31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(33, 6, 'Blouse Chambray polkadot', '#7C8A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(34, 6, 'Blouse', '#8C12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(35, 6, 'Strype Blouse', '#9D54', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(36, 6, 'Kebaya knit bordir blouse', '#9F63', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(37, 6, 'Blouse kancing sulam', '#64A8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(39, 6, 'Blouse sulam', '#480C', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(40, 6, 'Blouse', '#1522', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(41, 6, 'Bordir Maroko', '#6328', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(42, 6, 'Blouse lipit bordir', '#8214', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(43, 6, 'Blouse renda', '#8582', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(44, 6, 'Blouse rompi renda', '#A4FF', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(45, 6, 'Blouse', '#B09F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(46, 6, 'Blouse rompi tenun Bali', '#C213', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(47, 6, 'Blouse sulam usus', '#E448', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(48, 6, 'Blouse', '#ED0F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(49, 6, 'Blouse rompi krestik', '#EE61', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(50, 6, 'Blouse', '#1D53', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(51, 6, 'Blouse kirana', '#63BA', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(52, 6, 'Blouse Iris Cardigans', '#63EE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(53, 6, 'Blouse', '#AAF5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(54, 6, 'Blouse Zipina vest', '#EA7F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(55, 6, 'Blouse Haraka', '#EBDC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(56, 6, 'Blouse Zhaza Top', '#34D6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(57, 6, 'Blouse Zhaza Top', '#3557', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(58, 6, 'Blouse Zhaza Top', '#B6BE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(59, 6, 'Blouse', '#7C74', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(60, 6, 'Blouse Cardigan kerut', '#1048', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(61, 6, 'Blouse', '#5247', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(62, 6, 'Blouse', '#E715', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(63, 6, 'Blouse', '#F280', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(64, 6, 'Blouse', '#FB95', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(65, 6, 'Blouse Hera Oversize', '#074E', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(66, 6, 'Blouse Cardea Outwear', '#4433', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(67, 6, 'Blouse Diva Outer wear', '#B3D3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(68, 6, 'Blouse', '#C002', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(69, 6, 'Tunic', '#CABC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(70, 6, 'Tunic', '#ED36', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(71, 6, 'Long Tunic kutung basic', '#FF51', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(72, 7, 'Twill pants', '#23BE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(73, 7, 'Twill pants', '#FBEE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(74, 7, 'Pants', '#A036', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(75, 7, 'Pants', '#B5E4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(76, 7, 'Kalilawar Pants', '#B6D1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(77, 7, 'Pants', '#C28C', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(78, 7, 'Pants', '#2B8C', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(79, 7, 'Pants', '#BDFE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(80, 7, 'Pants', '#CEEA', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(81, 7, 'Rughna Pants', '#0B7D', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(82, 7, 'Pants', '#6095', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(83, 8, 'Skirt', '#9FBF', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(84, 8, 'Skirt', '#9376', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(85, 8, 'Skirt', '#30F3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(86, 8, 'Knit Skirt', '#6D6D', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(87, 8, 'Skirt', '#4122', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(88, 8, 'Batik Skirt', '#E75C', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(89, 8, 'Jeans Skirt', '#F6CA', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(90, 8, 'Knit Skirt', '#4ACD', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(91, 8, 'Skirt', '#B990', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(92, 8, 'Chiffon Skirt', '#FA5F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(93, 8, 'Valza Skirt', '#2CDD', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(94, 8, 'Grenszy Skirt', '#E7CF', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(95, 9, 'Pant Skirt', '#7131', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(96, 10, 'Dress', '#616A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(97, 10, 'Dress', '#844F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(98, 10, 'Dress', '#1584', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(99, 10, 'Dress', '#6784', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(100, 10, 'Dress', '#A050', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis');
INSERT INTO `articles` (`id`, `item_id`, `title`, `code`, `spec`, `other_info`) VALUES
(101, 10, 'Dress', '#CE5F', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(102, 10, 'Dress', '#8022', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(103, 10, 'Dress', '#D093', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(104, 10, 'Dress', '#4149', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(105, 10, 'Batik Dress', '#9405', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis'),
(106, 10, 'Dress', '#B3DB', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \r\nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \r\nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \r\nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \r\ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \r\nlacus ante, sollicitudin sit amet porta a, aliquam at justo.', 'Produk ini tidak dapat dibingkis');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_image`) VALUES
(1, 'Deyn scarf', NULL),
(2, 'Dhiyaa', NULL),
(3, 'Fattible', NULL),
(4, 'FolkCloth', NULL),
(5, 'Ina''s Scarf', NULL),
(6, 'Look Up', NULL),
(7, 'Malana Indonesia', NULL),
(8, 'Nasywa', NULL),
(9, 'RA.PRO', NULL),
(10, 'S', NULL),
(11, 'Square', NULL),
(12, 'Up2Date', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'INNER'),
(2, 'HEAD SCARF'),
(3, 'BASIC'),
(4, 'TOP'),
(5, 'BOTTOM'),
(6, 'DRESS'),
(7, 'JUMPSUIT'),
(8, 'ACCESSORIES');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(50) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `title` text,
  `home_view` longtext,
  `full_content` longtext,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `section`, `date`, `title`, `home_view`, `full_content`, `active`) VALUES
(12, 'editors_pick', NULL, 'dfsafsadfa', '<img width="370" height="300" alt="EditorPic.png" src="http://pickenchoose.com/application/style/images/uploads/EditorPic.png" /> \r\n \r\n \r\n', '<img width="1124" height="683" src="http://pickenchoose.com/application/style/images/uploads/PnC_ols_Home_Editor_s_Pick.png" alt="PnC_ols_Home_Editor_s_Pick.png" />\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<br />\r\n\r\n', 1),
(13, 'news_update', NULL, 'lorem ipsum', '<div align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \nlacus ante, sollicitudin sit amet porta a, aliquam at justo. Nam \nvolutpat sem id eros ultrices imperdiet. Sed eleifend tempor consequat. \nFusce sit amet dolor tortor. Fusce eu diam dui. Vivamus a vulputate \nquam. Suspendisse potenti.\n\n \n</div>', '<div align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquet, \nsapien nec adipiscing consequat, metus velit scelerisque ante, quis \nrhoncus enim sapien id arcu. Nulla posuere eleifend arcu, et iaculis \nlorem porttitor sit amet. Vivamus sit amet dolor condimentum, viverra \ndui sed, pulvinar velit. Sed mattis orci quis leo dictum pharetra. Duis \nlacus ante, sollicitudin sit amet porta a, aliquam at justo. Nam \nvolutpat sem id eros ultrices imperdiet. Sed eleifend tempor consequat. \nFusce sit amet dolor tortor. Fusce eu diam dui. Vivamus a vulputate \nquam. Suspendisse potenti.\n\n \n</div>', 1),
(14, 'todays_promo', NULL, 'today promo', 'fas\n \n', '<img width="1124" height="671" src="http://pickenchoose.com/application/style/images/uploads/PnC_ols_Home_Today_s_Promo.png" alt="PnC_ols_Home_Today_s_Promo.png" />', 1),
(15, 'new_release', NULL, 'new release', '\nsdf\n \n \n', '<img width="1124" height="671" src="http://pickenchoose.com/application/style/images/uploads/PnC_ols_Home_New_Release.png" alt="new_release.png" />', 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='tabel untuk informasi item.' AUTO_INCREMENT=22 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `category_id`) VALUES
(1, 'Head', 1),
(2, 'Body', 1),
(3, 'Long', 2),
(4, 'Top', 3),
(5, 'Full', 3),
(6, 'Top', 4),
(7, 'Pants', 5),
(8, 'Skirt', 5),
(9, 'Pant Skirt', 5),
(10, 'Dress', 6),
(11, 'Jump suit', 7),
(21, 'Scarf', 8);

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `color` varchar(255) NOT NULL DEFAULT '-',
  `size` varchar(255) NOT NULL DEFAULT '-',
  `stock` int(10) NOT NULL DEFAULT '0',
  `status` int(10) NOT NULL DEFAULT '0',
  `reg_price` decimal(10,0) NOT NULL DEFAULT '0',
  `disc_price` decimal(10,0) DEFAULT '0',
  `display_gallery` tinyint(1) DEFAULT NULL,
  `display_pnc` tinyint(1) DEFAULT NULL,
  `display_mixmatch` tinyint(1) DEFAULT NULL,
  `display_suggestion` tinyint(1) DEFAULT NULL,
  `gallery_images` varchar(255) NOT NULL,
  `mixmatch_images` varchar(255) NOT NULL,
  `suggestion_images` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT '-',
  `tematic` varchar(255) DEFAULT '-',
  `price_mode` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` tinyint(4) NOT NULL DEFAULT '0',
  `is_latest` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='merepresentasikan detail item termasuk warna,size,stok' AUTO_INCREMENT=124 ;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `brand_id`, `article_id`, `color`, `size`, `stock`, `status`, `reg_price`, `disc_price`, `display_gallery`, `display_pnc`, `display_mixmatch`, `display_suggestion`, `gallery_images`, `mixmatch_images`, `suggestion_images`, `barcode`, `tematic`, `price_mode`, `is_new`, `is_latest`) VALUES
(1, 10, 1, 'Tosca green', 'X', 9, 0, 35000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 1, 0),
(2, 10, 2, 'Pink', 'X', 10, 0, 35000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(3, 10, 3, 'Brown', 'L', 0, 0, 35001, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 1, 0),
(4, 11, 4, 'Black', 'M', 12, 0, 110000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 1, 0),
(5, 11, 5, 'Light green', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 1, 0),
(6, 11, 6, 'Pink', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(7, 11, 7, 'Black', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(8, 11, 8, 'Pale green', 'XL', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(9, 11, 9, 'Orange', '-', 0, 0, 135000, 0, 1, 1, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(10, 11, 10, 'Tosca green', '-', 0, 0, 110000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(11, 11, 11, 'White', '-', 0, 0, 110000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(12, 11, 12, 'Black', '-', 0, 0, 175000, 0, 1, 1, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(13, 5, 13, 'Pink', '-', 0, 0, 135000, 0, 1, 1, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(14, 1, 14, '3 Tone', '-', 0, 0, 120000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(15, 1, 15, 'Light blue', '-', 0, 0, 120000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(16, 1, 16, 'Pink', '-', 0, 0, 120000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(17, 1, 17, 'Pattern', '-', 0, 0, 150000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(18, 2, 18, 'Red', '-', 0, 0, 60000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(19, 2, 19, 'Grey', '-', 0, 0, 60000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(20, 5, 20, 'Orange', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(21, 5, 21, 'Blue', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(22, 5, 22, 'Purple', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(23, 5, 23, 'Green', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(24, 5, 24, 'Pink', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', 'Lebaran', 0, 0, 0),
(25, 10, 25, '', '-', 0, 0, 0, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(26, 2, 26, 'Polos', '-', 0, 0, 145000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(27, 6, 27, 'Polos', '-', 0, 0, 0, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(28, 8, 28, 'Blue', '-', 0, 0, 215000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(29, 2, 29, 'Red', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(30, 2, 30, '', '-', 0, 0, 200000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(31, 3, 31, 'Grey', '-', 0, 0, 345000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(32, 4, 32, 'Pink', '-', 0, 0, 500000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(33, 4, 33, 'Pink', '-', 0, 0, 299000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(34, 4, 34, 'Blue', '-', 0, 0, 349000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(35, 4, 35, 'Blue', '-', 0, 0, 429000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(36, 4, 36, '', '-', 0, 0, 349000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(37, 4, 37, 'Grey', '-', 0, 0, 269000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(39, 4, 39, 'White', '-', 0, 0, 269000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(40, 4, 40, 'Brown', '-', 0, 0, 399000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(41, 4, 41, '', '-', 0, 0, 349000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(42, 4, 42, 'Grey', '-', 0, 0, 249000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(43, 4, 43, 'Blue', '-', 0, 0, 249000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(44, 4, 44, 'Grey', '-', 0, 0, 269000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(45, 4, 45, 'Pink-grey', '-', 0, 0, 429000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(46, 4, 46, 'Blue', '-', 0, 0, 269000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(47, 4, 47, '', '-', 0, 0, 349000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(48, 4, 48, 'Pink', '-', 0, 0, 429000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(49, 4, 49, 'Grey', '-', 0, 0, 269000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(50, 6, 50, 'Dark Tosca', '-', 0, 0, 185000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(51, 6, 51, 'Red & green', '-', 0, 0, 115000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(52, 6, 52, 'Red', '-', 0, 0, 125000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(53, 6, 53, 'Orange', '-', 0, 0, 185000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(54, 6, 54, '', '-', 0, 0, 95000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(55, 6, 55, 'Red & Grey', '-', 0, 0, 115000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(56, 7, 56, '', '-', 0, 0, 310000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(57, 7, 57, 'Gold', '-', 0, 0, 350000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(58, 7, 58, 'Blue', '-', 0, 0, 325000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(59, 8, 59, 'Green', '-', 0, 0, 265000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(60, 8, 60, 'Pink', '-', 0, 0, 135000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(61, 8, 61, 'Black - White', '-', 0, 0, 225000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(62, 8, 62, 'Pink', '-', 0, 0, 235000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(63, 8, 63, 'Blue Square motive', '-', 0, 0, 400000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(64, 8, 64, 'Black Square motive', '-', 0, 0, 245000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(65, 9, 65, 'Dusty Pink', '-', 0, 0, 259900, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(66, 9, 66, 'Teracota', '-', 0, 0, 279900, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(67, 9, 67, 'Teracota', '-', 0, 0, 329900, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(68, 9, 68, 'Teracota', '-', 0, 0, 229900, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(69, 3, 69, 'Motive', '-', 0, 0, 455000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(70, 3, 70, 'Grey', '-', 0, 0, 455000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(71, 12, 71, 'Broken white', '-', 0, 0, 260000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(72, 4, 72, '', '-', 0, 0, 399000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(73, 4, 73, '', '-', 0, 0, 299000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(74, 6, 74, '', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(75, 6, 75, 'Tartan', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(76, 6, 76, '', '-', 0, 0, 225000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(77, 6, 77, '', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(78, 7, 78, 'Blue Flower', '-', 0, 0, 295000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(79, 8, 79, 'Pink', '-', 0, 0, 265000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(80, 8, 80, 'Stripe', '-', 0, 0, 265000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(81, 9, 81, 'Glitter', '-', 0, 0, 349900, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(82, 9, 82, 'Samba', '-', 0, 0, 399900, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(83, 2, 83, 'Blue', '-', 0, 0, 175000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(84, 2, 84, 'Cecila', '-', 0, 0, 230000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(85, 3, 85, 'Kubnak', '-', 0, 0, 365000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(86, 4, 86, '', '-', 0, 0, 349000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(87, 4, 87, 'Blue', '-', 0, 0, 299000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', 'Kondangan', 0, 0, 0),
(88, 4, 88, 'Batik', '-', 0, 0, 269000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(89, 4, 89, '', '-', 0, 0, 249000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(90, 6, 90, 'Beige', '-', 0, 0, 165000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(91, 6, 91, 'Brown', '-', 0, 0, 225000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', 'Kondangan', 0, 0, 1),
(92, 6, 92, '', '-', 0, 0, 285000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(93, 7, 93, '', '-', 0, 0, 382500, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(94, 7, 94, '', '-', 0, 0, 355000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(95, 6, 95, 'Grey', '-', 0, 0, 225000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(96, 2, 96, 'Stripe', '-', 0, 0, 230000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(97, 2, 97, 'Green', '-', 0, 0, 250000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(98, 2, 98, 'Blue', '-', 0, 0, 215000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(99, 2, 99, 'Blue', '-', 0, 0, 225000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(100, 2, 100, 'Grey', '-', 0, 0, 250000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 1, 0),
(101, 2, 101, 'Orange', '-', 0, 0, 200000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(102, 7, 102, 'Green', '-', 0, 0, 435000, 0, 1, 1, 1, NULL, '', '', NULL, '-', '-', 0, 0, 0),
(103, 7, 103, 'Red', '-', 0, 0, 525000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', 'Lebaran', 0, 0, 0),
(104, 8, 104, 'Light Brown', '-', 0, 0, 315000, 0, 1, 1, 1, NULL, '', '', NULL, '-', '-', 0, 1, 0),
(105, 8, 105, 'Batik Black', '-', 0, 0, 345000, 0, 1, NULL, 1, NULL, '', '', NULL, '-', '-', 0, 0, 1),
(106, 8, 106, 'Polkadot', 'L', 10, 0, 215000, 0, 1, 1, 1, NULL, '', '', NULL, '-', 'Lebaran', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_name` varchar(255) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `valid_code` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `member_name`, `address`, `city`, `telp`, `email`, `user_name`, `user_password`, `is_active`, `valid_code`) VALUES
(1, 'Awin Syirojul', 'askfjkjsfk', 'jakarta', '5435454', 'awin@gmail.com', 'awin', 'awin', 1, NULL),
(2, 'Hira Sirajudin', 'kjkj', 'kjkjkjk', 'jkjkjkj', 'kjkjkj', 'hira', 'hira', 1, NULL),
(3, 'Mathius Andy', 'lsflskflk', 'klkl', 'klkllk', 'lklklkl', 'andy', 'andy', 1, NULL),
(4, 'Pick En Choose', 'Address', 'Jakarta', '1121212', 'pick@pnc.com', 'pick', 'pick', 1, NULL),
(5, 'jk', 'kjk', 'jkjkj', 'kjkjk', 'fsdfkjk', NULL, NULL, NULL, NULL),
(6, 'sfsf', 'sdf', 'sdf', '78978787', 'sdfsdf@gmail.com', NULL, 'aa', NULL, 'a002f46d503d61f347817cf65021bb4f'),
(7, 'df', 'sdf', 'sdf', 'sdf', 'adf@skfjs.com', NULL, 'aa', NULL, 'b7a525b860d7696868746b617e6ca52b'),
(8, 'fsdf', 'sdfsdf', 'sfsdf', 'sdfsdf', 'awkjksf@skfjksfj.com', NULL, 'aa', NULL, 'b41d94171b352a2f013d64e8ba951ed3'),
(9, 'sf', 'sfsf', 'sfsf', 'sfsf', 'sfsf@gmail.com', NULL, 'aa', NULL, '5047863d53863e696763a3bb1960687a'),
(10, 'dgdfg', 'dfgd', 'sdfsf', 'sdfsf', 'asfs@gmail.com', NULL, 'aa', NULL, 'f4705f7e8098060482d2ba81283865a1'),
(11, 'sdfsdf', 'sdfsdfsdf', 'sdfsdfsdfsdf', 'sdfsdfdsf', 'aadsf@gmail.com', NULL, 'aa', NULL, '5939de74152773da1b13cb5678e276d6'),
(12, 'sfsdfsdf', 'sdfssgdg', 'dfgdfgdg', 'fghfgh', 'gfhfgh@gmail.com', NULL, 'aa', NULL, '955a38821345ccdbdff4384b7c2ce889'),
(13, 'saya', 'rumah saya', 'kota saya', 'hp saya', 'saya@gmail.com', NULL, 'aa', NULL, '7f3ae606f18fc428a7db4ad81a4b890a');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` varchar(32) NOT NULL,
  `payment_code` varchar(32) DEFAULT NULL,
  `amount` decimal(11,0) NOT NULL,
  `return_url` varchar(255) DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0: cart open, 1: sale complete, 2: payment complete, 3: on delivery, 4: goods received(complete), 5: payment failed, 6: delivery failed',
  `195_reff` text COMMENT 'log_no from finnet',
  `payment_source` text COMMENT 'payment_source from finnet',
  `notes` varchar(1024) DEFAULT NULL,
  `created_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_complete_dt` datetime DEFAULT NULL,
  `payment_dt` datetime DEFAULT NULL,
  `delivery_dt` datetime DEFAULT NULL,
  `goods_received_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14616 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sale_id`, `payment_code`, `amount`, `return_url`, `member_id`, `status`, `195_reff`, `payment_source`, `notes`, `created_dt`, `sale_complete_dt`, `payment_dt`, `delivery_dt`, `goods_received_dt`) VALUES
(14500, '109997', '0195750003972', 895000, NULL, 3, 1, NULL, '', 'mer_signature : 3c3a34086918774331a526739d7508b4c859f20877f9990532822280c5addffc trax_type : 195Code merchant_id : DEVPNCH119 invoice : 109997 payment_code : 0195750003972 ', '2013-11-04 19:14:27', NULL, NULL, NULL, NULL),
(14601, 'c5aa63b0', '0195750003975', 310000, NULL, 1, 1, NULL, '', 'mer_signature : 22d20579816058fdc9dad547dce9adddae68c832dd25faa5b428a6b9ccc2d191 trax_type : 195Code merchant_id : DEVPNCH119 invoice : c5aa63b0 payment_code : 0195750003975 ', '2013-11-05 00:21:46', NULL, NULL, NULL, NULL),
(14602, 'ef4196e1', '0195750003976', 310000, NULL, 1, 1, NULL, '', NULL, '2013-11-05 00:35:36', NULL, NULL, NULL, NULL),
(14603, 'bbd8057b', '0195750003977', 320000, NULL, 1, 1, NULL, '', NULL, '2013-11-05 00:41:17', NULL, NULL, NULL, NULL),
(14604, '14c60c66', '0195750003979', 485000, NULL, 3, 1, NULL, '', NULL, '2013-11-05 00:50:57', NULL, NULL, NULL, NULL),
(14605, '48c6f921', '0195750003980', 770000, NULL, 3, 1, NULL, '', NULL, '2013-11-05 00:53:45', NULL, NULL, NULL, NULL),
(14606, '0c88d40a', '0195750003981', 35001, NULL, 3, 1, NULL, '', NULL, '2013-11-05 00:55:50', NULL, NULL, NULL, NULL),
(14607, '498d4371', '0195750003982', 330000, NULL, 3, 1, NULL, '', NULL, '2013-11-05 00:57:02', NULL, NULL, NULL, NULL),
(14608, 'e27a3d81', NULL, 35001, NULL, 3, 0, NULL, '', NULL, '2013-11-05 01:06:10', NULL, NULL, NULL, NULL),
(14609, '73d41714', '0195750003983', 210000, NULL, 2, 1, NULL, '', NULL, '2013-11-05 01:08:49', NULL, NULL, NULL, NULL),
(14610, 'fe5c22ff', '0195750004172', 1635001, NULL, 1, 1, NULL, NULL, NULL, '2013-11-06 17:06:25', NULL, NULL, NULL, NULL),
(14611, '782965fa', '0195750004102', 350000, NULL, 4, 1, NULL, NULL, NULL, '2013-11-11 11:18:59', NULL, NULL, NULL, NULL),
(14612, '485e2055', '0195750004298', 35001, NULL, 1, 1, NULL, NULL, NULL, '2013-11-14 05:47:07', NULL, NULL, NULL, NULL),
(14613, '0aaf5139', '0195750004299', 110000, NULL, 1, 1, NULL, NULL, NULL, '2013-11-14 06:46:47', NULL, NULL, NULL, NULL),
(14614, '1fe8118d', '0195750004301', 175000, NULL, 1, 1, NULL, NULL, NULL, '2013-11-14 09:42:53', NULL, NULL, NULL, NULL),
(14615, '470c66b5', NULL, 70000, NULL, 13, 0, NULL, NULL, NULL, '2013-11-19 18:02:11', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE IF NOT EXISTS `sale_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `total_amount` decimal(10,0) NOT NULL,
  `material_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`id`, `quantity`, `total_amount`, `material_id`, `sales_id`, `notes`) VALUES
(5, 2, 220000, 10, 14500, NULL),
(9, 5, 675000, 8, 14500, NULL),
(16, 1, 135000, 7, 14601, NULL),
(17, 1, 175000, 12, 14601, NULL),
(18, 1, 135000, 8, 14602, NULL),
(19, 1, 175000, 12, 14602, NULL),
(20, 1, 35000, 2, 14603, NULL),
(21, 1, 110000, 10, 14603, NULL),
(22, 1, 175000, 6, 14603, NULL),
(23, 1, 175000, 12, 14604, NULL),
(24, 1, 135000, 9, 14604, NULL),
(25, 1, 175000, 6, 14604, NULL),
(26, 3, 330000, 4, 14605, NULL),
(27, 4, 440000, 11, 14605, NULL),
(28, 1, 35001, 3, 14606, NULL),
(29, 3, 330000, 4, 14607, NULL),
(31, 1, 175000, 12, 14609, NULL),
(32, 1, 35000, 1, 14609, NULL),
(33, 1, 175000, 12, 14610, NULL),
(34, 1, 175000, 12, 14610, NULL),
(35, 1, 135000, 8, 14610, NULL),
(36, 1, 35000, 1, 14610, NULL),
(37, 1, 35001, 3, 14610, NULL),
(38, 1, 35000, 1, 14610, NULL),
(39, 1, 135000, 9, 14610, NULL),
(40, 1, 175000, 12, 14610, NULL),
(41, 1, 175000, 12, 14610, NULL),
(42, 1, 175000, 12, 14610, NULL),
(43, 1, 120000, 14, 14610, NULL),
(44, 1, 120000, 15, 14610, NULL),
(45, 1, 35000, 2, 14610, NULL),
(46, 1, 175000, 5, 14611, NULL),
(47, 1, 175000, 12, 14611, NULL),
(48, 1, 110000, 4, 14610, NULL),
(49, 1, 35001, 3, 14612, NULL),
(50, 1, 110000, 4, 14613, NULL),
(51, 1, 175000, 5, 14614, NULL),
(52, 1, 35000, 1, 14615, NULL),
(53, 1, 35000, 1, 14615, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
