<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	define('IMG_TYPE','png|gif|jpg|jpeg');

	define('DATA_NOT_FOUND',1);
	define('UPDATE_FAILED',2);
	define('ADD_FAILED',3);
	define('UPDATE_SUCCEED',4);
	define('ADD_SUCCEED',5);
	define('LOGIN_FAILED',6);
	define('ROW_PER_PAGE', 20);

	function statusTeks($status) {

		switch ($status) {
			case ADD_SUCCEED:
				return 'DATA ADDED SUCCESSFULLY';
				break;
			case ADD_FAILED:
				return 'DATA ADD FAILED';
				break;
			case UPDATE_SUCCEED:
				return 'DATA UPDATED SUCCESSFULLY';
				break;
			case UPDATE_FAILED:
				return 'DATA UPDATE FAILED';
				break;
			case DATA_NOT_FOUND:
				return 'DATA NOT FOUND';
				break;
			case LOGIN_FAILED:
				return 'LOGIN FAILED';
				break;
			default:
				return $status;
				break;
		}
	}

	function printNotice($status)
	{
		?>

		<div class="notice-box"><?= statusTeks($status); ?></div>
		<?php
	}

	function content_section_arr() {
	 	return array("editors_pick" => "Editors Pick", 
					"news_update" => "News Update", 
					"todays_promo" => "Todays Promo", 
					"new_release" =>	"New Release");
	}


	function getFileExtension($str)
    {
    	$arrSegments = explode('.', $str); // may contain multiple dots
        $strExtension = $arrSegments[count($arrSegments) - 1];
        $strExtension = strtolower($strExtension);
        return $strExtension;
    }

    function pagination($currentPage,$total_rows,$base_url) {
		//$this->load->library('pagination');

		
		$per_page = 20;
		$total_page = floor($total_rows / $per_page);
		if ( $total_rows % $per_page > 0 ) {
			$total_page++;
		}

		
		$result = '' ;
		if ( $total_page > 1 ) 
		{
			if ($currentPage > 1 ) {
				$result .=	'<div><a href="' . $base_url . '">First</a></div>';
				$result .=	'<div><a href="' . $base_url . ($currentPage - 1) .  '">&lt;</a></div>';
			} else {
				$result .=	'<div>First</div>';
				$result .=	'<div>&lt;</div>';
			}

			for ($i=1; $i <= $total_page ; $i++) { 
				if ( $i == $currentPage ) {
					$result .= '<div>' . $i . '</div>';
				} else {
					$result .= '<div><a href="' . $base_url . $i  . '">' . $i . '</a></div>';
				}
			}

			if ($currentPage < $total_page ) {
				$result .=	'<div><a href="' . $base_url . ($currentPage + 1) .  '">&gt;</a></div>';
				$result .=	'<div><a href="' . $base_url . $total_page . '">Last</a></div>';
			} else {
				$result .=	'<div>&gt;</div>';
				$result .=	'<div>Last</div>';
			}

		} else {
			$result .=	'<div>First</div>';
			$result .=	'<div>&lt;</div>';
			$result .= '<div>1</div>';
			$result .=	'<div>&gt;</div>';
			$result .=	'<div>Last</div>';



		}
		return $result;

		
	}
