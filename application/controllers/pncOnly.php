<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pncOnly extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}
		
		$tpl['js'] = array();
		$tpl['js'][] = 'front-pncOnly.js';

		$tpl['css'] = array();
		$tpl['css'][] = 'front-gallery.css';

		$this->load->view('front/__header.php',array('tpl' =>$tpl));

		/*$tpl = array();
		$tpl['content'] = $this->getPncOnlyPage(1,1);*/
		$this->load->view('front/pncOnly/template.php'); 
		$this->load->view('front/_footer.php',array('tpl' => $tpl));
		
	}


	public function getPncOnlyPage($category_id, $item_id, $page_number) {
		$page_type = 'pncOnly';
		
		$this->load->library('session');
		$cart = array();
		if ($this->isLoggedIn(false)) {
			$member = $this->session->userdata('member');
			if ($member->sale_id > 0) {
				$this->load->model('sale_model','Sale');
				$sale = $this->Sale->get_by_id( $member->sale_id);
				foreach ($sale as $val ) {
					$cart[] =	$val->material_id;
					
				}
			}
		}
		


		$tpl['shopping_cart'] = $cart;
		
		$this->load->model('material_model','Material');
		$this->load->model('item_model','Item');
		$tpl['category_id'] = $category_id;
		$tpl['page_number'] = $page_number;
		$tpl['result'] = $this->Material->get_list_by_page($page_type, $category_id,1,true,$item_id);

		$items = $this->Item->get_list();
		$sub_category = array();
		foreach ($items as $val) {
			$category = $val->category_name;
			if (!array_key_exists($category, $items)) {
				$sub_category[$category] = array();
			}
				$sub_category[$category][] = array('id' => $val->category_id, 'item_id' =>$val->id, 'name' => $val->item_name);
		}
		$tpl['sub_category'] = $sub_category;

		$content =	$this->load->view('front/pncOnly/content.php',array('tpl' =>$tpl ),TRUE); 

		echo $content;
		return;

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */