<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

		var $USER_195 = 'DEVPNCH119';
		var $PASS_195 =	'DEVPNCH119$1031';
		var $RETURN_URL_195 = 'http://pickenchoose.com/index.php/member/returnUrl195/';
		var $BILLHOST_URL = 'http://demos.finnet-indonesia.com/195/'; //DEVELOPMENT HOST
		//$BILLHOST_URL = 'https://billhosting.finnet-indonesia.com/prepaidsystem/195/'; //PRODUCTION HOST
		

	public function index()
	{
		if ($this->isAdmin()) {
			$this->load->view('admin/header');
			//$this->load->view('admin/index');
			$this->load->view('admin/footer');
		} 
	}

	public function register() {
		$tpl['js'][] = 'member.js';
		$tpl['css'][] = 'member.css';
		$this->load->view('front/__header.php',array('tpl' => $tpl));
		
		$reg_valid = false;

		$this->load->library(array('form_validation'));
		if ($this->input->post('register')) {
			$this->form_validation->set_rules('member_name', 'Name', 'required|xss_clean');
			$this->form_validation->set_rules('address', 'Address', 'required|xss_clean');
			$this->form_validation->set_rules('city', 'Kota', 'required|xss_clean');
			$this->form_validation->set_rules('telp', 'Telp', 'required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');

			$this->form_validation->set_rules('user_password', 'Password', 'required|xss_clean');
			$this->form_validation->set_rules('conf_password', 'Confirm Password', 'required|matches[user_password]');

			if($this->form_validation->run()) {

				$data = $this->input->post();
				$data['valid_code'] = md5(time());
				$this->load->model('member_model','Member');
				$this->Member->save($data);
				$reg_valid = true;
			} 
			
		} 

		if ($reg_valid) {
			$this->load->view('member/register_complete');		
		} else {
			$this->load->view('member/register');		
		}

		$this->load->view('front/_footer.php',array('tpl' => $tpl));
		

		
		
	
		
	}

	public function login() {
		$tpl = array();
		if ($this->input->post('login')) {
			$this->load->model('member_model','Member');
			$user = $this->Member->login($this->input->post());
			$this->load->helper(array('url'));
			if (count($user) > 0)
			{
				$user = $user[0];
				$this->load->library('session');
				$login['member'] = $user; 
				
				$this->load->model('sale_model','Sale');
				$sale = $this->Sale->get_by_member($user->id , 'open');
				
				if ( count($sale) > 0 ) {
					$sale = $sale[0];
					$login['member']->sale_id = $sale->id;
				} else {

					$login['member']->sale_id = 0 ;
				}

				$this->session->set_userdata($login);
				
				//redirect('admin', 'refresh');
				//return;
			} else {
				$tpl['status'] = LOGIN_FAILED;
			}
			redirect ('','refresh');

		} 
	}

	public function logout()
	{
		$this->load->library('session');
		$this->load->helper(array('url'));

		$this->session->unset_userdata('member');
		redirect ('','refresh');
		
	}

	public function shoppingCart() {

		$this->load->library('session');
		if ($this->isLoggedIn(true)) {
			$member = $this->session->userdata('member');
			$tpl['member'] = $member;
			if ($member->sale_id > 0) {
				$this->load->model('sale_model','Sale');
				$this->load->model('material_model','Material');
				$sale = $this->Sale->get_by_id( $member->sale_id);
				foreach ($sale as $cart ) {
					$item =	$this->Material->get_by_id($cart->material_id);
					if ($item != false ) {
						$item = $item[0];
						$item->vol = $cart->quantity;
						$tpl['data'][] = $item;
					}
				}
			}
		} else {
			$tpl['member'] = NULL;
		}

		$tpl['css'][] = 'member.css';
		$tpl['js'][] = 'member.js';
		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		
		$this->load->model('material_model','Material');
		$this->load->model('sale_model','Sale');
		$this->load->library('session');
		

		$this->load->view('member/shopping_cart',array('tpl' => $tpl));		
		$this->load->view('front/_footer.php',array('tpl' => $tpl));
		
	}


	public function addToCart() {
		$arr_id = json_decode($_GET['id']);

		$this->load->library('session');
		$this->load->model('sale_model','Sale');
		$this->load->model('material_model','Material');
		$member = $this->session->userdata('member');

		$sale = array();

		if ($member->sale_id > 0) {
			//$member->shopping_cart = array();
			$sale['id'] = $member->sale_id;

		} else {
			$rand =  time() . rand();
			$rand = md5($rand);
			$rand = substr($rand, 4, 8);
			$sale['sale_id'] = $rand;
		}

		$sale['member_id'] = $member->id;

		foreach ($arr_id as $value) {
			$item = $this->Material->get_basic_by_id($value);
			$detail['quantity'] = 1;
			$detail['total_amount'] = $item->reg_price;
			$detail['material_id'] = $value ;

			$sale['detail'][] = $detail;
		}

		$new_id = $this->Sale->save($sale);
		if ($member->sale_id == 0) {
			$member->sale_id = $new_id  ;
			$this->session->set_userdata($member);

		}

		
		return;

	}

	public function removeFromCart() {
		$arr_id = json_decode($_GET['id']);
		$this->load->library('session');
		$this->load->model('sale_model','Sale');
		$member = $this->session->userdata('member');

		if ($member->sale_id == 0) {
			return;
		}
		$data['sales_id'] = $member->sale_id;
		foreach ($arr_id as $value) {
			 $data['material_id'] = $value;
			 $this->Sale->delete_detail($data);
		}

		
		return;

	}

	public function editCartVol($item_id,$new_vol,$price) {

		$this->load->library('session');
		$member = $this->session->userdata('member');

		if ($member->sale_id == 0  ) {
			return;
		}

		$this->load->model('sale_model','Sale');
		$data['sales_id'] = $member->sale_id;
		$data['material_id'] = $item_id;
		$data['quantity'] = $new_vol;
		$data['total_amount'] = $new_vol * $price;

		$this->Sale->update_detail($data);
				
		echo 'Rp. ' . number_format($new_vol * $price) ;
		return ;
	}

	function setTotalSaleAmount($total_amount) {
		$this->load->library('session');
		$member = $this->session->userdata('member');


		if ($member->sale_id == 0  ) {
			return;
		}	

		$this->load->model('sale_model','Sale');
		$data['id'] = $member->sale_id;
		$data['amount'] = $total_amount;
		$this->Sale->save($data,false);
		echo $total_amount;
		return;
	}

	public function returnUrl195($id) {
		$this->load->model('sale_model','Sale');
		
		//TO WRITE LOG POST FROM 195 RESPON
		$log = '';
		foreach($_POST as $name=>$value){
			$_POST[$name]=htmlspecialchars(strip_tags(trim($value)));
			$log .= $name.' : '.htmlspecialchars(strip_tags(trim($value))).' ';
		}

		extract($_POST);

		$data = array();
		$data['id'] = $id;
		if($_POST["trax_type"]=="195Code"){ // return payment code

			$data['payment_code'] = $_POST['payment_code'];
			$data['status'] = 1;
			$this->Sale->save($data,false);
			return;	
		}

		if($_POST["trax_type"]=="Payment"){ // payment result
			switch ($_POST['result_code']) {
				case '00':		// Payment Success
					$data['status'] = 2;
					$data['195_reff'] = $_POST['log_no'];
					$data['payment_source'] = $_POST['payment_source'];
					$data['notes'] = $_POST['result_desc'];
					$this->Sale->save($data,false);
					break;
				case '05':		// Timeout/Expired
					$data['status'] = 5;
					$data['notes'] = $_POST['result_desc'];
					$this->Sale->save($data,false);
					break;
				default:
					# code...
					break;
			}
		}		

	}

	

	public function processPayment() {

		$this->load->library('session');
		$member = $this->session->userdata('member');


		if ($member->sale_id == 0  ) {
			redirect('member/shoppingCart');
			return;			
		}	

		$this->load->model('sale_model','Sale');

		$sale = $this->Sale->get_by_member($member->id);
		$sale = $sale[0];
		$total_price = $sale->amount;


		if ($total_price == 0  ) {
			redirect('member/shoppingCart');
			return;			
		}

		// REQUESTING PAYMENT CODE

		$REQUEST_URL_195 = $this->BILLHOST_URL.'response-insert.php';
		$CHECK_STATUS_URL_195 = $this->BILLHOST_URL.'check-status.php';
		$CANCEL_URL_195 = $this->BILLHOST_URL.'cancel-transaction.php';
		


		$this->load->helper('payment');

		$mer_password = $this->PASS_195; //IMPORTANT!
		$postdata = array(
			'merchant_id' => $this->USER_195,  //IMPORTANT!
			'invoice' => $sale->sale_id ,  //IMPORTANT!
			'amount' => $total_price,  //IMPORTANT!
			'add_info1' => $member->member_name,  //Customer Name //IMPORTANT!
			'timeout' => '120' , //60 Menit (Expired Date)  //IMPORTANT!
			'return_url' => $this->RETURN_URL_195 . $member->sale_id   //IMPORTANT! CHANGE THIS WITH YOUR RETURN TARGET URL!!!
		);
		$mer_signature =  mer_signature($postdata).$mer_password;  //IMPORTANT!
		/* END CREATE SIGANTURE */
		
		/* DATA FOR SENT */
		$postdata = array(
			'mer_signature' => strtoupper(hash256($mer_signature)),  //IMPORTANT!
			'merchant_id' => $postdata['merchant_id'],  //IMPORTANT!
			'invoice' => $postdata['invoice'],  //IMPORTANT!
			'amount' => $postdata['amount'],  //IMPORTANT!
			'add_info1' => $postdata['add_info1'], //Customer Name //IMPORTANT!
			'timeout' => $postdata['timeout'], //IMPORTANT!
			'return_url' => $postdata['return_url']   //IMPORTANT!
		);
	/* END DATA FOR SENT */
	
	
	//SENT DATA VIA CURL
	$respon = curl_post($REQUEST_URL_195, $postdata);
	
	//if($respon=='00'){	
		$data = $this->Sale->get_by_id($member->sale_id,NULL) ;
		$data = $data[0];
		$payment_code = $data->payment_code;
		$error = 0;
	/*}else{
		$error = $respon ;
		$payment_code = 0;
	}*/

		$member->sale_id = 0  ;
		$this->session->set_userdata($member);

		$tpl['member'] = $member;
		$tpl['css'][] = 'member.css';
		$this->load->view('front/__header.php',array('tpl' =>$tpl));

		$tpl['error'] = $error;
		$tpl['payment_code'] = $payment_code;
		$this->load->view('member/finish_shopping',array('tpl' => $tpl));		
		$this->load->view('front/_footer.php');




	}

	function payed() {
		$tpl['css'][] = 'member.css';
		$this->load->view('front/__header.php',array('tpl' =>$tpl));

		$tpl['error'] = 0;
		$tpl['payment_code'] = '090900989898';
		$this->load->view('member/finish_shopping',array('tpl' => $tpl));		
		$this->load->view('front/_footer.php');	
	}
}

	

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */