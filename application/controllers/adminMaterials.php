<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminMaterials extends CI_Controller {



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page = 1,$status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('material_model','Material');
			
			$tpl['js'][] = 'adminMaterials.js';
			$this->load->view('admin/header',array('tpl' => $tpl ));
			
			$total_rows = $this->Material->getCount() ;
			$base_url = INDEX_URL . '/adminMaterials/index/';
			$tpl['pagination'] = pagination($page,$total_rows,$base_url);

			$tpl['data'] = $this->Material->get_list($page);
			$tpl['status'] = $status;
			$tpl['page'] = $page;
			$this->load->view('admin/material/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err_thumb = NULL;
		    $upload_err_full = NULL;
		    $upload_err_mix = NULL;
			
			$this->load->helper(array('form'));
			$this->load->model('material_model','Material');
			
			$this->load->library(array('form_validation'));
			if ($this->input->post('material_update')) {

				$this->form_validation->set_rules('brand_id', 'Brand', 'required|xss_clean');
				$this->form_validation->set_rules('article_id', 'Article', 'required|xss_clean');
				$this->form_validation->set_rules('barcode', 'Barcode', 'required|xss_clean');
				$this->form_validation->set_rules('tematic', 'Tematic', 'required|xss_clean');
				$this->form_validation->set_rules('color', 'Color', 'required|xss_clean');
				$this->form_validation->set_rules('size', 'Size', 'required|xss_clean');

				$id = $this->input->post('id');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
						
					if ($this->Material->save($data)) {
						redirect('adminMaterials/index/1/' . UPDATE_SUCCEED , 'refresh');
						return;
					}else{
						$status = UPDATE_FAILED;
					}
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Material->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}


			// POPULATE BRAND DATAS
			$this->load->model('brand_model','Brand');
			$tpl['brands'] = $this->Brand->get_list();

			// POPULATE ARTICLE DATAS
			$this->load->model('article_model','Article');
			$tpl['articles'] = $this->Article->get_list();
			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/material/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');

		} else {

		}
	}

	public function updateField($id,$field,$value ){
		$this->load->model('material_model','Material');
		$data = array();
		$data['id'] = $id;
		$data[$field] = $value;
		var_dump($data);
		$this->Material->save($data);
		return;
	}


	public function create() {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
			
			$this->load->helper(array('form'));
			$this->load->model('material_model','Material');
			
			$this->load->library(array('form_validation'));

			if ($this->input->post('material_create')) {

				$this->form_validation->set_rules('brand_id', 'Brand', 'required|xss_clean');
				$this->form_validation->set_rules('article_id', 'Article', 'required|xss_clean');
				$this->form_validation->set_rules('barcode', 'Barcode', 'required|xss_clean');
				$this->form_validation->set_rules('tematic', 'Tematic', 'required|xss_clean');
				$this->form_validation->set_rules('color', 'Color', 'required|xss_clean');
				$this->form_validation->set_rules('size', 'Size', 'required|xss_clean');
 				

				if($this->form_validation->run()) {
					$data = $this->input->post();
						
					if ($this->Material->save($data)) {
						redirect('adminMaterials/index/1/' . ADD_SUCCEED , 'refresh');
						return;
					}			
				
				} else {
					$status = ADD_FAILED;
				}

			} 

			// POPULATE BRAND DATAS
			$this->load->model('brand_model','Brand');
			$tpl['brands'] = $this->Brand->get_list();

			// POPULATE ARTICLE DATAS
			$this->load->model('article_model','Article');
			$tpl['articles'] = $this->Article->get_list();
			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/material/create',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}


	public function upload($category_id = 0, $id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    
			
			$this->load->model('category_model','Category');
			
			$tpl['js'][] = 'adminMaterials.js';
			$tpl['js'][] = 'jquery.form.js';

			$this->load->view('admin/header',array('tpl' => $tpl));
			$tpl = array();
			$tpl['categories'] = $this->Category->get_list();
			$tpl['status'] = $status;
			$tpl['category_id'] =  $category_id;
			$tpl['material_id'] = $id;

			$this->load->view('admin/material/upload',array('tpl' => $tpl));		
			$this->load->view('admin/footer');

		} else {

		}
	}

	public function uploadImage() {
		$tmp_file_name = $_FILES['doc_file']['tmp_name'];
		$ext = getFileExtension($_FILES['doc_file']['name']);
		$ext = 'png';
		$id = $_POST['id'];
		$rel = $_POST['rel'];
		if ($rel == 'S_') {
			$file_name = $rel . '_' . $id . '_' . time() . '.' . $ext;
			$this->load->model('material_model','Material');
			$data =	$this->Material->get_by_id($id);
			$data = $data[0];
			$lastImage = $data->suggestion_images;
			if (strlen($lastImage) > 0 ) {
				$lastImage .= ';';
			}
		} else {
			$file_name = $rel . '_' . $id .  '.' . $ext;
			
		}
		$file = $_SERVER['DOCUMENT_ROOT'] . '/application/style/images/materials/' . $file_name;

		$ok = move_uploaded_file($tmp_file_name,  $file);
		if ($ok) {
			$response = IMG_PATH . 'materials/' . $file_name;
			if ($rel == 'S_') {
				$data = array();
				$data['id'] = $id;
				$data['suggestion_images'] = $lastImage . $file_name;
				$this->Material->save($data);
			}

		} else {
			$response = 100;
		}
		

		echo $response;
		exit;
	}

	public function list_material($category_id)
	{
		$this->load->model('material_model','Material');
		$tpl['materials'] = $this->Material->get_list_by_category($category_id);

		$data =	$this->load->view('admin/material/upload_item_list',array('tpl' => $tpl),TRUE);		
		echo $data;
	}

	public function detail_material($id)
	{
		$this->load->model('material_model','Material');
		$tpl['materials'] = $this->Material->get_by_id($id);

		$data =	$this->load->view('admin/material/upload_item_detail',array('tpl' => $tpl),TRUE);		
		echo $data;
	}

	public function updateDisplay($page_type,$id,$value)
	{
		$this->load->model('material_model','Material');
		if ($value == 1){

			$data =	$this->Material->get_by_id($id);
			$data = $data[0];
			if ($page_type == 'gallery') {
				if ($data->gallery_thumb == NULL || $data->gallery_front == NULL || $data->gallery_rear == NULL ) {
					echo '100';
					return ;
				}
			}

			if ($page_type == 'pnc') {
				if ($data->gallery_thumb == NULL || $data->gallery_front == NULL || $data->gallery_rear == NULL ) {
					echo '100';
					return ;
				}
			}

			if ($page_type == 'mixmatch') {
				if ($data->mixmatch_thumb == NULL || $data->mixmatch_front == NULL || $data->mixmatch_rear == NULL ) {
					echo '100';
					return ;
				}
			}

			if ($page_type == 'suggestion') {
				if (count($data->suggestions) == 0 ) {
					echo '100';
					return ;
				}
			}
		}

		$data = array();
		$data['id'] = $id;
		if ($page_type == 'gallery') {
			$data['display_gallery'] = $value;
		}elseif ($page_type == 'pnc') {
			$data['display_pnc'] = $value;
		}elseif ($page_type == 'mixmatch') {
			$data['display_mixmatch'] = $value;
		}elseif ($page_type == 'suggestion') {
			$data['display_suggestion'] = $value;
		}

		$this->Material->save($data);

		echo 200;
		return;
	}

	public function display($page_type = 'gallery',$category_id = 0 ,$filter_display = 2) {
		$tpl['js'][] = 'adminDisplay.js' ;
		$tpl['js'][] = 'jquery.simplemodal.js';
		$this->load->view('admin/header',array('tpl' =>$tpl));

		$tpl = array();
		if ($category_id > 8 )  $category_id = 8 ;
		if ($filter_display > 2 )  $filter_display = 2 ;

		$this->load->model('material_model','Material');
		$this->load->model('category_model','Category');

		$result	= $this->Material->get_list_by_page($page_type, $category_id, $filter_display,false);
        
		$tpl['categories'] = $this->Category->get_list();
		$tpl['materials'] = $result['data'];
		$tpl['category_id'] = $category_id;
		$tpl['filter_display'] = $filter_display;
		$this->load->view('admin/display/' . $page_type,array('tpl' => $tpl));		
		$this->load->view('admin/footer');
	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */