<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminMaterials extends CI_Controller {



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('material_model','Materials');

			$tpl['data'] = $this->Materials->get_list();
			$tpl['status'] = $status;
			$this->load->view('admin/material/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err_thumb = NULL;
		    $upload_err_full = NULL;
		    $upload_err_mix = NULL;
			
			$this->load->helper(array('form'));
			$this->load->model('clothes_model','Clothes');
			
			$this->load->library(array('form_validation'));
			if ($this->input->post('clothes_update')) {

				$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
				$this->form_validation->set_rules('brand_id', 'Brand', 'required|xss_clean');
				$this->form_validation->set_rules('category_id', 'Category', 'required|xss_clean');
				$id = $this->input->post('id');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
				    $config['allowed_types'] = IMG_TYPE ;
					
					// UPLOAD THUMB
					if (strlen($_FILES['thumb']['name']) > 0)
					{	
						$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/clothes/thumb' ;
					    $this->load->library('upload', $config,'Thumb');
						
						$res = $this->Thumb->do_upload('thumb');
						if ($res) {
							$data['thumb_image'] = $this->Thumb->file_name;
						} else {
							$upload_err_thumb = $this->Thumb->display_errors();
						}
					}	

						// UPLOAD FULL
					if (strlen($_FILES['full']['name']) > 0)
					{
						$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/clothes/full' ;
					    $this->load->library('upload', $config,'Full');
						
						$res = $this->Full->do_upload('full');
						if ($res) {
							$data['full_image'] = $this->Full->file_name;
						} else {
							$upload_err_full = $this->Full->display_errors();
						}
					}

					// UPLOAD MIX MATCH
					if (strlen($_FILES['mix']['name']) > 0)
					{
						$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/clothes/mix' ;
					    $this->load->library('upload', $config,'Mix');
						
						$res = $this->Mix->do_upload('mix');
						if ($res) {
							$data['mix_image'] = $this->Mix->file_name;
						} else {
							$upload_err_mix = $this->Mix->display_errors();
						}
					}
					
					if (is_null($upload_err_thumb) && is_null($upload_err_full)  ) {
						
						if ($this->Clothes->save($data)) {
							redirect('adminClothes/index/' . UPDATE_SUCCEED , 'refresh');
							return;
						}else{
							$status = UPDATE_FAILED;
						}
					} else {
						$status = UPDATE_FAILED;
					}				
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Clothes->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}

			// POPULATE BRAND DATAS
			$this->load->model('brand_model','Brand');
			$tpl['brands'] = $this->Brand->get_list();
			// POPULATE CATEGORY DATAS
			$this->load->model('category_model','Category');
			$tpl['categories'] = $this->Category->get_list();

			$tpl['status'] = $status;
			$tpl['upload_err_thumb'] = $upload_err_thumb;
			$tpl['upload_err_full'] = $upload_err_full;
			$tpl['upload_err_mix'] = $upload_err_mix;
			$this->load->view('admin/header');
			$this->load->view('admin/clothes/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}

	public function create() {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err_thumb = NULL;
		    $upload_err_full = NULL;
		    $upload_err_mix = NULL;
			
			$this->load->helper(array('form'));
			$this->load->model('material_model','Material');
			
				$this->load->library(array('form_validation'));
			if ($this->input->post('material_create')) {

				$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
				$this->form_validation->set_rules('brand_id', 'Brand', 'required|xss_clean');
				$this->form_validation->set_rules('category_id', 'Category', 'required|xss_clean');
				$this->form_validation->set_rules('item_id', 'Item', 'required|xss_clean');
				$this->form_validation->set_rules('article_id', 'Article', 'required|xss_clean');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
				    $config['allowed_types'] = IMG_TYPE ;
					// UPLOAD THUMB
					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/clothes/thumb' ;
				    $this->load->library('upload', $config,'Thumb');
					
					$res = $this->Thumb->do_upload('thumb');
					if ($res) {
						$data['thumb_image'] = $this->Thumb->file_name;
					} else {
						$upload_err_thumb = $this->Thumb->display_errors();
					}
					

					// UPLOAD FULL
					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/clothes/full' ;
				    $this->load->library('upload', $config,'Full');
					
					$res = $this->Full->do_upload('full');
					if ($res) {
						$data['full_image'] = $this->Full->file_name;
					} else {
						$upload_err_full = $this->Full->display_errors();
					}

					// UPLOAD MIX MATCH
					if (strlen($_FILES['mix']['name']) > 0)
					{
						$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/clothes/mix' ;
					    $this->load->library('upload', $config,'Mix');
						
						$res = $this->Mix->do_upload('mix');
						if ($res) {
							$data['mix_image'] = $this->Mix->file_name;
						} else {
							$upload_err_mix = $this->Mix->display_errors();
						}
					}

					if (is_null($upload_err_thumb) && is_null($upload_err_full)  ) {
						
						if ($this->Clothes->save($data)) {
							redirect('adminClothes/index/' . ADD_SUCCEED , 'refresh');
							return;
						}else{
							$status = ADD_FAILED;
						}
					} else {
						$status = ADD_FAILED;
					}				
				
				} else {
					$status = ADD_FAILED;
				}

			} 

			// POPULATE BRAND DATAS
			$this->load->model('brand_model','Brand');
			$tpl['brands'] = $this->Brand->get_list();
			// POPULATE CATEGORY DATAS
			$this->load->model('category_model','Category');
			$tpl['categories'] = $this->Category->get_list();

			$tpl['status'] = $status;
			$tpl['upload_err_thumb'] = $upload_err_thumb;
			$tpl['upload_err_full'] = $upload_err_full;
			$tpl['upload_err_mix'] = $upload_err_mix;
			$this->load->view('admin/header');
			$this->load->view('admin/clothes/create',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */