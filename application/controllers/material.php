<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	
class material extends CI_Controller {
	

	public function index()
	{
		
	}

	public function listTematic() {
		$this->load->model('material_model','Material');
		$data = $this->Material->get_tematic_list();
		$arr = array();
		foreach ($data as $val) {
			$arr[] = $val->tematic;
		}
		echo json_encode($arr);
	}

	public function getMixMatchPage($category_id, $page_number) {
		$page_type = 'mixmatch';

		$this->load->model('material_model','Material');
		$result = $this->Material->get_list_by_page($page_type, $category_id);
		$pages_count = $result['pages_count'];
		$items_count = $result['rows_count'];
		$start = (($page_number - 1) * $this->Material->row_per_page) ;
		$limit = $this->Material->row_per_page * ($page_number ) ;
		
		$end = $start + $this->Material->row_per_page + 1;
		if ($end > $items_count ) {
			$end = $items_count;
		}
		$arr = array();
		$arr['page_number'] = intval($page_number);
		$arr['pages_count'] = $pages_count;
		$arr['item_start'] = $start + 1;
		$arr['item_end'] = $end;

		$arr['items_count'] = $items_count;
		$arr['data'] = array();

		//$rand = '?' . rand();
		$i = $start;
		while (($i < $items_count ) && ($i < $limit ) ) {
			$row = $result['data'][$i];
			
			
			$data['id'] = 						$row->id ;
			if ($row->category_name == 'BASIC' || $row->category_name == 'DRESS' || $row->category_name == 'JUMPSUIT') {
				$data['category'] = 				'OUTER' ;
			} else {
				$data['category'] = 				$row->category_name ;
			}
			$data['brand'] = 					$row->brand_name;
			$data['article']['title'] = 		$row->title ;
			$data['article']['code'] = 			$row->code ;
			$data['article']['spec'] = 			$row->spec ;
			$data['article']['other_info'] = 	$row->other_info ;
			$data['item'] = 					$row->item_name ;
			$data['color'] = 					$row->color  ;
			$data['size'] = 					$row->size ;
			$data['stock'] = 					$row->stock;
			$data['status'] = 					$row->status;
			$data['reg_price'] = 				$row->reg_price;
			$data['disc_price'] = 				$row->disc_price;
			$data['barcode'] = 					$row->barcode;
			$data['tematic'] = 					$row->tematic;
			$data['thumbnail'] = 				IMG_PATH . 'materials/T_M_' . $row->id . '.png' ;//. $rand;
			$data['display_front'] = 			IMG_PATH . 'materials/F_M_' . $row->id . '.png' ;//. $rand;
			$data['display_rear'] = 			IMG_PATH . 'materials/R_M_' . $row->id . '.png' ;//. $rand;



			// SUGGESTION
			$img_path = IMG_PATH  . 'materials/' ;
            $img_dir = IMG_DIR  . 'materials/' ;
            $suggestion = $row->suggestion_images;
            $arr_suggestion = array();

            if (strlen($suggestion) > 0) {
                $arr_images = explode(";", $suggestion);
                foreach ($arr_images as $value) {
                    if (file_exists($img_dir . $value))
                    {
                        $arr_suggestion[] = $img_path . $value;// . $rand;
                    }
                }
				$data['suggestions'] = 				$arr_suggestion;

            } else {

				$data['suggestions'] = 				array();
            }

			$arr['data'][] = $data; 
			$i++;
			
		}  ;

				//var_dump($arr);
		header("Content-type: application/json; charset=utf-8");
		$json =	json_encode($arr);
		echo ($json);

	}

	public function getTematicPage($tematic, $page_number) {
		$page_type = 'tematic';

		$this->load->model('material_model','Material');
		$result = $this->Material->get_list_by_page($page_type, $tematic);
		$pages_count = $result['pages_count'];
		$items_count = $result['rows_count'];
		$start = (($page_number - 1) * $this->Material->row_per_page) ;
		$limit = $this->Material->row_per_page * ($page_number ) ;
		
		$end = $start + $this->Material->row_per_page + 1;
		if ($end > $items_count ) {
			$end = $items_count;
		}
		$arr = array();
		$arr['page_number'] = intval($page_number);
		$arr['pages_count'] = $pages_count;
		$arr['item_start'] = $start + 1;
		$arr['item_end'] = $end;

		$arr['items_count'] = $items_count;
		$arr['data'] = array();


		$i = $start;
		while (($i < $items_count ) && ($i < $limit ) ) {
			$row = $result['data'][$i];
			
			
			$data['id'] = 						$row->id ;
			if ($row->category_name == 'BASIC' || $row->category_name == 'DRESS' || $row->category_name == 'JUMPSUIT') {
				$data['category'] = 				'OUTER' ;
			} else {
				$data['category'] = 				$row->category_name ;
			}
			$data['brand'] = 					$row->brand_name;
			$data['article']['title'] = 		$row->title ;
			$data['article']['code'] = 			$row->code ;
			$data['article']['spec'] = 			$row->spec ;
			$data['article']['other_info'] = 	$row->other_info ;
			$data['item'] = 					$row->item_name ;
			$data['color'] = 					$row->color  ;
			$data['size'] = 					$row->size ;
			$data['stock'] = 					$row->stock;
			$data['status'] = 					$row->status;
			$data['reg_price'] = 				$row->reg_price;
			$data['disc_price'] = 				$row->disc_price;
			$data['barcode'] = 					$row->barcode;
			$data['tematic'] = 					$row->tematic;
			$data['thumbnail'] = 				IMG_PATH . 'materials/T_M_' . $row->id . '.png';
			$data['display_front'] = 			IMG_PATH . 'materials/F_M_' . $row->id . '.png';
			$data['display_rear'] = 			IMG_PATH . 'materials/R_M_' . $row->id . '.png';



			// SUGGESTION
			$img_path = IMG_PATH  . 'materials/' ;
            $img_dir = IMG_DIR  . 'materials/' ;
            $suggestion = $row->suggestion_images;
            $arr_suggestion = array();

            if (strlen($suggestion) > 0) {
                $arr_images = explode(";", $suggestion);
                foreach ($arr_images as $value) {
                    if (file_exists($img_dir . $value))
                    {
                        $arr_suggestion[] = $img_path . $value;
                    }
                }
				$data['suggestions'] = 				$arr_suggestion;

            } else {

				$data['suggestions'] = 				array();
            }

			$arr['data'][] = $data; 
			$i++;
			
		}  ;

				//var_dump($arr);
		header("Content-type: application/json; charset=utf-8");
		$json =	json_encode($arr);
		echo ($json);

	}

	
	public function detail($material_id) {
		$this->load->library('session');
		$tpl['shopping_cart'] = array();
		if ($this->isLoggedIn(false)) {
			
			$member = $this->session->userdata('member');
			if (isset($member->shopping_cart)) {
				$tpl['shopping_cart'] = $member->shopping_cart;
			} else {

			}
			$tpl['member'] = $member;
		} else {
			$tpl['member'] = NULL;
		}
		
		$tpl['js'] = array();
		$tpl['js'][] = 'front-detail.js';
		$tpl['outer_js'][] = "http://www.google.com/recaptcha/api/js/recaptcha_ajax.js";
		$tpl['css'] = array();
		$tpl['css'][] = 'front-detail.css';

		$this->load->view('front/__header.php',array('tpl' =>$tpl));

		$this->load->model('material_model','Material');
		$tpl['result'] = $this->Material->getColorRange($material_id);
		$tpl['size_range'] = $this->Material->getSizeRange($material_id);
		$tpl['content'] =	$this->load->view('front/detail/content.php',array('tpl' =>$tpl ),TRUE); 


		$tpl['material_id'] = $material_id;
		$this->load->view('front/detail/template.php',array('tpl' => $tpl )); 
		$this->load->view('front/_footer.php',array('tpl' => $tpl));
		
	}

	public function detail_brand_info($brand_id) {
		$tpl = array();
		$this->load->model('brand_model','Brand');
		$tpl['data'] = $this->Brand->get_by_id($brand_id);
		$content = $this->load->view('front/detail/brand_info.php',array('tpl' => $tpl ),TRUE); 
		echo $content;
		exit;
	}	

	
	public function detail_review($material_id,$material_title) {
		$tpl = array();
		if ($this->isLoggedIn(false)) {			
			$member = $this->session->userdata('member');
			$tpl['member'] = $member;
		} else {
			$tpl['member'] = NULL;
		}


		$option = array();
		$option['where'] = array('material_id' => $material_id);

		$this->load->model('review_model','Review');
		$tpl['rows'] = $this->Review->getCount($option);
		$tpl['material_title'] = $material_title;

		$content = $this->load->view('front/detail/product_review.php',array('tpl' => $tpl ),TRUE); 
		echo $content;
		exit;
	}

	public function detail_review_content($material_id,$page) {
		$tpl = array();
		$option = array();
		$option['where'] = array('material_id' => $material_id);

		$option['select'] = 'product_reviews.*,members.member_name';
		$option['join'] = array('members' => 'product_reviews.member_id = members.id');
		$option['limit'] = 10;
		$option['page'] = $page;
		$option['order_by'] = 'product_reviews.id desc';

		$this->load->model('review_model','Review');
		$tpl['data'] = $this->Review->get_list($option);
		$content = $this->load->view('front/detail/product_review_content',array('tpl' => $tpl ),TRUE); 
		echo $content;
		exit;
	}	

	public function post_review($material_id) {
		$this->load->helper(array('form'));

		$tpl = array();
		if ($this->input->post('post_review')) {
			$this->load->helper('recaptcha_helper');

			$this->load->library(array('form_validation'));
			$this->form_validation->set_error_delimiters('<div style="color: red" >* ', '</div>');
			$this->form_validation->set_rules('comment', 'Comment', 'required|xss_clean');


			$resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
                                $_SERVER["REMOTE_ADDR"],
                                $this->input->post('recaptcha_challenge_field'),
                                $this->input->post('recaptcha_response_field'));
			if ($resp->is_valid) {

			} else {
				$this->form_validation->set_rules('recaptcha_match', 'Captcha', 'required|matches[post_review]');
				$this->form_validation->set_message('matches', 'verifikasi gagal');
				$tpl['status'] = 100;
			}
			if($this->form_validation->run()) {
				$this->load->library('session');
				$member = $this->session->userdata('member');
				$this->load->model('review_model','Review');
				$data = array();
				$data['member_id'] = $member->id;
				$data['material_id'] = $material_id;
				$data['date'] = date('Y-m-d h:i:s', time());
				$data['rate'] =  $this->input->post('rate');
				$data['comment'] =  $this->input->post('comment');
				$this->Review->save($data);
				$tpl['status'] = 200;
			} else {
				$tpl['status'] = 100;
				
			}
		}
		$content =  $this->load->view('front/detail/post_review',array('tpl' => $tpl),TRUE);
		echo $content;
		exit;

	}

}

