<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class brand extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}
		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		
		$tpl['js'] = array();
		$tpl['js'][] = 'swfobject_modified.js';

		$tpl = array();
		$this->load->view('front/brand.php',array('tpl' =>$tpl )); 
		$this->load->view('front/_footer.php');
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */