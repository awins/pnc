<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($this->isAdmin()) {
			$this->load->view('admin/header');
			//$this->load->view('admin/index');
			$this->load->view('admin/footer');
		} 
	}

	public function login() {
		$tpl = array();
		if ($this->input->post('login')) {
			$this->load->model('admin_model','User');
			$user = $this->User->login($this->input->post());
			$this->load->helper(array('url'));
			if (count($user) > 0)
			{
				$this->load->library('session');
				$login['admin'] = $user[0]; 
				$this->session->set_userdata($login);
				
				redirect('admin', 'refresh');
				return;
			} else {
				$tpl['status'] = LOGIN_FAILED;
			}

		} 
		
		$this->load->view('admin/login',array('tpl' => $tpl));		
	
		
	}

	public function logout()
	{
		$this->load->library('session');
		$this->session->unset_userdata('admin');
		$this->login();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */