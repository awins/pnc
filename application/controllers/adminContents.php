<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminContents extends CI_Controller {



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($section = 'ALL' , $status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('content_model','Content');
			$tpl['data'] = $this->Content->get_list();
			$tpl['section_active'] = $section;
			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/content/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err = NULL;
			
			$this->load->helper(array('form','xinha','url'));
			$this->load->model('content_model','Content');
			$this->load->library(array('form_validation'));

			if ($this->input->post('content_update')) {

				$this->form_validation->set_rules('section', 'Section', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
				$id = $this->input->post('id');
				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
						
						if ($this->Content->save($data)) {
							redirect('adminContents/index/' . UPDATE_SUCCEED , 'refresh');
							return;
						}else{
							$status = UPDATE_FAILED;
						}
									
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Content->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}
			$data['xinha_inclusion'] = javascript_xinha(array('hove_view','full_content'),
                                                   array('ImageManager',
                            'InsertSmiley'),
                            "blue-look");
			$tpl['status'] = $status;
			$tpl['upload_err'] = $upload_err;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/content/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}

	public function create() {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err_thumb = NULL;
		    $upload_err_full = NULL;
		    $upload_err_mix = NULL;
			
			$this->load->helper(array('form', 'xinha'));
			$this->load->model('content_model','Content');
			
			$this->load->library(array('form_validation'));
			if ($this->input->post('content_create')) {
				$data = $this->input->post();

				$this->form_validation->set_rules('section', 'Section', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
				//$this->form_validation->set_rules('home_view', 'Home View', 'required|xss_clean');
				//$this->form_validation->set_rules('full_content', 'Full Content', 'required|xss_clean');
 				
				if($this->form_validation->run()) {
										
					if ($this->Content->save($data)) {
						redirect('adminContents/index/' . $data['section'] . '/' . ADD_SUCCEED , 'refresh');
						return;
					}else{
						$status = ADD_FAILED;
					}
								
				} else {
					$status = ADD_FAILED;
				}

			} 
			$data['xinha_inclusion'] = javascript_xinha(array('hove_view','full_content'),
                                                   array('ImageManager',
                            'InsertSmiley'),
                            "blue-look");
			$tpl['status'] = $status;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/content/create',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */