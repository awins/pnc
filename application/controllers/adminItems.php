<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminItems extends CI_Controller {



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page = 1,$status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('item_model','Item');
			$tpl['menu']['header'] = 'adminItems';
			$tpl['menu']['child'] = 'list';
			$this->load->view('admin/header');

			$total_rows = $this->Item->getCount() ;
			$base_url = INDEX_URL . '/adminItems/index/';
			$tpl['pagination'] = pagination($page,$total_rows,$base_url);


			$tpl['data'] = $this->Item->get_list($page);
			$tpl['status'] = $status;
			$tpl['page'] = $page;
			$this->load->view('admin/item/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    
			
			$this->load->helper(array('form'));
			$this->load->model('item_model','Item');
			
			$this->load->library(array('form_validation'));
			if ($this->input->post('item_update')) {

				$this->form_validation->set_rules('category_id', 'Category', 'required|xss_clean');
				$this->form_validation->set_rules('item_name', 'Item Name', 'required|xss_clean');

				$id = $this->input->post('id');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
						
					if ($this->Item->save($data)) {
						redirect('adminItems/index/1/' . UPDATE_SUCCEED , 'refresh');
						return;
					}else{
						$status = UPDATE_FAILED;
					}
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Item->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}


			// POPULATE Category DATAS
			$this->load->model('category_model','Category');
			$tpl['categories'] = $this->Category->get_list();

			
			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/item/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');

		} else {

		}
	}


	public function create() {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
			
			$this->load->helper(array('form'));
			
			$this->load->library(array('form_validation'));

			if ($this->input->post('item_create')) {

				$this->form_validation->set_rules('category_id', 'Category', 'required|xss_clean');
				$this->form_validation->set_rules('item_name', 'Item Name', 'required|xss_clean');
 				

				if($this->form_validation->run()) {
					$data = $this->input->post();
					$this->load->model('item_model','Item');
					
					if ($this->Item->save($data)) {
						redirect('adminItems/index/1/' . ADD_SUCCEED , 'refresh');
						return;
					}			
				
				} else {
					$status = ADD_FAILED;
				}

			} 

			// POPULATE Category DATAS
			$this->load->model('category_model','Category');
			$tpl['categories'] = $this->Category->get_list();

			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/item/create',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}



}
