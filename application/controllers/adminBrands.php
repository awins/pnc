<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminBrands extends CI_Controller {



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page = 1,$status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('brand_model','Brand');
			$total_rows = $this->Brand->getCount() ;
			$base_url = INDEX_URL . '/adminBrands/index/';
			$tpl['pagination'] = pagination($page,$total_rows,$base_url);

			$tpl['data'] = $this->Brand->get_list($page);
			$tpl['status'] = $status;
			$tpl['page'] = $page;
			$this->load->view('admin/header');
			$this->load->view('admin/brand/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err = NULL;
			
			$this->load->helper(array('form'));
			$this->load->model('brand_model','Brand');
			if ($this->input->post('brand_update')) {

				$this->load->library(array('form_validation'));
				$this->form_validation->set_rules('brand_name', 'Brand Name', 'required|xss_clean');
				$id = $this->input->post('id');
				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/brand' ;
				    $config['allowed_types'] = IMG_TYPE;
				    $this->load->library('upload', $config);

					if (strlen($_FILES['userfile']['name']) > 0)
					{
						$res = $this->upload->do_upload('userfile');
						if ($res) {
							$data['brand_image'] = $this->upload->file_name;
						} else {
							$upload_err = $this->upload->display_errors();
						}
					
					} 

					if (is_null($upload_err)) {
						
						if ($this->Brand->save($data)) {
							redirect('adminBrands/index/1/' . UPDATE_SUCCEED , 'refresh');
							return;
						}else{
							$status = UPDATE_FAILED;
						}
					} else {
						$status = UPDATE_FAILED;
					}				
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Brand->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}
			$tpl['status'] = $status;
			$tpl['upload_err'] = $upload_err;
			$this->load->view('admin/header');
			$this->load->view('admin/brand/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}

	public function create() {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    $upload_err = NULL;
			
			$this->load->helper(array('form'));
			$this->load->model('brand_model','Brand');
			if ($this->input->post('brand_create')) {

				$this->load->library(array('form_validation'));
				$this->form_validation->set_rules('brand_name', 'Brand Name', 'required|xss_clean');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/application/core/images/brand' ;
				    $config['allowed_types'] = IMG_TYPE ;
				    $this->load->library('upload', $config);

					
					$res = $this->upload->do_upload('userfile');
					if ($res) {
						$data['brand_image'] = $this->upload->file_name;
					} else {
						$upload_err = $this->upload->display_errors();
					}
					

					if (is_null($upload_err)) {
						
						if ($this->Brand->save($data)) {
							redirect('adminBrands/index/1/' . ADD_SUCCEED , 'refresh');
							return;
						}else{
							$status = ADD_FAILED;
						}
					} else {
						$status = ADD_FAILED;
					}				
				
				} else {
					$status = ADD_FAILED;
				}

			} 

			$tpl['status'] = $status;
			$tpl['upload_err'] = $upload_err;
			$this->load->view('admin/header');
			$this->load->view('admin/brand/create',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */