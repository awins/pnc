<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

		var $USER_195 = 'DEVPNCH119';
		var $PASS_195 =	'DEVPNCH119$1031';
		var $RETURN_URL_195 = 'http://pickenchoose.com/index.php/member/returnPayment';
		var $BILLHOST_URL = 'http://demos.finnet-indonesia.com/195/'; //DEVELOPMENT HOST
		//$BILLHOST_URL = 'https://billhosting.finnet-indonesia.com/prepaidsystem/195/'; //PRODUCTION HOST
		

	public function index()
	{
		if ($this->isAdmin()) {
			$this->load->view('admin/header');
			//$this->load->view('admin/index');
			$this->load->view('admin/footer');
		} 
	}

	public function register() {
		$tpl = array();
		if ($this->input->post('login')) {
			$this->load->model('admin_model','User');
			$user = $this->User->login($this->input->post());
			$this->load->helper(array('url'));
			if (count($user) > 0)
			{
				$this->load->library('session');
				$login['admin'] = $user[0]; 
				$this->session->set_userdata($login);
				
				redirect('admin', 'refresh');
				return;
			} else {
				$tpl['status'] = LOGIN_FAILED;
			}

		} 
		
		$this->load->view('front/__header.php');
		$this->load->view('member/register',array('tpl' => $tpl));		
		$this->load->view('front/_footer.php');
	
		
	}

	public function login() {
		$tpl = array();
		if ($this->input->post('login')) {
			$this->load->model('member_model','Member');
			$user = $this->Member->login($this->input->post());
			$this->load->helper(array('url'));
			if (count($user) > 0)
			{
				$user = $user[0];
				$this->load->library('session');
				$login['member'] = $user; 
				
				$this->load->model('sale_model','Sale');
				$sale = $this->Sale->get_by_member($user->id , 'open');
				
				if ( count($sale) > 0 ) {
					$sale = $sale[0];
					$login['member']['sale_id'] = $sale->id;
				}

				$this->session->set_userdata($login);
				
				//redirect('admin', 'refresh');
				//return;
			} else {
				$tpl['status'] = LOGIN_FAILED;
			}
			redirect ('','refresh');

		} 
	}

	public function logout()
	{
		$this->load->library('session');
		$this->load->helper(array('url'));

		$this->session->unset_userdata('member');
		redirect ('','refresh');
		
	}

	public function shoppingCart() {

		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}
		$tpl['css'][] = 'member.css';
		$tpl['js'][] = 'member.js';
		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		
		$tpl = array();
		$this->load->model('material_model','Material');
		$this->load->library('session');
		$member = $this->session->userdata('member');
		if (isset($member->shopping_cart)) {
			foreach ($member->shopping_cart as $id => $val ) {
				$item =	$this->Material->get_by_id($id);
				if ($item != false ) {
					$item = $item[0];
					$item->vol = $val['vol'];
					$tpl['data'][] = $item;
				}
			}
		}

		$this->load->view('member/shopping_cart',array('tpl' => $tpl));		
		$this->load->view('front/_footer.php');
	}


	public function addToCart() {
		$arr_id = json_decode($_GET['id']);

		$this->load->library('session');
		$member = $this->session->userdata('member');

		$sale = array();

		if (isset($member->shopping_cart)) {
			$member->shopping_cart = array();

		}

		/*$arr = array_merge_recursive($arr_id,$member->shopping_cart);
		$arr = array_unique($arr);*/
		foreach ($arr_id as $value) {
			if (array_key_exists($value, $member->shopping_cart)) {
				$member->shopping_cart[$value]['vol'] += 1; 
			} else {
				$member->shopping_cart[$value] = array('vol' => 1 ) ;
			}
		}

		//$member->shopping_cart = $arr;
		$session['member'] = $member;
		$this->session->set_userdata($session);
		return;

	}

	public function removeFromCart() {
		$arr_id = json_decode($_GET['id']);
		$this->load->library('session');
		$member = $this->session->userdata('member');

		if (!isset($member->shopping_cart)) {
			return;
		}

		foreach ($arr_id as $value) {
			if (array_key_exists($value, $member->shopping_cart)) {
				unset($member->shopping_cart[$value]); 
			} 
		}

		$session['member'] = $member;
		$this->session->set_userdata($session);
		return;

	}

	public function editCartVol($item_id,$new_vol,$price) {

		$this->load->library('session');
		$member = $this->session->userdata('member');

		if (!isset($member->shopping_cart)) {
			return;
		}

		$member->shopping_cart[$item_id]['vol'] = $new_vol; 
			
		$session['member'] = $member;
		$this->session->set_userdata($session);
		echo 'Rp. ' . number_format($new_vol * $price) ;
		return ;
	}

	public function returnPayment() {
		$this->load->helper('payment');
		$this->load->model('sales_model','Sales');
		
		//TO WRITE LOG POST FROM 195 RESPON
		$log = '';
		foreach($_POST as $name=>$value){
			$_POST[$name]=htmlspecialchars(strip_tags(trim($value)));
			$log .= $name.' : '.htmlspecialchars(strip_tags(trim($value))).' ';
		}

		extract($_POST);
		$mer_signature = $_POST["mer_signature"];

			$data = array();
			$data['sale_id'] = substr(rand(), 1,7);
		if(check_mer_signature($mer_signature,$_POST,$this->PASS_195)){ //SECURE DATA
			$data['payment_code'] = $payment_code;
		} else {
			$data['notes'] = $log;
		}
			$this->Sales->save($data);

	}

	public function processPayment() {
		$rand =  time() . rand();
		$rand = md5($rand);
		$rand = substr($rand, 4, 8);
		$sellCode = $rand;

		$this->load->library('session');
		$this->load->model('material_model','Material');
		$member = $this->session->userdata('member');


		if (!isset($member->shopping_cart)) {
			return;
		}		

		$cart = $member->shopping_cart;
		$total_price = 0;
		foreach ($cart as $key => $value) {
			$item =	$this->Material->get_basic_by_id ($key);
			$total_price += $value['vol'] *  $item->reg_price ;
			
		}

		// REQUESTING PAYMENT CODE

		$REQUEST_URL_195 = $this->BILLHOST_URL.'response-insert.php';
		$CHECK_STATUS_URL_195 = $this->BILLHOST_URL.'check-status.php';
		$CANCEL_URL_195 = $this->BILLHOST_URL.'cancel-transaction.php';
		


		$this->load->helper('payment');

		$mer_password = $this->PASS_195; //IMPORTANT!
		$postdata = array(
			'merchant_id' => $this->USER_195,  //IMPORTANT!
			'invoice' => $sellCode ,  //IMPORTANT!
			'amount' => $total_price,  //IMPORTANT!
			'add_info1' => $member->member_name,  //Customer Name //IMPORTANT!
			'timeout' => '120' , //60 Menit (Expired Date)  //IMPORTANT!
			'return_url' => $this->RETURN_URL_195  //IMPORTANT! CHANGE THIS WITH YOUR RETURN TARGET URL!!!
		);
		$mer_signature =  mer_signature($postdata).$mer_password;  //IMPORTANT!
		/* END CREATE SIGANTURE */
		
		/* DATA FOR SENT */
		$postdata = array(
			'mer_signature' => strtoupper(hash256($mer_signature)),  //IMPORTANT!
			'merchant_id' => $postdata['merchant_id'],  //IMPORTANT!
			'invoice' => $postdata['invoice'],  //IMPORTANT!
			'amount' => $postdata['amount'],  //IMPORTANT!
			'add_info1' => $postdata['add_info1'], //Customer Name //IMPORTANT!
			'timeout' => $postdata['timeout'], //IMPORTANT!
			'return_url' => $postdata['return_url']  //IMPORTANT!
		);
	/* END DATA FOR SENT */
	
	//todo INSERT DATA TO DATABASE
	
	
	//SENT DATA VIA CURL
	$respon = curl_post($REQUEST_URL_195, $postdata);

	var_dump($respon);
	echo '<br>';
	var_dump($postdata);

		//$payment_code = 'TEST0987';

		/*$tpl['payment_code'] = $payment_code;
		$this->load->view('front/__header.php');
		$this->load->view('member/finish_shopping',array('tpl' => $tpl));		
		$this->load->view('front/_footer.php');*/




	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */