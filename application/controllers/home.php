<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->model('content_model','Content');
		$this->load->model('material_model','Material');

		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}
		
		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		$tpl = array();
		$tpl['data'] =	$this->Content->get_active_item();
		$tpl['latest_items']	= $this->Material->get_latest_items(6);
		$this->load->view('front/home.php',array('tpl' =>$tpl)); 
		$this->load->view('front/_footer.php');
	}	

	public function contactUs() {
		$tpl = array();
		$this->load->library('form_validation');
		$this->load->helper('form');
		if ($this->input->post('submit-comment')) {
			$this->form_validation->set_error_delimiters('<label class="error">* ', '</label>');

			$this->form_validation->set_rules('name','Name','required|xss_clean');
			$this->form_validation->set_rules('email','Email','required|valid_email|xss_clean');
			$this->form_validation->set_rules('message','Message','required|xss_clean');

			if($this->form_validation->run()) {
				$this->load->model('contact_model','Contact');
				$data = $this->input->post();
				$data['input_dt'] = time();
				$this->Contact->save($data);
				$tpl['status'] = 200;
			} else {
				$tpl['status'] = 100;
			}

		}

		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		$this->load->view('front/contactUs.php',array('tpl' =>$tpl)); 
		$this->load->view('front/_footer.php');
	}

	public function editorsPick() {

		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}
		$tpl['css'][] = 'front-editor-pick.css';
		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		
		$this->load->model('content_model','Content');
		$tpl['data'] =	$this->Content->get_active_item('editors_pick');

		$this->load->view('front/editorPick.php',array('tpl' =>$tpl )); 
		$this->load->view('front/_footer.php');
	}

	public function todaysPromo() {
		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}

		$tpl['css'][] = 'front-today-promo.css';
		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		
		$tpl = array();

		$this->load->model('content_model','Content');
		$tpl['data'] =	$this->Content->get_active_item('todays_promo');

		$this->load->view('front/todaysPromo.php',array('tpl' =>$tpl )); 
		$this->load->view('front/_footer.php',array('tpl' => $tpl));
	}

	public function newRelease()
	{
		$section = 'new_release';
		$this->load->library('session');
		if ($this->isLoggedIn(false)) {
			$tpl['member'] = $this->session->userdata('member');
		} else {
			$tpl['member'] = NULL;
		}

		$tpl['css'][] = 'front-new-release.css';

		$this->load->view('front/__header.php',array('tpl' =>$tpl));
		
		$tpl = array();

		$this->load->model('content_model','Content');
		$tpl['data'] =	$this->Content->get_active_item($section);

		$this->load->view('front/newRelease.php',array('tpl' =>$tpl )); 
		$this->load->view('front/_footer.php',array('tpl' => $tpl));
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */