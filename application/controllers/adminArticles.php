<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminArticles extends CI_Controller {


	public function index($page = 1,$status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('article_model','Article');
			$tpl['menu']['header'] = 'adminArticles';
			$tpl['menu']['child'] = 'list';
			$this->load->view('admin/header');
			$total_rows = $this->Article->getCount() ;
			$base_url = INDEX_URL . '/adminArticles/index/';
			$tpl['pagination'] = pagination($page,$total_rows,$base_url);

			$tpl['data'] = $this->Article->get_list($page);
			$tpl['status'] = $status;
			$tpl['page'] = $page;
			$this->load->view('admin/article/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    
			
			$this->load->helper(array('form'));
			$this->load->model('article_model','Article');
			
			$this->load->library(array('form_validation'));
			if ($this->input->post('article_update')) {

				$this->form_validation->set_rules('item_id', 'Item', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
				$this->form_validation->set_rules('code', 'Code', 'required|xss_clean');

				$id = $this->input->post('id');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
						
					if ($this->Article->save($data)) {
						redirect('adminArticles/index/1/' . UPDATE_SUCCEED , 'refresh');
						return;
					}else{
						$status = UPDATE_FAILED;
					}
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Article->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}


			// POPULATE ITEM DATAS
			$this->load->model('item_model','Item');
			$tpl['items'] = $this->Item->get_list();

			
			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/article/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');

		} else {

		}
	}


	public function create() {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
			
			$this->load->helper(array('form'));
			
			$this->load->library(array('form_validation'));

			if ($this->input->post('article_create')) {

				$this->form_validation->set_rules('item_id', 'Item', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
				$this->form_validation->set_rules('code', 'Code', 'required|xss_clean');
 				

				if($this->form_validation->run()) {
					$data = $this->input->post();
					$this->load->model('article_model','Article');
						
					if ($this->Article->save($data)) {
						redirect('adminArticles/index/1/' . ADD_SUCCEED , 'refresh');
						return;
					}			
				
				} else {
					$status = ADD_FAILED;
				}

			} 

			// POPULATE ITEM DATAS
			$this->load->model('item_model','Item');
			$tpl['items'] = $this->Item->get_list();

			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/article/create',array('tpl' => $tpl));		
			$this->load->view('admin/footer');
		} else {

		}
	}

}
