<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminMembers extends CI_Controller {



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page = 1,$status = NULL)
	{
		if ($this->isAdmin()){
			$this->load->model('member_model','member');
			$tpl['menu']['header'] = 'adminMembers';
			$tpl['menu']['child'] = 'list';
			$this->load->view('admin/header');

			$total_rows = $this->member->getCount() ;
			$base_url = INDEX_URL . '/adminMembers/index/';
			$tpl['pagination'] = pagination($page,$total_rows,$base_url);


			$tpl['data'] = $this->member->get_list($page);
			$tpl['status'] = $status;
			$tpl['page'] = $page;
			$this->load->view('admin/member/list',array('tpl' => $tpl));
			$this->load->view('admin/footer');
		} else {
			
		}
		
	}

	public function update($id = 0) {

		if ($this->isAdmin()){
			$status = NULL;
			$tpl = NULL;
		    
			
			$this->load->helper(array('form'));
			$this->load->model('member_model','Member');
			
			$this->load->library(array('form_validation'));
			if ($this->input->post('member_update')) {

				$this->form_validation->set_rules('member_name', 'Member Name', 'required|xss_clean');
				$this->form_validation->set_rules('address', 'Address', 'required|xss_clean');
				$this->form_validation->set_rules('city', 'City', 'required|xss_clean');
				$this->form_validation->set_rules('telp', 'Telp', 'required|xss_clean');
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean');

				$id = $this->input->post('id');
 				
				if($this->form_validation->run()) {
					$data = $this->input->post();
					
						
					if ($this->Member->save($data)) {
						redirect('adminMembers/index/1/' . UPDATE_SUCCEED , 'refresh');
						return;
					}else{
						$status = UPDATE_FAILED;
					}
				
				} else {
					$status = UPDATE_FAILED;
				}

			} else {
				$tpl['data'] = $this->Member->get_by_id($id);
				if (count($tpl['data']) == 0) {
					$status = DATA_NOT_FOUND;
				}
				
			}


			$tpl['status'] = $status;
			$this->load->view('admin/header');
			$this->load->view('admin/member/update',array('tpl' => $tpl));		
			$this->load->view('admin/footer');

		} else {

		}
	}


	public function sales($status = NULL, $member_id = NULL) {

		$tpl['css'][] = 'member.css';
		$tpl['menu']['header'] = 'adminMembers';
		$tpl['menu']['child'] = 'sale';
		$this->load->view('admin/header',array('tpl' => $tpl));
		
		$tpl = array();
		$this->load->model('material_model','Material');
		$this->load->model('sale_model','Sale');
		$tpl['statuses'] = $this->Sale->status;
		
		$sale = $this->Sale->get_list();
		foreach ($sale as $cart ) {
			$item =	$this->Material->get_by_id($cart->material_id);
			if ($item != false ) {
				$item = $item[0];
				$item->sale = $cart;
				$tpl['data'][] = $item;
			}
		}
		

		$this->load->view('admin/member/sales',array('tpl' => $tpl));		
		$this->load->view('admin/footer');
		
	}





}
