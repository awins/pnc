(function ($) {

	$(function () {
		//var img_buffer = {};
		var current_display = "front";
		var current_category = 1 ;
		var current_item = 0;		
		var current_id = 0;		


		function clearSelected() {
			$('.display_'+ current_display + '_img').hide();
			$('.detail-item').hide();
			$('.thumbnail_img').removeClass('selected');	
			$('.change_view').removeClass('selected');	

		}

		function showSelected() {
			var img	 = $('#display_' + current_display + '_' + current_id ).attr('rel');
			var is_new = $('#is_new_' +  current_id ).attr('rel');
			$('#no-collection-msg').hide();
			$('#change-' + current_display ).addClass('selected');
			$('#thumbnail_' + current_id).addClass('selected');
			$('#detail_' + current_id ).show();

			if ($('#thumbnail_' + current_id ).hasClass('in-Cart')) {
				$('#display-item').addClass('in-Cart');
				$('#addToCart').addClass('selected');
			} else {
				$('#addToCart').removeClass('selected');
				$('#display-item').removeClass('in-Cart');
			}
			console.log(img);
			$('#display-item').css('background-image','url(' + img + ')');
			if (is_new == 1 ){ 
				$('#display-item').addClass('is-New');
			} else {
				$('#display-item').removeClass('is-New');
			}

		}

		function loadPncOnlyPage(category_id, item_id, page_number ) {
			var url = "http://"+window.location.hostname + "/index.php/pncOnly/getPncOnlyPage/" + category_id + "/" + item_id + "/" + page_number ;
			console.log('load pncOnly page');
			current_id = 0;
			$('#category_' + category_id).addClass('selected');
			$.post( url , {}).done(function(data) {

				$('#box-content').html(data);
				current_category = category_id;
				current_item = item_id;
				$('.tabCategory').click(function() {
					var category_id = $(this).attr('id');
					category_id = category_id.replace('category_','');
					//$('.thumbnail').html('<div style="height:80px">&nbsp;</div> <img src="' + loader + '"> ');
					loadPncOnlyPage(category_id,0,1);

				});

				$('.category_head').click(function() {
					var category_id = $(this).attr('id');
					category_id = category_id.replace('category_head_','');
					//$('.thumbnail').html('<div style="height:80px">&nbsp;</div> <img src="' + loader + '"> ');
					loadPncOnlyPage(category_id,0,1);

				});

				$('.category_item').click(function() {
					var category_id = $(this).attr('rel');

					var item_id = $(this).attr('id');
					item_id = item_id.replace('category_item_','');

					//$('.thumbnail').html('<div style="height:80px">&nbsp;</div> <img src="' + loader + '"> ');
					loadPncOnlyPage(category_id,item_id,1);

				});


				$('.thumbnail_img').click(function() {
					current_id  = $(this).attr('id').replace('thumbnail_','');
					clearSelected();
					showSelected();
					console.log(current_id);

				});

				$('.change_view').click(function() {
					clearSelected();
					current_display = $(this).attr('id').replace('change-','');
					showSelected();
				});
				
				$('#addToCart').click(function() {
					console.log(current_id);
					var member_id = $('#member_id').val();

					if (Number(member_id) == 0 ) {
						window.location.assign("/index.php/member/register");
						return;
					}

					if (current_id == null || current_id == 0 ) {
						return;
					}

					if ($(this).hasClass('selected')) {
						return;
					}

					if (Number($('#stock_val_' + current_id).html()) == 0 ) {
						window.alert('Maaf, Stok item ini sedang kosong');
						return;
					}

					var url = "http://"+window.location.hostname + "/index.php/member/addToCart?id=[" + current_id + "]" ;
					$.post( url , {}).done(function(data) {
						$('#addToCart').addClass('selected');
						$('#thumbnail_' + current_id ).addClass('in-Cart');
						$('#display-item').addClass('in-Cart');


					});
				});

				$('#showCart').click(function() {
					window.location.assign("/index.php/member/shoppingCart");
				});


				$('#page-next').click(function() {
					if ($(this).hasClass('disabled') ) {
						return;
					}
					var page_number = Number ($('#page_number').val()) + 1;
					loadPncOnlyPage(current_category,current_item, page_number);

				});

				$('#page-prev').click(function() {
					if ($(this).hasClass('disabled') ) {
						return;
					}
					var page_number = Number ($('#page_number').val()) - 1;
					loadPncOnlyPage(current_category,current_item, page_number);

				});
				

			});
		}


	
		$(document).ready(function() {
			current_id =0;
			loadPncOnlyPage(1,0,1);


		});


	});

})(jQuery);