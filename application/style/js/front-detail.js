(function ($) {

	$(function () {
		//var img_buffer = {};
		var current_display = "front";
		var current_id = 0;
		var current_title = null;
		var current_rate = 0;

		function clearSelected() {
			$('.display_'+ current_display + '_img').hide();
			$('.detail-item').hide();
			$('.thumbnail_img').removeClass('selected');	
			$('.change_view').removeClass('selected');	

		}

		function showSelected() {
			current_title = $('#title_' + current_id).html();
			$('#display_' + current_display + '_' + current_id ).show();
			$('#thumbnail_' + current_id ).addClass('selected');
			$('#change-' + current_display ).addClass('selected');
			$('#detail_' + current_id ).show();

			if ($('#thumbnail_' + current_id ).hasClass('in-Cart')) {
				$('#addToCart').addClass('selected');
			} else {
				$('#addToCart').removeClass('selected');
			}

			var stock = $('#stock_holder_' + current_id).val();
			$('#detail-stock').html(stock );

			var spec = $('#spec_holder_' + current_id).val();
			$('#detail-spec').html(spec);

			var info = $('#info_holder_' + current_id).val();
			$('#detail-info').html(info );
			
		}

		function filled_star(id) {
			for (var i = id; i >= 1; i--) {
				$('#star_' + i).addClass('filled');
				$('#star_' + i).removeClass('empty');
			};

			for (var i = 5; i > id; i--) {
				$('#star_' + i).addClass('empty');
				$('#star_' + i).removeClass('filled');
			};
		}

		function bind_post_review() {
			$('#submit-review').click(function() {
				var url = "http://"+window.location.hostname + "/index.php/material/post_review/" + current_id ;
				var form = $('#form_review');
				$.post(url,form.serialize()).done(function(data) {
					$('#box-product_review').html(data);
					if ($('#recaptcha-box')) {
						Recaptcha.create("6LdtnO0SAAAAAHWDFPEAWr3KoWprNGSZcIltf61j","recaptcha-box",
						    {
						      theme: "red",
						      callback: Recaptcha.focus_response_field
						    }
					  	);
					}
					$('#call-review').click(function() {
						$("#product_review").triggerHandler("click");
					});
					filled_star(current_rate);
					bind_post_review();
				});

			});

			$('.rating_star').mouseenter(function() {
				var id = $(this).attr('id').replace('star_','');
				filled_star(id);
			});	

			$('.rating_star').mouseout(function() {
				filled_star(current_rate);
			});	

			$('.rating_star').click(function() {
				var id = $(this).attr('id').replace('star_','');
				$('#rate').val(id);
				current_rate = id;

			});
		}

		function show_review(page){
			$('.pagination').removeClass('selected');
			$('#pagination_' + page).addClass('selected');
			var url = "http://"+window.location.hostname + "/index.php/material/detail_review_content/" + current_id + '/' + page ;
			$.post( url , {}).done(function(data) {
				$('#box-review').html(data);

				$('#post-review-button').click(function() {
					if (!$(this).hasClass('active')) return;

					$('#box-product_review').attr('filled','');
					current_rate = 0;
					var url = "http://"+window.location.hostname + "/index.php/material/post_review/" + current_id;
					$.post(url,{}).done(function(data) {
						$('#box-product_review').html(data);
						$('#material_id').val(current_id);
						
						Recaptcha.create("6LdtnO0SAAAAAHWDFPEAWr3KoWprNGSZcIltf61j","recaptcha-box",
						    {
						      theme: "red",
						      callback: Recaptcha.focus_response_field
						    }
					  	);
						bind_post_review();

					});

				});
			});
		}
	
		$(document).ready(function() {
			current_id = $('#material_id').val();
			console.log('ready86 =' + current_id );

			showSelected();
			$('.tabMenu').click(function() {

				$('.tabMenu').removeClass('selected');

				$(this).addClass('selected');
				var menu_id = $(this).attr('id');
				var brand_id = $('#brand_id').val();
				var url = null;
				var box = null;

				switch (menu_id) {
					case 'product_info':
						url = '';
						box = "#box-product_info";
						break;
					case 'brand_info':
						url = "http://"+window.location.hostname + "/index.php/material/detail_brand_info/" + brand_id ;
						box = "#box-brand_info";
						break;
					case 'product_review':
						url = "http://"+window.location.hostname + "/index.php/material/detail_review/" + current_id + '/' + current_title;
						box = "#box-product_review";
						break;
				}

				// only fill the box just if it is still empty, else show/hide them for sake of efficiency


				console.log($(box).attr('filled'));
				if ($(box).attr('filled') !='true' ) // box still empty
				{
					$('#box-product_info').hide();
					$('#box-brand_info').hide();
					$('#box-product_review').hide();
					$(box).html('');
					$(box).show();

					$.post( url , {}).done(function(data) {

						$(box).html(data);
						$(box).attr('filled','true');
						
						if (box == '#box-product_review' ) {
							show_review(1);

							$('.pagination').click(function() {
								var rel = $(this).attr('id').replace('pagination_','');
								console.log(rel);
								
								if (rel == 'first') {
									return;
								}

								if (rel == 'back') {
									return;
								}

								if (rel == 'next') {
									return;
								}

								if (rel == 'last') {
									return;
								}
								show_review(rel);

							});
						}

					});
				} else {
					$('#box-product_info').hide();
					$('#box-brand_info').hide();
					$('#box-product_review').hide();
					$(box).show();
				}


			});


			$('.thumbnail_img').click(function() {
				if ($(this).attr('id').replace('thumbnail_','') != current_id){
					$('#box-product_review').attr('filled','');
				}
				current_id  = $(this).attr('id').replace('thumbnail_','');
				clearSelected();
				showSelected();

			});

			$('.change_view').click(function() {
				clearSelected();
				current_display = $(this).attr('id').replace('change-','');
				showSelected();
			});
			
			$('#addToCart').click(function() {
				console.log(current_id);
				var member_id = $('#member_id').val();

				if (Number(member_id) == 0 ) {
					window.location.assign("/index.php/member/register");
					return;
				}

				if (current_id == null || current_id == 0 ) {
					return;
				}

				if ($(this).hasClass('selected')) {
					return;
				}

				if (Number($('#stock_val_' + current_id).html()) == 0 ) {
					window.alert('Maaf, Stok item ini sedang kosong');
					return;
				}

				var url = "http://"+window.location.hostname + "/index.php/member/addToCart?id=[" + current_id + "]" ;
				$.post( url , {}).done(function(data) {
					$('#addToCart').addClass('selected');
					$('#thumbnail_' + current_id ).addClass('in-Cart');

				});
			});

			$('#showCart').click(function() {
				window.location.assign("/index.php/member/shoppingCart");
			});

		});


	});

})(jQuery);