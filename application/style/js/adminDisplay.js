(function ($) {
	$(function () {
		var curInd;
		var curID;
		function slideIt()
		{
			for (ind=1;ind<=3;ind++) {
				var slidingDiv = document.getElementById("img_" + curID + "_" + ind);
				var stopPosition = ((ind - 1) * 180) -  (curInd * 180) ;
				if (parseInt(slidingDiv.style.left) > stopPosition )
				{
					slidingDiv.style.left = (parseInt(slidingDiv.style.left ) - 10) + "px";
				}
			}
			setTimeout(slideIt, 1);
		}

		function refreshNav(id,rel) {
			console.log(rel);

			if (parseInt(rel) == 1 ) {
				$('#prev_' + id ).attr('disabled','disabled');
				$('#next_' + id ).attr('disabled',false);
			}

			if (parseInt(rel) == 2 ) {
				$('#prev_' + id ).attr('disabled',false);
				$('#next_' + id ).attr('disabled',false);
			}

			if (parseInt(rel) == 3 ) {
				$('#prev_' + id ).attr('disabled',false);
				$('#next_' + id ).attr('disabled','disabled');
			}
		}

		$(document).ready(function() {
			console.log('ready945');

			$('.btnUpload').live('click' ,function() {
				var category_id = $(this).attr('category_id');
				var material_id = $(this).attr('material_id');

				var url = "/index.php/adminMaterials/upload/" + category_id + '/' + material_id ;
				window.location.assign(url );


			});

			$('.btnShow').live('click', function()  {
				var thumbnail =	$(this).attr('thumbnail') ;
				var front =	$(this).attr('rear') ;
				var rear = 	$(this).attr('front') ;
				var title = $(this).attr('title') ;

				$('#show_thumbnail').attr('src', thumbnail );
				$('#show_front').attr('src', front );
				$('#show_rear').attr('src', rear );
				$('#title_label').html(title);
				$( "#images-popup" ).modal();
				$( "#images-popup" ).show();

			});

			$('.btnShow-suggestion').live('click', function()  {
				var img_path =	$(this).attr('img_path') ;
				var suggestion =	$(this).attr('suggestion') ;
				var title = $(this).attr('title') ;
				
				var images = '';

				suggestion = suggestion.split(";");
				console.log(suggestion);
				for (i=0;i< suggestion.length;i++ )
				{
					if(suggestion[i] != '' ) {
						images += '<div style="width:170px;float:left;" ><img  src="' + img_path + suggestion[i]  + '" /></div>';
					}
				}
				if (images == '') {
					images = '<div style="width:170px;float:left;" ><img  src="' + img_path + 'no-photo.png" /></div>' ;
				}
				console.log(images);
				$('#suggestion-box').html(images);

				$('#title_label').html(title);
				$( "#images-popup" ).modal();
				$( "#images-popup" ).show();

			});

			$('#close-popup').live('click', function()  {
				$.modal.close();
				$('#images-popup').hide();
			});

			$('.display_gallery').click(function() {
		    	var id = $(this).attr('id');
		    	var material_id = id.replace('display_gallery_', '');
				var val = ($(this).is(':checked')) ? 1 : 0 ;
				var url = "http://"+window.location.hostname;
	    		$.post( url + "/index.php/adminMaterials/updateDisplay/gallery/" +  material_id + '/' + val , {}).done(function(data) {
					if (Number(data) == 100) {
						$('input.#' + id).prop('checked', false);
						alert('silakan lengkapi gambar-gambar GALLERY terlebih dahulu');
					} 
				});

		    });



		    $('.display_mixmatch').click(function() {
		    	var id = $(this).attr('id');
		    	var material_id = id.replace('display_mixmatch_', '');
				var val = ($(this).is(':checked')) ? 1 : 0 ;
				var url = "http://"+window.location.hostname;
	    		$.post( url + "/index.php/adminMaterials/updateDisplay/mixmatch/" +  material_id + '/' + val , {}).done(function(data) {
					if (Number(data) == 100) {
						$('input.#' + id).prop('checked', false);
						alert('silakan lengkapi gambar-gambar MIX & MATCH terlebih dahulu');
					} 
				});

		    });

		    $('.select_page').change(function () {
		    	var page_type = $('#page_type').val();
		    	var category_id = $('#select_category').val();
		    	var filter_display = $('#select_display').val();
				//window.location.assign("/index.php/adminMaterials/shoppingCart");
				window.location.assign("/index.php/adminMaterials/display/" + page_type + "/" + category_id + "/" + filter_display );
		    });

			/*$('.next').live('click', function() {
				if ($(this).attr('disabled')) {
					return;
				}
				rel = $(this).attr('rel');
				id = $(this).attr('id');
				id = id.replace('next_','');

				console.log('click');
				curInd = rel ;
				curID = id;

				slideIt()
				rel = parseInt(rel)+1;
				
				$(this).attr('rel',rel);
				
				refreshNav(id,rel);

			});

			$('.prev').live('click', function() {
				if ($(this).attr('disabled')) {
					return;
				}
				rel = $(this).attr('rel');
				id = $(this).attr('id');
				id = id.replace('next_','');

				console.log('click');
				curInd = rel ;
				curID = id;

				slideIt()
				rel = parseInt(rel)+1;
				
				$(this).attr('rel',rel);
				
				refreshNav(id,rel);

			});*/


		});






});
})(jQuery);