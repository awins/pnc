(function ($) {

	$(function () {


		function editCartVol(id,old_vol,action) {
			var price_item = $("#item_price_" + id);
			var price = price_item.html().replace('.','').replace('Rp','').replace(',','').replace('Price','' ).replace(':','').replace(' ','') ;
			price = Number(price);

			var vol = old_vol;
			if (action == 'add') {
				vol++;
			} else {
				vol--;
			}
			console.log(vol);
			console.log(price);
			
			var url = "http://"+window.location.hostname + "/index.php/member/editCartVol/" + id + "/" + vol  + "/" + price ;
			$.post( url , {}).success(function(data) {
				$('#vol_' + id).html(vol);
				$("#total_item_" + id).html(data);

				var arr_price = $('.total_item'),
					total_price = 0;



				for (var i=0;i<arr_price.length;i++) {
					var item = arr_price[i];
					price = $(item).html().replace('.','').replace('Rp','').replace(',','').replace('Price','' ).replace(':','').replace(' ','') ;
					price = Number(price);
					total_price += price;
				}

				
				var url = "http://"+window.location.hostname + "/index.php/member/setTotalSaleAmount/" + total_price ;
				$.post( url , {}).success(function(data) {
					$('#total_price').html('Rp. ' + Number(data).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").replace('.00',''));
				});




			});
		}

		$(document).ready(function() {
			console.log('ready458');
			

			$('.undoCart-box').click(function() {
				var id = $(this).attr('rel');
				var this_ = $(this);
				if ($(this).attr('status') == 'added' ) {
					var url = "http://"+window.location.hostname + "/index.php/member/removeFromCart?id=[" + id + "]" ;
					$.post( url , {}).done(function(data) {
						$("#undo_" + id).addClass('selected');
						//$("#undoLabel_" + id).html('ADD SHOPPING CART');
						this_.attr('status','removed');
					});
				} else {
					/*var url = "http://"+window.location.hostname + "/index.php/member/addToCart?id=[" + id + "]" ;
					$.post( url , {}).done(function(data) {

						$("#undo_" + id).removeClass('selected');
						//$("#undoLabel_" + id).html('UNDO SHOPPING CART');
						this_.attr('status','added');
					});*/
				}		
			});

			
			$('.item_vol').hover(function(){
				var id = $(this).attr('id').replace('item_vol_','');
				$('#up_' + id).show();
				$('#down_' + id).show();

			});

			$('.item_vol').mouseleave(function(){
				$('.item_up' ).hide();
				$('.item_down' ).hide();

			});

			$('.item_up').click(function(){
				var id = $(this).attr('id').replace('up_','');
				var item =	$('#vol_' + id);
				var vol = Number(item.html()) ;
				editCartVol(id,vol,'add');
			});

			$('.item_down').click(function(){
				var id = $(this).attr('id').replace('down_','');
				var item =	$('#vol_' + id);
				var vol = Number(item.html()) ;
				if (vol <= 1) {
					return;
				}
				editCartVol(id,vol,'less');
				
			});

			$('#continue-shopping').click(function() {
				window.location.assign("/index.php/gallery");
			});

			$('input.input_pass').bind('keyup',function(e) {
				var pass = $('#user_password').val();
				var c_pass = $('#conf_password').val();
				if ((pass == c_pass) && pass != '' && c_pass != '' ) {
					$('#pass_check').show();
				} else {
					$('#pass_check').hide();
				}

			});

			$('input#email').val('');
			$('input#user_password').val('');
			$('input#conf_password').val('');



		});

	});
})(jQuery);;