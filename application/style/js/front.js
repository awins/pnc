
(function ($) {

	$(function () {
		$('#slider').cycle({ 
	        fx:     'scrollHorz', 
	        speed:  'slow', 
	        next:   '#next', 
	        prev:   '#prev' 
	    });
		
		$('#login').click(function() {
			form = document.getElementById('loginForm');
			form.submit();
		});

		$('#logout').click(function() {
			window.location.assign("/index.php/member/logout");

		});

	});

})(jQuery);