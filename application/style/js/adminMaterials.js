(function ($) {
	$(function () {

		function populateMaterials()
		{
			var category_id = $('#select_category').val();
			var material_id = $('#material_id').val();
			var url = "http://"+window.location.hostname + "/index.php/adminMaterials/list_material/" + category_id ;

			$('#material_id').val(0);

			$.post( url , {}).done(function(data) {

				$('#item_list_box').html(data);

				if (Number (material_id) != 0  ) {
					var tr = $('.table-list tr.#' + material_id  );
					if ( tr.length > 0 ) {
					    tr.addClass('selected');
					    populateImage(material_id);
					}
				}


				$('.table-list tr').click(function() {
			        $('.table-list tr').removeClass('selected');
			        $(this).addClass('selected');

			       	material_id = $(this).attr('id') ;
			       	populateImage(material_id);

			    });

				$('.table-list tr').hover(function() {
					$('.table-list tr').removeClass('hover');
			        $(this).addClass('hover');
			    });

			});
		}

		function populateImage(material_id)
		{
			var url = "http://"+window.location.hostname + "/index.php/adminMaterials/detail_material/" + material_id
			$.post( url , {}).done(function(data) {
				$('#item_image_box').html(data);

				$('#display_gallery').click(function() {
					var val = ($('input.#display_gallery').is(':checked')) ? 1 : 0 ;
					console.log(val);
					var url = "http://"+window.location.hostname + '/index.php/adminMaterials/' ;

		    		$.post(url + "updateDisplay/gallery/" +  material_id + '/' + val , {}).done(function(data) {
						if (Number(data) == 100) {
							alert('silakan lengkapi gambar-gambar GALLERY terlebih dahulu');
							$('input.#display_gallery').prop('checked', false);
						} 
					});

			    });

			    $('#display_pnc').click(function() {
					val = ($('input.#display_pnc').is(':checked')) ? 1 : 0 ;
					console.log(val);
					var url = "http://"+window.location.hostname + '/index.php/adminMaterials/' ;

		    		$.post(url + "updateDisplay/pnc/" +  material_id + '/' + val , {}).done(function(data) {
						if (Number(data) == 100) {
							alert('silakan lengkapi gambar-gambar GALLERY terlebih dahulu');
							$('input.#display_pnc').prop('checked', false);
						} 
					});

			    });

			    $('#display_mixmatch').click(function() {
					val = ($('input.#display_mixmatch').is(':checked')) ? 1 : 0 ;
					console.log(val);
					var url = "http://"+window.location.hostname + '/index.php/adminMaterials/' ;
		    		$.post(url + "updateDisplay/mixmatch/" +  material_id + '/' + val , {}).done(function(data) {
						if (Number(data) == 100) {
							alert('silakan lengkapi gambar-gambar MIX & MATCH terlebih dahulu');
							$('input.#display_mixmatch').prop('checked', false);
						} 
					});

			    });

			    $('#display_suggestion').click(function() {
					val = ($('input.#display_suggestion').is(':checked')) ? 1 : 0 ;
					console.log(val);
					var url = "http://"+window.location.hostname + '/index.php/' ;
		    		$.post( url + "adminMaterials/updateDisplay/suggestion/" +  material_id + '/' + val , {}).done(function(data) {
						if (Number(data) == 100) {
							alert('silakan lengkapi sedikitnya 1 gambar Suggestion Style terlebih dahulu');
							$('input.#display_suggestion').prop('checked', false);
						} 
					});

			    });

			    

				
			});
		}


		$(document).ready(function() {
			console.log('ready911');

			if($('#item_list_box').length  > 0) {
				populateMaterials();
			}

			$(".menu-item").hover( function() {

				console.log('hover');
				menu_id = $(this).attr("id");
				sub_menu = $('#sub-' + menu_id );
				if (sub_menu) {
					$(".sub-menu").hide();
					sub_menu.show();
				}

			});

			$('#select_category').change(function() {
				populateMaterials();
				
		    });


			$('.btn-upload').live('click', function() {
				var rel = this.getAttribute("rel");
				document.getElementById('doc_rel').value = rel;
				console.log(rel);
				$('#btnfile').click();
				return;
				$('.btn-upload').prop('disabled', true);
				//$('.btn-upload').prop('disabled', false);

			});

			$('#btnfile').live('change', function(){ 
				console.log('uploading');
				var rel = document.getElementById('doc_rel').value;
			 	/*var	loader  = document.getElementById('loader-' + rel);
			 	loader.style.display = '';*/
			 	if (rel == 'S_') {
					var img  = document.getElementById('noImage');
			 	} else {
					var img  = document.getElementById(rel);
			 	}
				img.src = "http://pnc.local/application/style/images/loader.gif"
				var url = "http://"+window.location.hostname + '/index.php/adminMaterials/uploadImage';
				$("#form_test").ajaxSubmit({
					url: url,
					type: 'post',
					target: '#crConts',
					success: function(data) {
						if (Number(data) == 100) 
						{
							
						} else {
				        	
							var rel = document.getElementById('doc_rel').value;
							if (rel == 'S_') {
					        	$('#divnoImage').remove();
					        	var div  = '<div style=" float:left;" class="image-thumbnail"  ><img  src="'+  data +  '"/></div><div style=" float: left;"  id="divnoImage" ><img  src="" id ="noImage" /></div>' ,
					        	divimg = document.getElementById('div_suggestion');
			        			divimg.innerHTML += div;

							} else {
								var img  = document.getElementById(rel);
								img.src = data;// "http://pnc.local/application/style/images/loader.gif"
								$(img).attr('src', $(img).attr('src')+'?'+Math.random());
							}

						}
					}
				});
			});

			$('.radio_update').click(function() {
				var material_id = $(this).attr('id').replace('radio_new_','') ;
				var material_id = material_id.replace('radio_latest_','') ;
				var field = $(this).attr('rel');
				var val = ($(this).is(':checked')) ? 1 : 0 ;
				
				var url = "http://"+window.location.hostname + '/index.php/adminMaterials/' ;
	    		$.post(url + "updateField/" +  material_id + '/' + field + '/' + val , {}).done(function(data) {
					console.log('update success'); 
				});
			});





			

		});


	});
})(jQuery);