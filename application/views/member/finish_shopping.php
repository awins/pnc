<? 
    $payment_code = $tpl['payment_code'];
    $error = $tpl['error'];
 ?>

<div style="width: 1043px;height:auto; text-align: left;font-size:14px;background-color: white;padding: 40px 40px 40px 40px" >
	<? if ($error == 0) { ?>
   		<span style="">KODE PEMBAYARAN ANDA :</span>
   		
   		<span style="font-size: 24px;color: red"> <?= $payment_code ?> </span>
   		<br><br>

   		
    <span style="font-size: 34px;font-weigth: bold"> Cara Melakukan Pembayaran dengan Menggunakan Finnet/ATM Transfer/Internet Banking</span>
    <br /><br />
    <span style="font-size: 24px;font-weigth: bold;color: blue">Melalui ATM:</span>
<br />
    
    1. Pilih menu “Pembayaran” di ATM
<br />
    2. Pilih menu “Pembayaran Telepon / Telkom”
<br />
    3. Masukkan 13 digit payment code yang didapat
<br />
    4. Bila diminta memasukkan kode area, masukkan 3 digit pertama ditambah dengan angka 0 didepannya, sehingga menjadi “0195” dan masukkan 9 digit berikutnya (Untuk BCA, angka 0 didepan tidak usah dipakai)
<br />
    5. Ikuti petunjuk ATM selanjutnya
<br />
<br />
    <span style="font-size: 24px;font-weigth: bold;color: blue">Melalui E-Banking:</span>

<br />
    1. Login ke Internet Banking
<br />
    2. Pilih menu pembayaran
<br />
    3. Pilih jenis pembayaran Telepon
<br />
    4. Pilih Provider Telkom
<br />
    5. Masukan 13 digit payment code yang didapat
<br />
    6. Bila diminta memasukan kode area, masukan 3 digit pertama dengan angka 0 didepannya, sehingga menjadi “0195” dan masukan 9 digit berikutnya (Untuk BCA, angka 0 didepan tidak usah dipakai)
<br />
    7. Klik lanjutkan
<br />
    8. Masukan challenge code ke token
<br />
    9. Masukan kode token internet banking
<br />
    10. Klik Submit
<br />
<br />
  <span style="font-size: 18px;font-weigth: bold;color: blue">Berikut ini adalah daftar Bank yang dapat menggunakan pembayaran melalui ATM: </span>
    
<br />
    BRI, BNI, DANAMON, PERMATA, BCA, BII, PANIN, CIMB NIAGA, UOB Indonesia, OCBC Indonesia, Citibank N.A, HSBC, BOTM UFJ Ltd, RBS, ANZ, EKONOMI, RABOBANK, BPD DKI, BPD DIY, BPD JATIM, BPD SUMUT, NAGARI, BPD SUMSEL, BPD NTB, BPD Bali, BNP, MUAMALAT, MESTIKA, MASPION, GANESHA, BTN, BHS, MEGA, BUKOPIN, BSM, ANDARA, BUMIPUTERA, AGRO, BSMI, Bank INA, Sinar Harapan, BPRKS, Artha Graha, HSBC, Bank Jabar Banten, BPD Aceh, BPD NTT, BRISyariah, VICTORIA, FinChannel, MC
<br />
    Berikut ini adalah daftar Bank yang dapat menggunakan pembayaran melalui Internet Banking: 
<br />
    BRI, MANDIRI, BNI, DANAMON, PERMATA, BCA, BII, PANIN, CIMB NIAGA, OCBC Indonesia, Citibank N.A, HSBC, MUAMALAT, BTN, MEGA, BUKOPIN, BSM, BUMIPUTERA, BPRKS, FinChannel, MC
<br /><br />
  <span style="font-size: 18px;font-weigth: bold;color: blue">Keterangan: </span>
     
<br />
    - Bank Mandiri : khusus untuk ATM dan SMS Banking Mandiri tidak bisa menggunakan metode pembayaran untuk kode 195 (transfer antar bank), namun ada channel lain pada Bank Mandiri yang bisa menerima payment code 195 yaitu Internet Banking Mandiri.
<br />
    - Untuk Bank BCA : Payment method 195 bisa dilakukan di ATM dan Klik BCA (via menu Telkom)
<br />
    - Untuk bank lain tidak ada issue. Jadi dapat menggunakan payment method 195 untuk semua ATM (via menu Telkom)


		<?
	} else { ?>
   		<span style="font-size: 14px;">ERROR, CODE : <?= $error ?> </span>


		<?
	} ?>

</div>
    