<? 
    if ( isset($tpl) && is_array($tpl) && isset($tpl['data']) && is_array($tpl['data']) ) {
        $data = $tpl['data'];
    } else {
        $data = false;
    }

 ?>

    <table style="width: 1123px" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
       <thead style="width:1123px;display:block"  >
            <tr class="cart-header">
                <td style="width: 113px">
                    ARTICLE
                </td>
                <td class="w250">
                    DESCRIPTION
                </td>

                <td class="w250">
                    DELIVERY TIME
                </td>

                <td class="w270">
                    INFO
                </td>
                
                <td class="w40">
                    VOL
                </td>
                <td class="w200">
                    AMOUNT
                </td>
            </tr>
        </thead>
        <tbody style="width: 1123px; height: 350px; display:block; overflow: auto;background-color:white">
        <?  $total = 0 ;
            $vol = 0;
        if ($data !=false ) {
            foreach ($data as $val) {

            ?>
                <tr class="h150 cart-item" style="width: 113px"  >
                    <td style="vertical-align:top;padding-top:5px;border-left-width:0.1px" >
                        <img src="<?= $val->gallery_thumb ?>" />
                        <div class="undoCart-box" rel="<?= $val->id ?>" status="added" >
                            <div class="undoCart" id="undo_<?= $val->id ?>">&nbsp;</div>
                            &nbsp;
                            <span id="undoLabel_<?= $val->id ?>" >REMOVE</span>
                        </div>
                        
                    </td>
                    <td style="vertical-align:middle" class="w250" >
                        <div width="100%">Brand: <?= $val->brand_name ?></div>
                        <div width="100%">Title: <?= $val->title ?></div>
                        <div width="100%">Article Code: <?= $val->code ?></div>
                        <div width="100%" id="item_price_<?= $val->id ?>">Price: Rp. <?= number_format($val->reg_price) ?></div>
                        

                    </td>
                    
                    <td class="w250" style="vertical-align:middle">
                        Jakarta 1-3 Hari Kerja <br>
                        Luar Jakarta, 2-6 Hari Kerja 
                    </td>
                    
                    <td class="w270" style="vertical-align:middle">
                       <?= $val->other_info ?>
                    </td>

                    <td style="text-align:center;color:#767778;vertical-align:middle" class="w40 item_vol" id="item_vol_<?= $val->id ?>"   >
                        <div class="item_up" id="up_<?= $val->id ?>"></div>
                        <div class="h30" style="vertical-align: middle;line-height:30px" id="vol_<?= $val->id ?>"><?= $val->vol ?></div>
                        <div class="item_down" id="down_<?= $val->id ?>"></div>
                    </td>
                    <td  style="color:#767778;vertical-align:middle" class="w180 " >
                        <div class= "total_item"  id="total_item_<?= $val->id ?>" >
                            Rp. <?= number_format($val->reg_price * $val->vol) ?>
                        </div>
                    </td>
                </tr>
                <? 
                $total += $val->reg_price * $val->vol;
                $vol += $val->vol;
            } 

            if (count($data) == 1 ) { ?>
                <tr class="h150 cart-item" style="width: 113px"  >
                    <td style="vertical-align:top;padding-top:5px;border-left-width:0.1px" >
                        &nbsp;
                        
                    </td>
                    <td style="vertical-align:middle" class="w250" >
                        &nbsp;
                    </td>
                    
                    <td class="w250">
                        &nbsp;
                    </td>
                    
                    <td class="w270">
                       &nbsp;
                    </td>

                    <td style="text-align:center;color:#767778" class="w40 item_vol" id="item_vol_<?= $val->id ?>"  >
                        &nbsp;
                    </td>
                    <td  style="color:#767778" class="w180 " >
                        &nbsp;
                    </td>
                </tr>
                <?
            } ?>

            <tr class="h50 cart-item"  >
                <td style="border-left-width:0.1px;">
                    &nbsp;
                </td >
                <td style="">
                    &nbsp;
                </td>
                
                <td style="">
                    &nbsp;
                </td>
                
                <td style="">
                    &nbsp;
                </td>

                <td style="">
                   &nbsp;
                </td>
                <td  style="">
                    &nbsp;
                </td>
            </tr>
            <? 
        }?>
        </tbody>
    </table>


    <table style="width: 1123px" border="0" cellspacing="0" cellpadding="0" style='table-layout:fixed'>
           
            <tr class="h20 cart-footer"  >
                <td style="width: 113px">
                    &nbsp;
                </td>
                <td class="w250">
                    &nbsp;
                </td>

                <td class="w250">
                    &nbsp;
                </td>
                <td class="w270" style="text-align:left;font-size: 16px;font-weight: bold" >
                    TOTAL
                </td>
                <td class="w40">
                    &nbsp;
                </td>
                <td class="w200" id="total_price" style="text-align:left;font-size: 16px;font-weight: bold">
                    Rp. <?= number_format($total) ?>
                </td>
                
            </tr>
            <tr class="h20 cart-footer bottom"  >
                <td  >
                    &nbsp;
                </td>
                <td  >
                    <div class="shopping-button" id="continue-shopping">
                        &nbsp;
                    <div>
                </td>
                <td  >
                    &nbsp;
                </td>
                <td style="text-align:left;" >
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td  >
                    <a href="<?= INDEX_URL . 'member/processPayment' ?>" >
                         <div class="shopping-button" id="finish-shopping">
                                &nbsp;
                        </div>
                    </a>
                </td>
                
            </tr>
            <tr  >
                <td colspan="6" style="background-color: white" >
                    <div class="h50" style="margin: 5px 5px 5px 5px;background-color: #D6D7D7;height"  >
                        &nbsp;
                    </div>
                </td>
            </tr>
            <tr class="cart-bottom "  >
                <td colspan="2">
                    <div id="show-detail" >
                        <a href="<?= INDEX_URL . 'material/detailProduct' ?>" >
                            BACK TO DETAIL PRODUCT
                        </a>
                    </div>
                </td>
                <td  >
                    <div id="show-gallery" >
                        <a href="<?= INDEX_URL . 'gallery' ?>" >
                            BACK TO GALLERY
                        </a>
                    </div>
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
                                
            </tr>
    </table>
