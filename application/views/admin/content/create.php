
CREATE NEW CONTENT
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$section_arr = content_section_arr() ;
?>

<form action="<?= INDEX_URL . 'adminContents/create' ?>" method="post" class="form" enctype="multipart/form-data">

	<input type="hidden" name="content_create" value="1" />

	<p>
		<label class="title">Section</label>
		<select name="section" class="text w100 required" >
			<option value="" <?php echo $this->form_validation->set_select('section', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($section_arr as $key => $value) { ?>
				<option value="<?= $key ?>" <?php echo $this->form_validation->set_select('section', $key); ?>  ><?= $value ?></option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('section') ?></div>
	</p>

	<p>
		<label class="title">Title</label>
		<input type="text" name="title" class="text w550 required" value="<?= set_value('title') ?>" />

		<div class="error_note"><?= form_error('title') ?></div>
	</p>
	<p>
		<label class="title">&nbsp;</label>
		<input type="checkbox" name="active" id="active" value="1" checked="checked" />
		<label for="active">Show on Front Page</label>

	</p>
	<p>
		<label class="title">Home view</label>
		<div class="wysiwyg-editor">
		    <textarea id="hove_view" name="home_view" rows="30" cols="10" style="width: 100%"> </textarea>
		</div>
	</p>
	<p>
		<label class="title">Full Content</label>
		<div class="wysiwyg-editor">
		    <textarea id="full_content" name="full_content" rows="40" cols="40" style="width: 100%"> </textarea>
		</div>
	</p>
	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

