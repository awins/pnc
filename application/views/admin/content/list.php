<?
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
	} 

	$section_active = $tpl['section_active'];
	$section_arr = content_section_arr() ;
?>
	<table border="0" width="100%" >
		<th style="text-align: left;" >
			SECTION: &nbsp;&nbsp;
			<select name="brand_id" class="text w100 required" >
				<option value="ALL" <?= ($section_active == "ALL") ? "selected='selected'" : NULL ?> >ALL</option>
				<?php 
			  	foreach ($section_arr as $key => $value) { ?>
					<option value="<?= $key ?>" <?= ($section_active == $key) ? "selected='selected'" : NULL ?> ><?= $value ?></option>
			  		<?
			  	}
				?>
			</select>	
		</th>
		<th style="text-align: right;">
			<a class="icon icon-add" title="Ubah" href="<?= INDEX_URL . 'adminContents/create'  ?>" >ADD NEW CONTENT</a>
		</th>
	</table>
	
	<table class="table" >
		<thead>
			<tr>
				<th style="width: 4%;text-align: center;">NO</th>
				<th style="width: 8%;text-align: center;">FRONT PAGE</th>
				<th style="width: 15%;text-align: center;">SECTION</th>
				<th style="width: 10%;text-align: center;">DATE</th>
				<th style="width: 55%;text-align: center;">TITLE</th>
				<th style="width: 8%;text-align: center;">ACTION</th>
			</tr>
		</thead>
		<tbody>

	<?php
	$i = 0;
	//if (count($tpl['data']) > 0) {
		foreach ($tpl['data'] as $val) 
		{
			$i++;
		 ?>
			<tr >
				<td style="text-align: center;"><?= $i ?></td>		
				<td style="text-align: center;">
					<? if($val->active == 1 ) { ?>
					<input type="radio" <?= ($val->active == 1 ) ? 'checked' : NULL ?> >
					<?php
				} ?>
				</td>		
				<td align="padding-left:5px">
					<?= $section_arr[$val->section] ?>
				</td>				
				<td align="padding-left:5px">
					<?= date('d M Y',strtotime($val->date)) ?>
				</td>				
				<td align="padding-left:5px">
					<?= $val->title ?>
				</td>				
				<td style="padding-left:5px;text-align:center;"   >
					<a class="icon icon-edit" title="Ubah" href="<?= INDEX_URL . 'adminContents/update/' . $val->id  ?>" />&nbsp;</a>
					<a class="icon icon-delete " title="Hapus" href="#" rel="<?= $val->id ?>" >&nbsp;</a>
				</td>
			</tr>
			<?php
	//	}
	}	?>
	</tbody>
</table>