
UPDATE CONTENT
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$section_arr = content_section_arr() ;

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	}
?>

<form action="<?= INDEX_URL . 'adminContents/update' ?>" method="post" class="form" enctype="multipart/form-data">
	<input type="hidden" name="content_update" value="1" />
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />

	<p>
		<label class="title">Section</label>
		<select name="section" class="text w100 required" >
			<option value="" <?php echo $this->form_validation->set_select('section', ''); ?> >CHOOSE</option>
			<?php 
          	foreach ($section_arr as $key => $value) { ?>
				<option  <?=  (($data) ? $data->section : set_value('section') == $key) ? 'selected' : NULL   ?>  value="<?= $key ?>" <?php echo $this->form_validation->set_select('section', $key); ?>  ><?= $value ?></option>
          		<?  
          	}  
			?>
		</select>		

		<div class="error_note"><?= form_error('section') ?></div>
	</p>

	<p>
		<label class="title">Title</label>
		<input type="text" name="title" class="text w550 required" value="<?= ($data) ? $data->title : set_value('title') ?>" />

		<div class="error_note"><?= form_error('title') ?></div>
	</p>
	<p>
		<label class="title">&nbsp;</label>
		<input type="checkbox" name="active" id="active" value="1" <?= (($data) && $data->active == 1) ?  'checked' : NULL ; ?> />
		<label for="active">Show on Front Page</label>

	</p>
	<p>
		<label class="title">Home view</label>
		<div class="wysiwyg-editor">
		    <textarea id="hove_view" name="home_view" rows="30" cols="10" style="width: 100%"><?= ($data) ? $data->home_view : ' ' ?> </textarea>
		</div>
	</p>
	<p>
		<label class="title">Full Content</label>
		<div class="wysiwyg-editor">
		    <textarea id="full_content" name="full_content" rows="40" cols="40" style="width: 100%"><?= ($data) ? $data->full_content : ' ' ?></textarea>
		</div>
	</p>
	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

