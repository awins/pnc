CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	} 
	
?>

<form action="<?= INDEX_URL . 'adminMembers/update' ?>" method="post" class="form" >

	<input type="hidden" name="member_update" value="1" />
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />

	
	<p>
		<label class="title">Member Name</label>
		<input type="text" name="member_name" class="text w200 " value="<?= ($data) ? $data->member_name : set_value('member_name') ?>" />

		<div class="error_note"><?= form_error('member_name') ?></div>
	</p>

	<p>
		<label class="title">Address</label>
		<input type="text" name="address" class="text w500 " value="<?= ($data) ? $data->address : set_value('address') ?>" />

		<div class="error_note"><?= form_error('address') ?></div>
	</p>

	<p>
		<label class="title">City</label>
		<input type="text" name="city" class="text w200 " value="<?= ($data) ? $data->city : set_value('city') ?>" />

		<div class="error_note"><?= form_error('city') ?></div>
	</p>

	<p>
		<label class="title">Telp</label>
		<input type="text" name="telp" class="text w200 " value="<?= ($data) ? $data->telp : set_value('telp') ?>" />

		<div class="error_note"><?= form_error('telp') ?></div>
	</p>

	<p>
		<label class="title">Email</label>
		<input type="text" name="email" class="text w200 " value="<?= ($data) ? $data->email : set_value('email') ?>" />

		<div class="error_note"><?= form_error('email') ?></div>
	</p>
	
	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

