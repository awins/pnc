<? 
    if ( isset($tpl) && is_array($tpl) && isset($tpl['data']) && is_array($tpl['data']) ) {
        $data = $tpl['data'];
        $statuses = $tpl['statuses'];
    } else {
        $data = false;
    }

 ?>

    <table style="width: 1024px" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
       <thead style="width:1024px;display:block"  >
            <tr class="cart-header">
                <td style="width: 105px">
                    ARTICLE
                </td>
                <td style="width: 155px">
                    DESCRIPTION
                </td>

                <td style="width: 47px">
                    VOL
                </td>

                <td style="width: 95px">
                    AMOUNT
                </td>

                <td style="width: 175px">
                    MEMBER
                </td>

                <td style="width: 75px">
                    STATUS
                </td>
                
                <td style="width: 175px">
                    TIME LINE
                </td>
                <td style="width: 190px">
                    NOTES
                </td>
            </tr>
        </thead>
        <tbody style="width: 1024px; height: 350px; display:block; overflow: auto;background-color:white">
        <?  $total = 0 ;
            $vol = 0;
        if ($data !=false ) {
            foreach ($data as $val) {

            ?>
                <tr class="h150 cart-item" style="width: 113px"  >
                    <td style="vertical-align:top;padding-top:5px;border-left-width:0.1px" >
                        <img src="<?= $val->gallery_thumb ?>" />
                    </td>
                    <td style="vertical-align:middle" class="w150" >
                        <div width="100%">Brand: <?= $val->brand_name ?></div>
                        <div width="100%">Title: <?= $val->title ?></div>
                        <div width="100%">Article Code: <?= $val->code ?></div>
                        <div width="100%" id="item_price_<?= $val->id ?>">Price: Rp. <?= number_format($val->reg_price) ?></div>
                    </td>
                    
                    <td style="text-align:center;color:#767778;vertical-align:middle" class="w40 item_vol" id="item_vol_<?= $val->id ?>"   >
                        <div class="h30" style="vertical-align: middle;line-height:30px" id="vol_<?= $val->id ?>"><?= $val->sale->quantity ?></div>
                    </td>

                    <td  style="color:#767778;vertical-align:middle" class="w90 " >
                        <div class= "total_item"  id="total_item_<?= $val->id ?>" >
                            Rp. <?= number_format($val->reg_price * $val->sale->quantity) ?>
                        </div>
                    </td>

                    <td style="vertical-align:middle" class="w170">
                        <?= $val->sale->member_name ?>
                    </td>

                    <td style="vertical-align:middle" class="w70">
                        <?= array_search($val->sale->status,$statuses)  ?>
                    </td>

                    <td style="vertical-align:middle" class="w230">
                        <div width="100%">Cart Created: <?= date('d/m/Y - H:i', strtotime($val->sale->created_dt)) ?></div>
                        <div width="100%">Cart Completed: <?= (is_null($val->sale->sale_complete_dt)) ? '' : date('d/m/Y - H:i', strtotime($val->sale->sale_complete_dt)) ?></div>
                        <div width="100%">Payment: <?= (is_null($val->sale->payment_dt)) ? '' : date('d/m/Y - H:i', strtotime($val->sale->payment_dt)) ?></div>
                        <div width="100%">Goods Received: <?= (is_null($val->sale->goods_received_dt)) ? '' : date('d/m/Y - H:i', strtotime($val->sale->goods_received_dt)) ?></div>
                    </td>

                    <td style="vertical-align:middle" class="w170">
                        <?= $val->sale->notes ?>
                    </td>

                </tr>
                <? 
                $total += $val->reg_price * $val->sale->quantity;
                $vol += $val->sale->quantity;
            } ?>

            

            <tr class="h50 cart-item"  >
                <td style="border-left-width:0.1px;">
                    &nbsp;
                </td >
                <td style="">
                    &nbsp;
                </td>
                
                <td style="">
                    &nbsp;
                </td>
                
                <td style="">
                    &nbsp;
                </td>

                <td style="">
                   &nbsp;
                </td>
                <td  style="">
                    &nbsp;
                </td>
                <td  style="">
                    &nbsp;
                </td>
                <td  style="">
                    &nbsp;
                </td>
            </tr>
            <? 
        }?>
        </tbody>
    </table>


    <table style="width: 1024px" border="0" cellspacing="0" cellpadding="0" style='table-layout:fixed'>
           
            <tr class="h20 cart-footer"  >
                <td style="width: 113px">
                    &nbsp;
                </td>
                <td class="w150">
                    TOTAL
                </td>

                <td class="w40">
                    &nbsp;
                </td>
                <td class="w90" style="text-align:left;font-size: 16px;font-weight: bold" >
                    Rp. <?= number_format($total) ?>
                </td>
                <td class="w170">
                    &nbsp;
                </td>
                <td class="w70">
                    &nbsp;
                </td>

                <td class="w230">
                    &nbsp;
                </td>

                <td class="w170">
                    &nbsp;
                </td>

                
            </tr>
           
    </table>
