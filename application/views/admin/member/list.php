<?
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
	} 

?>
<table border="0" width="100%" >
	<th style="text-align: left;" class="pagination" >
		<?= $tpl['pagination'] ?>
	</th>
	<th style="text-align: right;">
		&nbsp;
	</th>
</table>
<table class="table" >
	<thead>
		<tr>
			<th style="width: 8%;text-align: center;">NO</th>
			<th >MEMBER NAME</th>
			<th >ADDRESS</th>
			<th >CITY</th>
			<th >TELP</th>
			<th >EMAIL</th>

			<th style="width: 8%;text-align: center;">ACTION</th>
		</tr>
	</thead>
	<tbody>

	<?php
	$i = ($tpl['page'] - 1) * 20 ;
	foreach ($tpl['data'] as $val) 
	{ 
		$i++;
		?>
		<tr>
			<td style="text-align: center;"><?= $i ?></td>		
			<td style="padding-left:5px"><?= $val->member_name ?></td>		
			<td style="padding-left:5px"><?= $val->address ?></td>		
			<td style="padding-left:5px"><?= $val->city ?></td>		
			<td style="padding-left:5px"><?= $val->telp ?></td>		
			<td style="padding-left:5px"><?= $val->email ?></td>		
			<td style="padding-left:5px;text-align:center;"   >
				<a class="icon icon-edit" title="Ubah" href="<?= INDEX_URL . 'adminMembers/update/' . $val->id  ?>" />&nbsp;</a>
				<a class="icon icon-delete " title="Hapus" href="#" rel="<?= $val->id ?>" >&nbsp;</a>
			</td>
		</tr>
		<?php
	}	?>

	</tbody>
</table>