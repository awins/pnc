UPDATE BRAND
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	} 
?>

<form action="<?= INDEX_URL . 'adminBrands/update' ?>" method="post" class="form" enctype="multipart/form-data">

	<input type="hidden" name="brand_update" value="1" />
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />
	<p>
		<label class="title">Brand</label>
		<input type="text" name="brand_name" class="text w50 required" value="<?= ($data) ? $data->brand_name : set_value('brand_name') ?>" />

		<div class="error_note"><?= form_error('brand_name') ?></div>
	</p>
	
	<p>
		<label class="title">Image</label>
		<input type="file" name="userfile" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err']) ? $tpl['upload_err'] : NULL ?></div>
	</p>


	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

