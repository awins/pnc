<?
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
	} 
?>
<table border="0" width="100%" >
	<th style="text-align: left;" class="pagination" >
		<?= $tpl['pagination'] ?>
	</th>
	<th style="text-align: right;">
		<a class="icon icon-add" title="Ubah" href="<?= INDEX_URL . 'adminBrands/create'  ?>" >ADD NEW BRAND</a>
	</th>
</table>
<table class="table" >
	<thead>
		<tr>
			<th style="width: 8%;text-align: center;">NO</th>
			<th >BRAND</th>
			<th style="width: 20%;text-align: center;">IMAGE</th>
			<th style="width: 8%;text-align: center;">ACTION</th>
		</tr>
	</thead>
	<tbody>

	<?php
	$i = ($tpl['page'] - 1) * 20 ;
	foreach ($tpl['data'] as $val) 
	{
		$i++;
	 ?>
		<tr>
			<td style="text-align: center;"><?= $i ?></td>		
			<td style="padding-left:5px"><?= $val->brand_name ?></td>		
			<td align="center">
				<img id="thumb_<?= $val->id ?>" src="<?= IMG_PATH . 'brand/' . $val->brand_image ?>" class="thumb"  />
			</td>				
			
			<td style="padding-left:5px;text-align:center;"   >
				<a class="icon icon-edit" title="Ubah" href="<?= INDEX_URL . 'adminBrands/update/' . $val->id  ?>" />&nbsp;</a>
				<a class="icon icon-delete " title="Hapus" href="#" rel="<?= $val->id ?>" >&nbsp;</a>
			</td>
		</tr>
		<?php
	}	?>

	</tbody>
</table>