CREATE NEW BRAND
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 


?>

<form action="<?= INDEX_URL . 'adminBrands/create' ?>" method="post" class="form" enctype="multipart/form-data">

	<input type="hidden" name="brand_create" value="1" />
	<p>
		<label class="title">Brand Name</label>
		<input type="text" name="brand_name" class="text w50 required" value="<?= set_value('brand_name') ?>" />

		<div class="error_note"><?= form_error('brand_name') ?></div>
	</p>
	
	<p>
		<label class="title">Image</label>
		<input type="file" name="userfile" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err']) ? $tpl['upload_err'] : NULL ?></div>
	</p>


	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

