<?
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
	} 

?>
<table border="0" width="100%" >
	<th style="text-align: left;" class="pagination" >
		<?= $tpl['pagination'] ?>
	</th>
	<th style="text-align: right;">
		<a class="icon icon-add" title="Tambah" href="<?= INDEX_URL . 'adminItems/create'  ?>" >ADD NEW ITEM</a>
	</th>
</table>
<table class="table" >
	<thead>
		<tr>
			<th style="width: 8%;text-align: center;">NO</th>
			<th >CATEGORY</th>
			<th >ITEM NAME</th>

			<th style="width: 8%;text-align: center;">ACTION</th>
		</tr>
	</thead>
	<tbody>

	<?php
	$i = ($tpl['page'] - 1) * 20 ;
	foreach ($tpl['data'] as $val) 
	{ 
		$i++;
		?>
		<tr>
			<td style="text-align: center;"><?= $i ?></td>		
			<td style="padding-left:5px"><?= $val->category_name ?></td>		
			<td style="padding-left:5px"><?= $val->item_name ?></td>		
			<td style="padding-left:5px;text-align:center;"   >
				<a class="icon icon-edit" title="Ubah" href="<?= INDEX_URL . 'adminItems/update/' . $val->id  ?>" />&nbsp;</a>
				<a class="icon icon-delete " title="Hapus" href="#" rel="<?= $val->id ?>" >&nbsp;</a>
			</td>
		</tr>
		<?php
	}	?>

	</tbody>
</table>