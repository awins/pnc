CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$categories = $tpl['categories'];
?>

<form action="<?= INDEX_URL . 'adminItems/create' ?>" method="post" class="form" >

	<input type="hidden" name="item_create" value="1" />

	
	<p>
		<label class="title">Category</label>
		<select name="category_id" class="text w400" >
			<option value="" <?php echo $this->form_validation->set_select('category_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($categories as $val) { ?>
				<option value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('category_id', $val->id); ?>  ><?= $val->category_name  ?></option>
          		<?
          	}
			?>
		</select>		
		<div class="error_note"><?= form_error('category_id') ?></div>

	</p>

	<p>
		<label class="title">Item Name</label>
		<input type="text" name="item_name" class="text w100 " value="<?= set_value('item_name') ?>" />

		<div class="error_note"><?= form_error('item_name') ?></div>
	</p>	

	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

