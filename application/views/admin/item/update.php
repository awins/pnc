CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$categories = $tpl['categories'];

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	} 
	
?>

<form action="<?= INDEX_URL . 'adminItems/update' ?>" method="post" class="form" >

	<input type="hidden" name="item_update" value="1" />
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />

	
	<p>
		<label class="title">Category</label>
		<select name="category_id" class="text w400" >
			<option value="" <?php echo $this->form_validation->set_select('category_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($categories as $val) { ?>
				<option <?=  ((($data) ? $data->category_id : set_value('category_id')) == $val->id) ? 'selected' : NULL  ?>  value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('category_id', $val->id); ?>  ><?= $val->category_name  ?></option>
          		<?
          	}
			?>
		</select>		
		<div class="error_note"><?= form_error('category_id') ?></div>

	</p>

	<p>
		<label class="title">Item Name</label>
		<input type="text" name="item_name" class="text w100 " value="<?= ($data) ? $data->item_name : set_value('item_name') ?>" />

		<div class="error_note"><?= form_error('item_name') ?></div>
	</p>

	
	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

