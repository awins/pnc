CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$brands = $tpl['brands'];
	$categories = $tpl['categories'];
?>

<form action="<?= INDEX_URL . 'adminMaterials/create' ?>" method="post" class="form" enctype="multipart/form-data">

	<input type="hidden" name="Material_create" value="1" />

	<p>
		<label class="title">Brand</label>
		<select name="brand_id" class="text w100 required" >
			<option value="" <?php echo $this->form_validation->set_select('brand_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($brands as $val) { ?>
				<option value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('brand_id', $val->id); ?>  ><?= $val->brand_name ?></option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('brand_id') ?></div>
	</p>

	<p>
		<label class="title">Category</label>
		<select name="category_id" class="text w100 required" >
			<option value="" <?php echo $this->form_validation->set_select('category_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($categories as $val) { ?>
				<option value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('category_id', $val->id); ?>><?= $val->category_name ?></option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('category_id') ?></div>
	</p>

	
	<p>
		<label class="title">Thumb Image</label>
		<input type="file" name="thumb" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err_thumb']) ? $tpl['upload_err_thumb'] : NULL ?></div>
	</p>

	<p>
		<label class="title">Full Image</label>
		<input type="file" name="full" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err_full']) ? $tpl['upload_err_full'] : NULL ?></div>
	</p>

	<p>
		<label class="title">Mix  Match Image</label>
		<input type="file" name="mix" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err_mix']) ? $tpl['upload_err_mix'] : NULL ?></div>
	</p>

	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

