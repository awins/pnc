<?
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
	} 

?>
<table border="0" width="100%" >
	<th style="text-align: left;" class="pagination" >
		<?= $tpl['pagination'] ?>
	</th>
	<th style="text-align: right;">
		<a class="icon icon-add" title="Tambah" href="<?= INDEX_URL . 'adminMaterials/create'  ?>" >ADD NEW MATERIAL</a>
	</th>
</table>
<div class="outer">
 	<div class="inner">
		<table class="table scroll" >
			<thead>
				<tr>
					<td style="width: 50px;text-align: center;">NO</td>
					<td style="width: 50px;" >NEW</td>
					<td style="width: 50px;" >LATEST</td>
					<td style="width: 150px;">BRAND</td>
					<td style="width: 100px;" >CATEGORY</td>
					<td style="width: 100px;" >ITEM</td>
					<td style="width: 150px;" >TITLE</td>
					<td style="width: 120px;" >ARTICLE CODE</td>
					<td style="width: 150px;" >COLOR</td>
					<td style="width: 50px;" >SIZE</td>
					<td style="width: 50px;" >STOCK</td>
					<td style="width: 150px;" >TEMATIC</td>

					<th style="padding-left:5px;text-align: center;">ACTION</th>
				</tr>
			</thead>
			<tbody>

			<?php
			$i = ($tpl['page'] - 1) * 20 ;
			foreach ($tpl['data'] as $val) 
			{ 
				$i++;
				?>
				<tr>
					<td style="text-align: center;"><?= $i ?></td>		
					<td style="padding-left:5px">
						<input type="checkbox" class="radio_update" rel="is_new" id="radio_new_<?=$val->id ?>" <?= ($val->is_new == 1) ?  'checked="checked"' : NULL ; ?>  >&nbsp;	
					</td>		
					<td style="padding-left:5px">
						<input type="checkbox" class="radio_update" rel="is_latest" id="radio_latest_<?=$val->id ?>" <?= ($val->is_latest == 1) ?  'checked="checked"' : NULL ; ?> >&nbsp;	
					</td>		
					<td style="padding-left:5px"><?= $val->brand_name ?></td>		
					<td style="padding-left:5px"><?= $val->category_name ?></td>		
					<td style="padding-left:5px"><?= $val->item_name ?></td>		
					<td style="padding-left:5px"><?= $val->title ?></td>		
					<td style="padding-left:5px"><?= $val->code ?></td>		
					<td style="padding-left:5px"><?= $val->color ?></td>		
					<td style="padding-left:5px"><?= $val->size ?></td>		
					<td style="padding-left:5px"><?= $val->stock ?></td>		
					<td style="padding-left:5px"><?= $val->tematic ?></td>		

					<th  style="padding-left:5px;text-align:center;"   >
							<a class="icon icon-edit" title="Ubah" href="<?= INDEX_URL . 'adminMaterials/update/' . $val->id  ?>" />&nbsp;</a>
							<a class="icon icon-delete " title="Hapus" href="#" rel="<?= $val->id ?>" >&nbsp;</a>
					</th>
				</tr>
				<?php
			}	?>

			</tbody>
		</table>

	</div>
</div>