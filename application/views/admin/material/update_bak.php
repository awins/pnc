UPDATE CLOTHES
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	}

	$brands = $tpl['brands'];
	$categories = $tpl['categories'];
?>

<form action="<?= INDEX_URL . 'adminClothes/update' ?>" method="post" class="form" enctype="multipart/form-data">
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />
	<input type="hidden" name="clothes_update" value="1" />
	<p>
		<label class="title">Clothes Name</label>
		<input type="text" name="title" class="text w50 required" value="<?= ($data) ? $data->title : set_value('title') ?>" />
		<div class="error_note"><?= form_error('title') ?></div>
	</p>

	<p>
		<label class="title">Brand</label>
		<select name="brand_id" class="text w100 required" >
			<option value="" <?php echo $this->form_validation->set_select('brand_id', ''); ?> >CHOOSE</option>
			<?php 
          	foreach ($brands as $val) { ?>
				<option <?=  (($data) ? $data->brand_id : set_value('brand_id') == $val->id) ? 'selected' : NULL   ?>  value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('brand_id', $val->id); ?>>
					<?= $val->brand_name ?>

				</option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('brand_id') ?></div>
	</p>

	<p>
		<label class="title">Category</label>
		<select name="category_id" class="text w100 required" >
			<option value="" <?php echo $this->form_validation->set_select('category_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($categories as $val) { ?>
				<option <?=  (($data) ? $data->category_id : set_value('category_id') == $val->id) ? 'selected' : NULL   ?> value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('category_id', $val->id); ?>>
					<?= $val->category_name ?>
				</option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('category_id') ?></div>
	</p>

	
	<p>
		<label class="title">Thumb Image</label>
		<input type="file" name="thumb" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err_thumb']) ? $tpl['upload_err_thumb'] : NULL ?></div>
	</p>

	<p>
		<label class="title">Full Image</label>
		<input type="file" name="full" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err_full']) ? $tpl['upload_err_full'] : NULL ?></div>
	</p>

	<p>
		<label class="title">Mix  Match Image</label>
		<input type="file" name="mix" class="crText w300 required" />
		<div class="error_note"><?= ($tpl['upload_err_mix']) ? $tpl['upload_err_mix'] : NULL ?></div>
	</p>

	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

