<?
$materials = $tpl['materials'];
$val = $materials[0];
?>
<div style="width:560px">
<span class="material_title">MATERIAL DETAIL</span>
	
</div>

<div style="width:280px;float:left;">
	<p>
		<label style="width:80px">Category</label>
		<label>: <?= $val->category_name ?>  </label>
	</p>
	<p>
		<label style="width:80px">Item</label>
		<label>: <?= $val->item_name ?>  </label>
	</p>
	
</div>

<div style="width:280px;float:left;">
	<p>
		<label style="width:80px">Article</label>
		<label>: <?= $val->title ?>  </label>
	</p>
	<p>
		<label style="width:80px">Brand</label>
		<label>: <?= $val->brand_name ?>  </label>
	</p>
	
</div>
<br style="clear:both;">
<p>
	<hr>
	<span class="material_title">GALLERY</span>
	<br>
</p>
<div style="float:left">
	<input type="checkbox" id="display_gallery" <?= ($val->display_gallery == 1) ?  'checked="checked"' : NULL ; ?>  />
	<label for="display_gallery">Display material onto Gallery Page</label>
</div>
<div style="float:right">
	<input type="checkbox" id="display_pnc" <?= ($val->display_pnc == 1) ?  'checked="checked"' : NULL ; ?>  />
	<label for="display_pnc">Display material onto PNC Only Page</label>
</div>

<p >
	<div class="image-thumbnail" >
		<? $src = ($val->gallery_thumb) ? $val->gallery_thumb : IMG_PATH . 'materials/no-photo.png' ; ?>
		<input class="btn-upload" type="button" value="Upload" rel="T_G" />
		<span >Thumbnail</span>
		<img src="<?= $src ?>" id="T_G"  />
	</div>
	<div style="float:left" class="image-thumbnail" >
		<? $src = ($val->gallery_front) ? $val->gallery_front : IMG_PATH . 'materials/no-photo.png' ; ?>
		<input class="btn-upload" type="button" value="Upload" rel="F_G" />
		<span >Display Front</span>
		<img src="<?= $src ?>" id="F_G" />

	</div>
	<div style="float:left" class="image-thumbnail" >
		<? $src = ($val->gallery_rear) ? $val->gallery_rear : IMG_PATH . 'materials/no-photo.png' ; ?>
		<input class="btn-upload" type="button" value="Upload" rel="R_G" />
		<span >Display Rear</span>
		<img src="<?= $src ?>" id="R_G" />
	</div>
</p>

<br style="clear:both;">
<p>
	<hr>
	<span class="material_title">MIX & MATCH</span>
	<br>
</p>
<input type="checkbox" id="display_mixmatch" <?= ($val->display_mixmatch == 1) ?  'checked="checked"' : NULL ; ?>  />
<label for="display_mixmatch">Display material onto Mix & Match Page</label>
<br>
<p >
	<div style="float:left" class="image-thumbnail" >
		<? $src = ($val->mixmatch_thumb) ? $val->mixmatch_thumb : IMG_PATH . 'materials/no-photo.png' ; ?>
		<input class="btn-upload" type="button" value="Upload"  rel="T_M" />
		<span >Thumbnail</span>
		<img src="<?= $src ?>" id="T_M"  />
	</div>
	<div style="float:left" class="image-thumbnail" >
		<? $src = ($val->mixmatch_front) ? $val->mixmatch_front : IMG_PATH . 'materials/no-photo.png' ; ?>
		<input class="btn-upload" type="button" value="Upload" rel="F_M" />
		<span >Display Front</span>
		<img src="<?= $src ?>" id="F_M" />
	</div>
	<div style="float:left" class="image-thumbnail" >
		<? $src = ($val->mixmatch_rear) ? $val->mixmatch_rear : IMG_PATH . 'materials/no-photo.png' ; ?>
		<input class="btn-upload" type="button" value="Upload"  rel="R_M" />
		<span >Display Rear</span>
		<img src="<?= $src ?>" id="R_M" />
	</div>
</p>
<br style="clear:both" >
<? if ($val->category_id == 5 ) { ?>
	<p>
		<hr>
		<span class="material_title">SUGGESTION</span>
		<br>
	</p>

	<input type="checkbox" id="display_suggestion" <?= ($val->display_suggestion == 1) ?  'checked="checked"' : NULL ; ?>  />
	<label for="display_suggestion">Display suggestion style(s) of Material onto Mix & Match Page</label>
	<br>
	<p >
		<?
		$arr_suggestion = $val->suggestions;

		?>


		<div style="" id="div_suggestion"  >
			<input class="btn-upload" type="button" value="Upload"  rel="S_" />
			<span style="color: blue;font-weight: bold;" >Suggestion Styles</span>
			<div style="width:100%" >
				<? if (count($arr_suggestion) > 0 ) { 
					foreach ($arr_suggestion as $img) 
					{  
						$src =  $img ; ?>
						<div style=" float:left;" class="image-thumbnail"  >
							<img src="<?= $src ?>"  />
						</div>	
						<div style=" float: left;" id ="divnoImage"  >
							<img src="" id="noImage" />
						</div>
					<?
					}

				} else { 
					$src = IMG_PATH . 'materials/no-photo.png';
					?>
					<div style=" float: left;" id ="divnoImage"  >
						<img src="<?= $src ?>" id="noImage" />
					</div>
					<?
				} ?>
			</div>
		</div>
		
	</p>
<? } ?>

<br style="clear:both" >

<div style="display:none" >
	<form  action="" method="post" class="crForm" enctype="multipart/form-data" id="form_test" >
		<input type="text" value="<?php echo $val->id; ?>" id="doc_id" name="id" />
		<input type="text" value="text" id="doc_rel" name="rel" />
		<input type="file" name="doc_file" id="btnfile"  /> 
	</form>
	<div id="crConts">&nbsp;</div>
</div>