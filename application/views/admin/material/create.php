CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$brands = $tpl['brands'];
	$articles = $tpl['articles'];
?>

<form action="<?= INDEX_URL . 'adminMaterials/create' ?>" method="post" class="form" >

	<input type="hidden" name="material_create" id="material_create" value="1" />

	
	<p>
		<label class="title">Brand</label>
		<select name="brand_id" class="text w200 " >
			<option value="" <?php echo $this->form_validation->set_select('brand_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($brands as $val) { ?>
				<option value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('brand_id', $val->id); ?>  ><?= $val->brand_name ?></option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('brand_id') ?></div>
	</p>

	<p>
		<label class="title">Article</label>
		<select name="article_id" class="text w400" >
			<option value="" <?php echo $this->form_validation->set_select('article_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($articles as $val) { ?>
				<option value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('article_id', $val->id); ?>  ><?= $val->code . ' - ' . $val->title . ' - ' . $val->category_name . ' - ' . $val->item_name ?></option>
          		<?
          	}
			?>
		</select>		
		<div class="error_note"><?= form_error('article_id') ?></div>

	</p>

	<p>
		<label class="title">Barcode</label>
		<input type="text" name="barcode" class="text w200 " value="<?= set_value('barcode') ?>" />

		<div class="error_note"><?= form_error('barcode') ?></div>
	</p>

	<p>
		<label class="title">Tematic</label>
		<input type="text" name="tematic" class="text w400 " value="<?= set_value('tematic') ?>" />

		<div class="error_note"><?= form_error('tematic') ?></div>
	</p>

	<p>
		<label class="title">Color</label>
		<input type="text" name="color" class="text w100 " value="<?= set_value('color') ?>" />

		<div class="error_note"><?= form_error('color') ?></div>
	</p>

	<p>
		<label class="title">Size</label>
		<input type="text" name="size" class="text w100 " value="<?= set_value('size') ?>" />

		<div class="error_note"><?= form_error('size') ?></div>
	</p>
	
	<p>
		<label class="title">Stock</label>
		<input type="text" name="stock" class="text w50 " value="<?= set_value('stock') ?>" />

	</p>

	<p>
		<label class="title">Regular Price</label>
		<input type="text" name="reg_price" class="text w100 " value="<?= set_value('reg_price') ?>" />

	</p>

	<p>
		<label class="title">Discount Price</label>
		<input type="text" name="disc_price" class="text w100 " value="<?= set_value('disc_price') ?>" />

	</p>

	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

