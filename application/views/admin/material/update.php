CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$brands = $tpl['brands'];
	$articles = $tpl['articles'];

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	} 
	
?>

<form action="<?= INDEX_URL . 'adminMaterials/update' ?>" method="post" class="form" >

	<input type="hidden" name="material_update" id="material_update" value="1" />
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />

	
	<p>
		<label class="title">Brand</label>
		<select name="brand_id" class="text w200 " >
			<option value="" <?php echo $this->form_validation->set_select('brand_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($brands as $val) { ?>
				<option <?=  ((($data) ? $data->brand_id : set_value('brand_id')) == $val->id) ? 'selected' : NULL  ?>  value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('brand_id', $val->id); ?>  ><?= $val->brand_name ?></option>
          		<?
          	}
			?>
		</select>		

		<div class="error_note"><?= form_error('brand_id') ?></div>
	</p>

	<p>
		<label class="title">Article</label>
		<select name="article_id" class="text w400" >
			<option value="" <?php echo $this->form_validation->set_select('article_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($articles as $val) { ?>
				<option <?=  ((($data) ? $data->article_id : set_value('article_id')) == $val->id) ? 'selected' : NULL  ?>  value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('article_id', $val->id); ?>  ><?= $val->code . ' - ' . $val->title . ' - ' . $val->category_name . ' - ' . $val->item_name ?></option>
          		<?
          	}
			?>
		</select>		
		<div class="error_note"><?= form_error('article_id') ?></div>

	</p>

	<p>
		<label class="title">Barcode</label>
		<input type="text" name="barcode" class="text w200 " value="<?= ($data) ? $data->barcode : set_value('barcodd') ?>" />

		<div class="error_note"><?= form_error('barcode') ?></div>
	</p>

	<p>
		<label class="title">Tematic</label>
		<input type="text" name="tematic" class="text w400 " value="<?= ($data) ? $data->tematic : set_value('tematic') ?>" />

		<div class="error_note"><?= form_error('tematic') ?></div>
	</p>

	<p>
		<label class="title">Color</label>
		<input type="text" name="color" class="text w100 " value="<?= ($data) ? $data->color : set_value('color') ?>" />

		<div class="error_note"><?= form_error('color') ?></div>
	</p>

	<p>
		<label class="title">Size</label>
		<input type="text" name="size" class="text w100 " value="<?= ($data) ? $data->size : set_value('size') ?>" />

		<div class="error_note"><?= form_error('size') ?></div>
	</p>
	
	<p>
		<label class="title">Stock</label>
		<input type="text" name="stock" class="text w50 " value="<?= ($data) ? $data->stock : set_value('stock') ?>" />

	</p>

	<p>
		<label class="title">Regular Price</label>
		<input type="text" name="reg_price" class="text w100 " value="<?= ($data) ? $data->reg_price : set_value('reg_price') ?>" />

	</p>

	<p>
		<label class="title">Discount Price</label>
		<input type="text" name="disc_price" class="text w100 " value="<?= ($data) ? $data->disc_price : set_value('disc_price') ?>" />

	</p>
	
	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

