<?
$materials = $tpl['materials'];
$categories = $tpl['categories'];
$img_path = IMG_PATH  . 'materials/' ;
$img_dir = IMG_DIR  . 'materials/' ;

$category_id = $tpl['category_id'] ;
$filter_display = $tpl['filter_display'] ;

?>

<input type="hidden" id ="page_type" value="mixmatch" />

<div width="100%"  >
	<div style="float:left;margin-left:20px">
		SELECT CATEGORY: 
		<select id="select_category" class="select_page text w200 " >
			<option value="0"  <?= ($category_id == 0 ) ? 'selected' : NULL ; ?> >ALL</option>
			<?php 
	      	foreach ($categories as $val) { ?>
				<option value="<?= $val->id ?>" <?= ($category_id == $val->id ) ? 'selected' : NULL;  ?> ><?= $val->category_name ?></option>
	      		<?
	      	}
			?>
		</select>
	</div>

	<div style="float:right;margin-right:20px">
		SELECT DISPLAYED STATUS:
		<select id="select_display" class="select_page text w150" >
			<option value="2" <?= ($filter_display == 2 ) ? 'selected' : NULL ; ?> > ALL </option>
			<option value="1" <?= ($filter_display == 1 ) ? 'selected' : NULL ; ?> > Displayed </option>
			<option value="0" <?= ($filter_display == 0 ) ? 'selected' : NULL ; ?>> Not Displayed </option>
		</select>
	</div>
</div>
<br style="clear:both;">
<hr>
<div>
	<? 

	foreach ($materials as $val) { 
		$thumbnail = 			'T_M_' . $val->id . '.png';
		$front = 		'F_M_' . $val->id . '.png';
		$rear = 		'R_M_' . $val->id . '.png';

		$src_thumnail = ((file_exists($img_dir . $thumbnail))) ? $img_path . $thumbnail : $img_path . 'no-photo.png' ;
		$src_front = ((file_exists($img_dir . $front))) ? $img_path . $front : $img_path . 'no-photo.png' ;
		$src_rear = ((file_exists($img_dir . $rear))) ? $img_path . $rear : $img_path . 'no-photo.png' ;
		?>
		<div style="margin:14px;" class="image-thumbnail" >

				<span ><?=  $val->title ?></span>
			<!-- <div style="width:170px;height:185;overflow:hidden;position:relative"> -->
				<!-- <div style="width:600px;height:185;overflow:hidden" > -->

					<div style="position:absolute;width:170px;left:0px; top:30px" id="img_<?= $val->id ?>_1">
						<img  src="<?= $src_thumnail ?>" />
					</div>
					
					
				<!-- </div> -->
			<!-- </div> -->
			<div style="position:absolute;bottom:0;padding:5px" >
				<div width="200px" style="float:left" align='right'>
					<label class="btnUpload" material_id='<?= $val->id ?>' category_id="<?= $val->category_id ?>" >upload</label>
				</div>

				<div width="200px" style="float:right" align='right'>
					<label class="btnShow" id='show_<?= $val->id ?>'  thumbnail="<?= $src_thumnail ?>" front="<?= $src_front ?>" rear="<?= $src_rear ?>" title="Mix & Match Images of '<?= $val->title ?>'" >show images</label>
				</div>
				<br style="clear:both" />
				<input type="checkbox" id="display_mixmatch_<?= $val->id ?>" class="display_mixmatch" <?= ($val->display_mixmatch == 1) ?  'checked="checked"' : NULL ; ?>  />
				<label for="display_mixmatch_<?= $val->id ?>">Display on Mix Match</label>
			</div>
		</div>
		<?
	} ?>


	
</div>
<br style="clear:both;">



<div id="images-popup"  style="display:none" >
	<div class="title" >
		<div style= "float:right">
			<img  src="<?= IMG_PATH . 'page/button/icon-close.gif?5454' ?>" id="close-popup"  />
		</div>
		<div style= "float:left;">
			<label id="title_label" >IMAGES</label>
		</div>
	</div>
	<br style="clear:both;">

	<? $src = $img_path . 'no-photo.png'; ?>
	<div style="width:170px;float:left;" >
		<span >Thumbnail</span>
		<img  src="<?= $src ?>" id="show_thumbnail"  />
	</div>
	<div style="width:170px;float:left" >
		<span >Display Front</span>
		<img  src="<?= $src ?>" id="show_front"  />
	</div>
	<div style="width:170px;float:left" >
		<span >Display Rear</span>
		<img  src="<?= $src ?>" id="show_rear"  />
	</div>
	
</div>
