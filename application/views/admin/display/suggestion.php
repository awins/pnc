<?
$materials = $tpl['materials'];
$img_path = IMG_PATH  . 'materials/' ;
$img_dir = IMG_DIR  . 'materials/' ;

$filter_display = $tpl['filter_display'] ;

?>

<input type="hidden" id ="page_type" value="suggestion" />

<div width="100%"  >
	

	<div style="float:left;margin-left:20px">
		SELECT DISPLAYED STATUS:
		<select id="select_display" class="select_page text w150" >
			<option value="2" <?= ($filter_display == 2 ) ? 'selected' : NULL ; ?> > ALL </option>
			<option value="1" <?= ($filter_display == 1 ) ? 'selected' : NULL ; ?> > Displayed </option>
			<option value="0" <?= ($filter_display == 0 ) ? 'selected' : NULL ; ?>> Not Displayed </option>
		</select>
	</div>
</div>
<br style="clear:both;">
<hr>
<div>
	<? 

	foreach ($materials as $val) { 
		$thumbnail = 	'T_M_' . $val->id . '.png';
		$suggestion = 		$val->suggestion_images ;

		$src_thumnail = ((file_exists($img_dir . $thumbnail))) ? $img_path . $thumbnail : $img_path . 'no-photo.png' ;
		?>
		<div style="margin:14px;" class="image-thumbnail" material="<?= $val->id ?>" >

				<span ><?=  $val->title ?></span>
			<!-- <div style="width:170px;height:185;overflow:hidden;position:relative"> -->
				<!-- <div style="width:600px;height:185;overflow:hidden" > -->

					<div style="position:absolute;width:170px;left:0px; top:30px" id="img_<?= $val->id ?>_1">
						<img  src="<?= $src_thumnail ?>" />
					</div>
					
					
				<!-- </div> -->
			<!-- </div> -->
			<div style="position:absolute;bottom:0;padding:5px" >
				<div width="200px" style="float:left" align='right'>
					<label class="btnUpload" material_id='<?= $val->id ?>' category_id="5" >upload</label>
				</div>

				<div width="200px" style="float:right" align='right'>
					<label class="btnShow-suggestion" id='show_<?= $val->id ?>' img_path="<?= $img_path  ?>" suggestion="<?= $suggestion ?>" title="Suggestion Images of '<?= $val->title ?>'" >show images</label>
				</div>
				<br style="clear:both" />
				<input type="checkbox" id="display_suggestion_<?= $val->id ?>" class="display_suggestion" <?= ($val->display_suggestion == 1) ?  'checked="checked"' : NULL ; ?>  />
				<label for="display_suggestion_<?= $val->id ?>">Display Suggestion</label>
			</div>
		</div>
		<?
	} ?>


	
</div>
<br style="clear:both;">



<div id="images-popup"  style="display:none" >
	<div class="title" >
		<div style= "float:right">
			<img  src="<?= IMG_PATH . 'page/button/icon-close.gif?5454' ?>" id="close-popup"  />
		</div>
		<div style= "float:left;">
			<label id="title_label" >IMAGES</label>
		</div>
	</div>
	<br style="clear:both;">

	<? $src = $img_path . 'no-photo.png'; ?>
	<div id="suggestion-box"  style="height:250px;overflow:auto"  >
		<div style="width:170px;float:left;" >
			<img  src="<?= $src ?>"   />
		</div>
		
	</div>
</div>
