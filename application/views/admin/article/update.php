CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$items = $tpl['items'];

	$data = null;
	if (isset($tpl['data'][0]))
	{
		$data = $tpl['data'][0];
	} 
	
?>

<form action="<?= INDEX_URL . 'adminArticles/update' ?>" method="post" class="form" >

	<input type="hidden" name="article_update" value="1" />
	<input type="hidden" name="id" value=" <?=  ($data) ? $data->id : set_value('id')  ?>" />

	
	<p>
		<label class="title">Item</label>
		<select name="item_id" class="text w400" >
			<option value="" <?php echo $this->form_validation->set_select('item_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($items as $val) { ?>
				<option <?=  ((($data) ? $data->item_id : set_value('item_id')) == $val->id) ? 'selected' : NULL  ?>  value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('item_id', $val->id); ?>  ><?= $val->category_name . ' - ' . $val->item_name ?></option>
          		<?
          	}
			?>
		</select>		
		<div class="error_note"><?= form_error('item_id') ?></div>

	</p>

	<p>
		<label class="title">Title</label>
		<input type="text" name="title" class="text w100 " value="<?= ($data) ? $data->title : set_value('title') ?>" />

		<div class="error_note"><?= form_error('title') ?></div>
	</p>

	<p>
		<label class="title">Code</label>
		<input type="text" name="code" class="text w100 " value="<?= ($data) ? $data->code : set_value('code') ?>" />

		<div class="error_note"><?= form_error('code') ?></div>
	</p>
	
	<p>
		<label class="title">Specification</label>
		<input type="text" name="spec" class="text w50 " value="<?= ($data) ? $data->spec : set_value('spec') ?>" />

	</p>

	<p>
		<label class="title">Info</label>
		<input type="text" name="other_info" class="text w100 " value="<?= ($data) ? $data->other_info : set_value('other_info') ?>" />

	</p>

	
	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

