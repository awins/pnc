CREATE NEW MATERIAL
<? 
	if (isset($tpl['status']))
	{
		printNotice($tpl['status']);
		
	} 
	$items = $tpl['items'];
?>

<form action="<?= INDEX_URL . 'adminArticles/create' ?>" method="post" class="form" >

	<input type="hidden" name="article_create" value="1" />

	
	<p>
		<label class="title">Item</label>
		<select name="item_id" class="text w400" >
			<option value="" <?php echo $this->form_validation->set_select('item_id', ''); ?> selected='selected' >CHOOSE</option>
			<?php 
          	foreach ($items as $val) { ?>
				<option value="<?= $val->id ?>" <?php echo $this->form_validation->set_select('item_id', $val->id); ?>  ><?= $val->category_name . ' - ' . $val->item_name ?></option>
          		<?
          	}
			?>
		</select>		
		<div class="error_note"><?= form_error('item_id') ?></div>

	</p>

	<p>
		<label class="title">Title</label>
		<input type="text" name="title" class="text w100 " value="<?= set_value('title') ?>" />

		<div class="error_note"><?= form_error('title') ?></div>
	</p>

	<p>
		<label class="title">Code</label>
		<input type="text" name="code" class="text w100 " value="<?= set_value('code') ?>" />

		<div class="error_note"><?= form_error('code') ?></div>
	</p>
	
	<p>
		<label class="title">Specification</label>
		<input type="text" name="spec" class="text w450 " value="<?= set_value('spec') ?>" />

	</p>

	<p>
		<label class="title">Info</label>
		<input type="text" name="other_info" class="text w450 " value="<?= set_value('other_info') ?>" />

	</p>

	

	<p><label class="title">&nbsp;</label><input type="submit" value="Save" class="button button_save" /></p>
</form>

