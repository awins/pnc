<!DOCTYPE html>
<html >
<head>
	<meta charset="utf-8">
	<title>PNC ADMIN</title>
	<script src="<?= LIBS_PATH .'jquery/jquery-1.6.4.min.js' ?>" language="javascript" type="text/javascript"> </script>
	<script src="<?= JS_PATH .'admin.js?' . rand() ?>" language="javascript" type="text/javascript"> </script>
	<link href="<?= CSS_PATH .'admin.css?' . rand() ?>" rel="stylesheet" type="text/css" />

	<?php
		$rand = rand();
        if (isset($tpl['js']) && count($tpl['js'] > 0 ) ) {
            foreach ($tpl['js'] as  $value) { ?>
                <script src="<?= JS_PATH . $value . '?' .  $rand ?>" type="text/javascript"></script>
                <?php
            }
        }
    
        if (isset($tpl['css']) && count($tpl['css'] > 0 ) ) {
            foreach ($tpl['css'] as  $value) { ?>
                <link rel="stylesheet" href="<?= CSS_PATH . $value . '?' .  $rand ?>" type="text/css" media="screen" /> 
                <?php
            }
        }
    ?>

	<?= (isset($xinha_inclusion)) ? $xinha_inclusion : NULL; ?>


</head>
<body>
	<div id="header" >
		<div style="color:white;font-size:14px;font-weight:bold" >
			&nbsp;PICK EN CHOOSE LOGO HERE
		</div>
		<div>
			<ul class="main-menu">
				
				<li >
					<a class="menu-item" id="logout" href="<?= INDEX_URL . 'admin/logout' ?>">LOGOUT</a>
				</li>

				<li > 	
					<a class="menu-item" id="members" href="#">MEMBERS</a>
				</li>

				<li >
					<a class="menu-item" id="content" href="<?= INDEX_URL . 'adminContents' ?>">CONTENTS</a>
				</li>

				<li >
					<a class="menu-item" id="display" href="#">DISPLAY</a>
				</li>

				<li > 	
					<a class="menu-item" id="materials" href="#">MATERIALS</a>
				</li>

				<li >
					<a class="menu-item" id="master" href="#">MASTER</a>
				</li>
			</ul>
			<br><br>
			<div id="div-sub-menu">
				<ul class="sub-menu" id="sub-materials" rel="" >
					<li>
						<a href="<?= INDEX_URL . 'adminMaterials' ?>" >LIST</a>
					</li>
					<li>
						<a href="<?= INDEX_URL . 'adminMaterials/upload' ?>" >UPLOAD</a>
					</li>

				</ul>
			
				<ul class="sub-menu" id="sub-master" rel="">
					<li>
						<a href="<?= INDEX_URL . 'adminBrands' ?>" >BRANDS</a>
					</li>
					<li>
						<a href="<?= INDEX_URL . 'adminArticles' ?>" >ARTICLES</a>
					</li>
					<li>
						<a href="<?= INDEX_URL . 'adminItems' ?>" >ITEMS</a>
					</li>
				</ul>

				<ul class="sub-menu" id="sub-display" rel="">
					<li>
						<a href="<?= INDEX_URL . 'adminMaterials/display/suggestion' ?>" >SUGGESTION</a>
					</li>
					<li>
						<a href="<?= INDEX_URL . 'adminMaterials/display/gallery' ?>" >GALLERY</a>
					</li>
					<li>
						<a href="<?= INDEX_URL . 'adminMaterials/display/mixmatch' ?>" >MIX & MATCH</a>
					</li>
				</ul>

				<ul class="sub-menu" id="sub-members" rel="">
					<li>
						<a href="<?= INDEX_URL . 'adminMembers/' ?>" >LIST</a>
					</li>

					<li>
						<a href="<?= INDEX_URL . 'adminMembers/sales' ?>" >SALES</a>
					</li>

				</ul>

			</div>
		</div>
	</div>
	<div id="main-body" >
		<br>