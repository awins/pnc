
            <!---------------------------------- Setting pageContent ---------------------------------->    
    
        
<? 
    $editors_pick_view = '&nbsp;';
    $news_update_view = NULL;
    $todays_promo_view = NULL;
    $new_release_view = NULL;
    foreach ($tpl['data'] as $val) {
        switch ($val->section) {
            case 'editors_pick':
                $editors_pick_view = $val->home_view;
                break;
            case 'news_update':
                $news_update_view = $val->home_view;
                break;
            case 'todays_promo':
                $todays_promo_view = $val->home_view;
                break;
            
            case 'new_release':
                $new_release_view  = $val->home_view;
                break;
            default:
                # code...
                break;
        }
    }

?>
<div style="height:660px;width:1123px;overflow:hidden;clear:both" >

<table style="width:1123px;height:660px;overflow:hidden"  border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="300" id="editorPick">
            <div style="height:45px;width:300px;overflow:hidden;clear:both" >
                <img src="<?= IMG_PATH . 'page/home/EditorPick.png' ?>" width="300" height="45" /> 
            </div>
        </td>

        <td width="523" rowspan="7" bgcolor="#FFFFFF" style="vertical-align:top">

            <div id="container" style="height:660px;width:523px;overflow:hidden;clear:both">
      
                <div id="slider">
                    <img src="<?= IMG_PATH . 'page/home/display/mainview.jpg' ?>" width="523" height="660px" />
                    <img src="<?= IMG_PATH . 'page/home/display/display1.png' ?>" width="523" height="660px" />
                    <img src="<?= IMG_PATH . 'page/home/display/display2.png' ?>" width="523" height="660px" />
                    <img src="<?= IMG_PATH . 'page/home/display/display3.png' ?>" width="523" height="660px" /> 
                    <img src="<?= IMG_PATH . 'page/home/display/display4.png' ?>" width="523" height="660px" /> 
                    <img src="<?= IMG_PATH . 'page/home/display/display5.png' ?>" width="523" height="660px" /> 
                    <img src="<?= IMG_PATH . 'page/home/display/display6.png' ?>" width="523" height="660px" /> 
                    <img src="<?= IMG_PATH . 'page/home/display/display7.png' ?>" width="523" height="660px" />
                    <img src="<?= IMG_PATH . 'page/home/display/display8.png' ?>" width="523" height="660px" />
                    <img src="<?= IMG_PATH . 'page/home/display/display9.png' ?>" width="523" height="660px" />
                    <img src="<?= IMG_PATH . 'page/home/display/display10.png' ?>" width="523" height="660px" />
                </div>
            </div> 
        </td>

        <td width="300" bgcolor="#E49182" class="latestITEM"  >
            <!-- &nbsp;&nbsp;LATES ITEMS -->
            <div style="height:45px;width:300px;overflow:hidden;clear:both">
                <img src="<?= IMG_PATH . 'page/home/Latestitem.png' ?>" width="300" height="45" /> 
            </div>
        </td>
    </tr>
    
    <tr height="250" bgcolor="#E2E2E4">
        <td rowspan="1" >
            <a href="<?= INDEX_URL . 'editorPick' ?>">
                 <!-- <img src="<?= IMG_PATH . 'page/home/EditorPic.png' ?>" width="300" height="250" alt="editorPick" />    -->
                <div style="height:250px;width:300px;overflow:hidden;clear:both">
                   <?= $editors_pick_view ?>
                </div>
            </a>
        </td>
        <td class="latestITEM" rowspan="2" height="250px"  >
            <div style="height:250px;width:300px;overflow:hidden;clear:both">

                 <table width="300" border="0" cellspacing="5">
                    <tr style="vertical-align:top;height:130px">
                        <td><img src="<?= IMG_PATH . 'page/home/dis3.png' ?>" width="90" height="100" /></td>
                        <td><img src="<?= IMG_PATH . 'page/home/dis1.png' ?>" width="90" height="100" /></td>
                        <td><img src="<?= IMG_PATH . 'page/home/dis2.png' ?>" width="90" height="100" /></td>
                    </tr>
                    <tr  style="vertical-align:top;height:130px">
                        <td><img src="<?= IMG_PATH . 'page/home/dis3.png' ?>" width="90" height="100" /></td>
                        <td><img src="<?= IMG_PATH . 'page/home/dis1.png' ?>" width="90" height="100" /></td>
                        <td><img src="<?= IMG_PATH . 'page/home/dis2.png' ?>" width="90" height="100" /></td>
                    </tr>
                    <tr  style="vertical-align:top;">
                        <td></td>
                        <td></td>
                        <td style="text-align:right;font-size:14px" >MORE</td>
                    </tr>

                </table>
            </div>
        </td>
    </tr>
    <!-- <tr>
        <td rowspan="1">
           
        </td>
    </tr> -->
    <tr>
        <td height="115" id="newsUpdate" >
            <div style="height:45px;width:300px;overflow:hidden;clear:both">
                <img src="<?= IMG_PATH . 'page/home/news update.png' ?>" width="300" height="45" />
            </div>
        </td>
        
        
    </tr>
    <tr>
        <td rowspan="3" bgcolor="#E2E2E4" height="157" style="vertical-align:top" >
                <div style= "float:top:height:157px;width:290px;margin: 10px 5px 0 5px;overflow:hidden;clear:both;font-size:12px" >
                    <?= $news_update_view ?>
                </div>
                <div align="right" style="margin-right:10px;position:relative;float:bottom">
                    <a href="<?= INDEX_URL . 'newsUpdate' ?>">More...</a>
                </div>
        </td>
        <td id="">
            <div style="height:45px;width:300px;overflow:hidden;clear:both">

                <table width="300" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td id="followUS" height="45">FOLLOW US ON</td>
                    <td bgcolor="#FFFFFF"><a href="#"><img src="<?= IMG_PATH . 'page/home/twitter.jpg' ?>" width="41" height="41" alt="twitter" /></a></td>
                    <td bgcolor="#FFFFFF"><a href="#"><img src="<?= IMG_PATH . 'page/home/facebook.jpg' ?>" width="41" height="41" alt="fb" /></a></td>
                    <td bgcolor="#FFFFFF"><a href="#"><img src="<?= IMG_PATH . 'page/home/youtube.png' ?>" width="41" height="41" alt="yt" /></a></td>
                  </tr>
                </table>
            </div>
        </td>
    </tr>
    
    <tr>
        <td id="newRelease">
            <div style="height:45px;width:300px;overflow:hidden;clear:both">
                NEW RELEASE
            </div>
        </td>
    </tr>
    <tr>
        <td rowspan="2" bgcolor="#FFFFFF">
            <div>
                <div align="center">
                    <a href="<?= INDEX_URL . 'newRelease' ?>">
                        <div style="height:250px;width:300px;overflow:hidden;clear:both">
                            <img src="<?= IMG_PATH . 'page/home/newrelease.png' ?>" width="300" height="250" alt="newRelease" />   
                            <?// $new_release_view ?>
                        </div>
                    </a>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="300" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <a href="<?= INDEX_URL . 'todaysPromo' ?>">
                            <div style="height:123px;width:150px;overflow:hidden;clear:both">
                                <img src="<?= IMG_PATH . 'page/home/todayspromo.png' ?>" width="150" height="123" alt="todayPromo" />
                            </div>
                        </a>
                    </td>
                    <td>
                        <div style="height:123px;width:150px;overflow:hidden;clear:both">
                            <img src="<?= IMG_PATH . 'page/home/todayPic.png' ?>" width="150" height="123" alt="todayPick" />
                        </div>
                    </td>
                </tr>
            </table>
        
            <?// $todays_promo_view ?>
        </td>
    </tr>
</table>
    </div>       
        


    










    
    
    