<div id="header-content">
    NEW RELEASE
</div>


<div id="box-content" >
    <div id="box-main">
    	<img src="<?= IMG_PATH . 'uploads/new-release-main.png' ?>">
    </div>
    <div id="box-image-module">
    	<img src="<?= IMG_PATH . 'uploads/new-release-img-module2.png' ?>">
    	<img src="<?= IMG_PATH . 'uploads/new-release-img-module3.png' ?>">

    </div>
    <div id="background-header">
    	<div id="box-header" >
    		<div id="header-module"> 
		    	<img src="<?= IMG_PATH . 'uploads/new-release-header.png' ?>">
    		</div>
    		<div id="sub-header-module" class="right-align">
    			by Henidar Amroe
    		</div>
    		<div id="content-module">
    			<div>
	    			"Tampakan terawang dalam jiwa, bumikan tanahku dan manusiakan hatiku. semua jadi satu untuk mencintai dan berkreasi dalam karsaku"
	    		</div>
	    		<br >
    			<span class="right-align">Henidar Amroe
    		</div>
    	</div>

    </div>
    <div style="clear:both"> </div>
</div>
  