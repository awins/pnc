<div id="header-content">
    EDITOR'S PICK
    <span>June 04, 2013</span>
</div>

<div id="box-content" >
    <div id="box-main" >
        <img src="<?= IMG_PATH ?>uploads/editor-pick-main-image.png">
    </div>
    <div id="box-header"> 
        <div id="header-module">
            Cheerly Fearless
        </div>
        <div id="sub-header-module">
            in Office
        </div>
        <div id="content-module">
            Dear Cheerly Fearless,
            <br >
            <br >
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
    </div>
    <div id="box-detail">
        <div class="detail-image"> 
            <!-- detail image TOP -->
            <img src="<?= IMG_PATH ?>uploads/editor-pick-detail-top.png">

        </div>
        <div class="detail-description"> 
            <!-- detail description TOP -->
            <span class="title" >TOP</span>
            <span class="desc" >Celcom's highly acclaimed Service is now made available online, 24 hours a day.</span>

        </div>
        <div class="detail-image"> 
            <!-- detail image HEAD -->
            <img src="<?= IMG_PATH ?>uploads/editor-pick-detail-head.png">
        </div>
        <div class="detail-description"> 
            <!-- detail description HEAD -->
            <span class="title" >HEAD</span>
            <span class="desc" >Celcom's highly acclaimed Service is now made available online, 24 hours a day.</span>
        </div>
        <div class="detail-image"> 
            <!-- detail image PANTS -->
            <img src="<?= IMG_PATH ?>uploads/editor-pick-detail-pants.png">

        </div>
        <div class="detail-description"> 
            <!-- detail description PANTS -->
            <span class="title" >PANTS</span>
            <span class="desc" >Celcom's highly acclaimed Service is now made available online, 24 hours a day.</span>
        </div>
        <div class="detail-image"> 
            <!-- detail image SHOES -->
            <img src="<?= IMG_PATH ?>uploads/editor-pick-detail-shoes.png">

        </div>
        <div class="detail-description"> 
            <!-- detail description SHOES -->
            <span class="title" >SHOES</span>
            <span class="desc" >Celcom's highly acclaimed Service is now made available online, 24 hours a day.</span>
        </div>
        
        
    </div>
</div>
  