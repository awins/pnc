<?
    if (isset($tpl['member'])) {
        $member = $tpl['member'];
    } else {
        $member = NULL;
    }
    $controller = $this->uri->rsegment(1);

    $rand = 11;
    

    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pick en Choose</title>

    <link rel="stylesheet" href="<?= CSS_PATH . 'front.css?' . $rand ?>" type="text/css" media="screen" /> 
    
    <?php
       
        if (isset($tpl['css']) && count($tpl['css'] > 0 ) ) {
            foreach ($tpl['css'] as  $value) { ?>
                <link rel="stylesheet" href="<?= CSS_PATH . $value . '?' .  $rand ?>" type="text/css" media="screen" /> 
                <?php
            }
        }
    ?>
</head>

<body>

    <div  align="center" id="mainWrapper">

    <input id="member_id" type="hidden" value="<?= (is_null($member) || !$member ) ? 0 : $member->id ?>" />
<!---------------------------------- Setting PageHeader ------------------------------------>
    
        <div id="pageHeader">
   
            <table width="1123px" height="79" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="820px" >
                        <div class="main_menu gray" id="logo" >
                            <a href="<?= ROOT_URL  ?>" >
                                &nbsp;
                            </a>
                        </div>

                        <div class="main_menu <?= ($controller == 'home') ? 'white' : 'gray' ?>" id="home" >
                            <a href="<?= ROOT_URL  ?>" >
                                &nbsp;
                            </a>
                        </div>

                        <div class="main_menu <?= ($controller == 'mixmatch') ? 'white' : 'gray' ?>" id="mixmatch" >
                            <a href="<?= INDEX_URL . 'mixmatch'  ?>" >
                                &nbsp;
                            </a>
                        </div>

                        <div class="main_menu <?= ($controller == 'gallery') ? 'white' : 'gray' ?>" id="gallery" >
                            <a href="<?= INDEX_URL  . 'gallery' ?>" >
                                &nbsp;
                            </a>
                        </div>

                        <div class="main_menu <?= ($controller == 'pncOnly') ? 'white' : 'gray' ?>" id="pncOnly" >
                            <a href="<?= INDEX_URL . 'pncOnly'  ?>" >
                                &nbsp;
                            </a>
                        </div>

                        <div class="main_menu <?= ($controller == 'contactUs') ? 'white' : 'gray' ?>" id="contactUs" >
                            <a href="<?= INDEX_URL  . 'home/contactUs' ?>" >
                                &nbsp;
                            </a>
                        </div>

                    </td>

                    <td width="303px" style="vertical-align:top">
                        <div style="height:79px;width:303px;overflow:hidden;clear:both" >
                            <? if (is_null($member) || !$member ) 
                            { ?>
                                <form id="loginForm" class="form" method="post" action="<?= INDEX_URL . 'member/login'      ?>" >
                                    <input type="hidden" name="login" value="1" />

                                    <input type="submit" style="position: absolute; left: -9999px"/>
                                    <table width="303px" height="79px" border="0" cellspacing="0" cellpadding="0" style="padding-top:5px">
                                        
                                        <tr>
                                            <td colspan="2">
                                                <input type="text" name="email" id="email"  size="27" placeholder="Email" autocomplete="off"/>
                                            </td>
                                            
                                           
                                            <td id="login">
                                                <a href="#" >LOGIN>></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input type="password" name="password" id="password"  size="27" placeholder="password" autocomplete="off" />
                                            </td>
                                            
                                            <td id="signUp">
                                                <a href="<?= INDEX_URL . 'member/register' ?>" >SIGNUP>></a>
                                            </td>
                                        </tr>
                                        <tr>
                                           <td width="100px"  id="memberME">
                                                <input type="checkbox" name="checkRemember" id="checkRemember"  />
                                                 <label for="checkRemember"  >Remember Me</label>
                                            </td>
                                            <td width="100px" id="forgotPasswd" colspan="2">
                                                <label  >Forget Password</label>
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </form>
                                <?
                            } else { ?>
                                <table width="303px" border="0" cellspacing="0" cellpadding="0">
                                    <tr height="35px">
                                        <td colspan="3" style="vertical-align: middle;line-height: 35px" >
                                            <span class="white" style="color:black;text-shadow:none">Welcome, </span><span class="white"> <?= $member->member_name ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label id="showWishList" class="white  pointer">MY WISHLIST (0) >></label>
                                            
                                        </td>
                                        <td>
                                            <label class="white  pointer">MY ACCOUNT >></label>

                                        </td>
                                        <td>
                                            <label class="white pointer" id="logout"> LOGOUT >></label>
                                        </td>
                                    </tr>
                                </table>
                                <?
                            } ?>
                        </div>
                        
                    </td>
                </tr>
            </table>
        </div>

        <div id="pageContent">



