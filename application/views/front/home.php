
            <!---------------------------------- Setting pageContent ---------------------------------->    
    
        
<? 
    $editors_pick_view = '&nbsp;';
    $news_update_view = NULL;
    $todays_promo_view = NULL;
    $new_release_view = NULL;
    $latest_items = $tpl['latest_items'];
    foreach ($tpl['data'] as $val) {
        switch ($val->section) {
            case 'editors_pick':
                $editors_pick_view = $val->home_view;
                break;
            case 'news_update':
                $news_update_view = $val->home_view;
                break;
            case 'todays_promo':
                $todays_promo_view = $val->home_view;
                break;
            
            case 'new_release':
                $new_release_view  = $val->home_view;
                break;
            default:
                # code...
                break;
        }
    }

?>
<div style="height:650px;width:1123px;background-color:white;overflow: hidden" >
    <div  style="height:650px;float:left">
        <div class="frame" style="height:45px;float:top">
            <img src="<?= IMG_PATH . 'page/home/EditorPick.png' ?>" width="300" height="45" /> 
        </div>
        <div class="frame" style="height:250px;float:top">
            <a href="<?= INDEX_URL . 'home/editorsPick' ?>">
                <?= $editors_pick_view ?>   
            </a>
        </div>
        <div class="frame" style="height:45px;float:top">
            <img src="<?= IMG_PATH . 'page/home/news update.png' ?>" width="300" height="45" />
        </div>
        <div  class="frame" style="height:167px;width:280px;float:top;padding: 10px 10px 10px 10px">
            <div style="width:280px;height:87px;float:top;overflow:hidden;font-size:11px;padding-bottom: 5px" >
                    <?= $news_update_view ?>
            </div>
            <div  style="width:280px;height:10px;float:top;margin-bottom: 5px;text-align:right;color:red" >
                MORE...
            </div>

            <div style="width:280px;height:45px;float:top;overflow:hidden;font-size:11px;text-align:left" >
                Lorem Ipsum dolor si ismet1
                <br />
                Lorem Ipsum dolor si ismet2
                <br />
                Lorem Ipsum dolor si ismet3
            </div>
            <div style="width:280px;height:10px;float:top;text-align:right;color:red" >
                MORE...
            </div>
        </div>
        <div class="frame" style="height:123px;float:top">
            <div style="height:123px;float:left;position: relative;width:150px;overflow:hidden;">
                <a href="<?= INDEX_URL . 'home/todaysPromo' ?>">
                    <img src="<?= IMG_PATH . 'page/home/todayspromo.png' ?>" width="150" height="123" alt="todayPromo" />
                </a>
            </div>
            <div style="height:123px;float:left;position: relative;width:150px;overflow:hidden;">
                <img src="<?= IMG_PATH . 'page/home/todayPic.png' ?>" width="150" height="123" alt="todayPick" />
            </div>
        </div>
    </div>

    <div class="frame" style="width:523px;height:650px;float:left">
        <div id="container" class="frame" style="width:523px;height:650px">
            <div id="slider">
                <img src="<?= IMG_PATH . 'page/home/display/mainview.jpg' ?>" width="523" height="650px" />
                <img src="<?= IMG_PATH . 'page/home/display/display1.png' ?>" width="523" height="650px" />
                <img src="<?= IMG_PATH . 'page/home/display/display2.png' ?>" width="523" height="650px" />
                <img src="<?= IMG_PATH . 'page/home/display/display3.png' ?>" width="523" height="650px" /> 
                <img src="<?= IMG_PATH . 'page/home/display/display4.png' ?>" width="523" height="650px" /> 
                <img src="<?= IMG_PATH . 'page/home/display/display5.png' ?>" width="523" height="650px" /> 
                <img src="<?= IMG_PATH . 'page/home/display/display6.png' ?>" width="523" height="650px" /> 
                <img src="<?= IMG_PATH . 'page/home/display/display7.png' ?>" width="523" height="650px" />
                <img src="<?= IMG_PATH . 'page/home/display/display8.png' ?>" width="523" height="650px" />
                <img src="<?= IMG_PATH . 'page/home/display/display9.png' ?>" width="523" height="650px" />
                <img src="<?= IMG_PATH . 'page/home/display/display10.png' ?>" width="523" height="650px" />
            </div>
        </div>
    </div>

    <div class="frame" style="height:650px;float:left">
        <div class="frame" style="height:55px;align:left">
            <div class="frame" height="10px">&nbsp;</div>
            <div class="more">
                FOLLOW US ON
            </div>
            <div syle="width:200px;float:left">
                <a href="#"><img src="<?= IMG_PATH . 'page/home/twitter.jpg' ?>" width="41" height="41" alt="twitter" /></a> &nbsp;
                <a href="#"><img src="<?= IMG_PATH . 'page/home/facebook.jpg' ?>" width="41" height="41" alt="fb" /></a> &nbsp;
                <a href="#"><img src="<?= IMG_PATH . 'page/home/youtube.png' ?>" width="41" height="41" alt="yt" /></a> &nbsp;
            </div>
        </div>
        <div class="frame" style="height:45px">
            <img src="<?= IMG_PATH . 'page/home/Latestitem.png' ?>" width="300" height="45" /> 
        </div>
        <div class="frame h260" >
           <? foreach ($latest_items as $item) { ?>
                <div style="float:left;margin: 5px;background-color: #F2F2F2" class="h120 w90" >
                    <a href="<?= INDEX_URL . 'material/detail/' . $item->id  ?>" >
                        <img src="<?= IMG_PATH . 'materials/T_G_' . $item->id . '.png'; ?>" width="90" height="120" />
                    </a>
                </div>
                <?
           } ?>

        </div>
        <div class="frame more w280" style="text-align:left">
            MORE...
        </div>
                
        <div class="frame" style="height:10px;text-align:left;width: 280px">
            <hr style="border-color: #ECECEC" />
        </div>

        <div class="frame" style="height:45px;font-size:22px;vertical-align:middle;line-height:45px;text-align:left;margin-left:10px;">
            NEW RELEASE
        </div>
        <div class="frame" style="height:210px;margin-left:10px;text-align:left">
                <a href="<?= INDEX_URL . 'home/newRelease' ?>">
                    <img src="<?= IMG_PATH . 'page/home/newrelease.png' ?>" width="260" height="187" alt="newRelease" />   
                    <?// $new_release_view ?>
                </a>
        </div>
    </div>

</div>       
        


    










    
    
    