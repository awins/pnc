<?
    if (isset($tpl)) {
        $member = $tpl['member'];
    } else {
        $member = NULL;
    }
    $controller = $this->uri->rsegment(1);

    $rand = rand();
    if ($controller == 'gallery' || $controller == 'mixmatch' || $controller == 'member' || $controller == 'pncOnly' ) {
        $menu_color = 'white.png'  ;
    } else {
        $menu_color = 'gray.png'  ;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pick en Choose</title>

    <link rel="stylesheet" href="<?= CSS_PATH . 'front.css?' . $rand ?>" type="text/css" media="screen" /> 
    <script src="<?= LIBS_PATH .'jquery/jquery-1.6.4.min.js' ?>" language="javascript" type="text/javascript"> </script>
    <script type="text/javascript" src="<?= JS_PATH . 'jquery-1.10.0.min.js' ?>"></script>
    <script type="text/javascript" src="<?= JS_PATH . 'jquery.cycle.all.js' ?>"></script>
    <script src="<?= JS_PATH .'front.js?' . $rand ?>" language="javascript" type="text/javascript"> </script>
    <?php
        if (isset($tpl['js']) && count($tpl['js'] > 0 ) ) {
            foreach ($tpl['js'] as  $value) { ?>
                <script src="<?= JS_PATH . $value . '?' .  $rand ?>" type="text/javascript"></script>
                <?php
            }
        }
    ?>

    <?php
        if (isset($tpl['css']) && count($tpl['css'] > 0 ) ) {
            foreach ($tpl['css'] as  $value) { ?>
                <link rel="stylesheet" href="<?= CSS_PATH . $value . '?' .  $rand ?>" type="text/css" media="screen" /> 
                <?php
            }
        }
    ?>
</head>

<body>

    <div  align="center" id="mainWrapper">

    <input id="member_id" type="hidden" value="<?= (is_null($member) || !$member ) ? 0 : $member->id ?>" />
<!---------------------------------- Setting PageHeader ------------------------------------>
    
        <div id="pageHeader">
   
            <table width="1123px" height="79" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="820px" >
                        <div>
                            <img src="<?= IMG_PATH . 'page/header/Logo.png' ?>" width="296" height="79" alt="logo" style="float:left" />
                            <a href="<?= ROOT_URL  ?>" class="main_menu" rel="home" >
                                <img src="<?= IMG_PATH . 'page/header/HOME' . $menu_color  ?>" width="100" height="79" id="home" style="float:left"  />
                            </a>
                            <a href="<?= INDEX_URL . 'mixmatch' ?>" class="main_menu" rel="mixmatch">
                                <img src="<?= IMG_PATH . 'page/header/MIXMATCH' . $menu_color  ?>" width="100" height="79" id="mixmatch"  style="float:left"  />
                            </a>
                            <a href="<?= INDEX_URL . 'gallery' ?>" class="main_menu" rel="gallery">
                                <img src="<?= IMG_PATH . 'page/header/GALLERY' . $menu_color  ?>" name="gallery" width="100" height="79" id="gallery" style="float:left"  />
                            </a>
                            <a href="<?= INDEX_URL . 'pncOnly' ?>" class="main_menu" rel="pncOnly">
                                <img src="<?= IMG_PATH . 'page/header/PnC' . $menu_color  ?>" width="100" height="79" id="pncOnly" style="float:left"  />
                            </a> 
                            <a href="<?= INDEX_URL . 'contact' ?>" class="main_menu" rel="contact">
                                <img src="<?= IMG_PATH . 'page/header/CONTACT' . $menu_color  ?>" width="100" height="79" id="contact" style="float:left"  />
                            </a> 
                        </div>
                    </td>
                    <td width="303px" style="vertical-align:top">
                        <? if (is_null($member) || !$member ) 
                        { ?>
                            <form id="loginForm" class="form" method="post" action="<?= INDEX_URL . 'member/login'      ?>" >
                                <input type="hidden" name="login" value="1" />

                                <input type="submit" style="position: absolute; left: -9999px"/>
                                <table width="303px" border="0" cellspacing="0" cellpadding="0" style="padding-top:10px">
                                    
                                    <tr>
                                        <td colspan="2">
                                            <input type="text" name="email" id="email"  size="25" placeholder="User Name or E-mail" autocomplete="off"/>
                                        </td>
                                        
                                       
                                        <td>
                                            <a href="#" id="login">LOGIN>></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input type="password" name="password" id="password"  size="25" placeholder="password" autocomplete="off" />
                                        </td>
                                        
                                        <td>
                                            <a href="<? INDEX_URL . '/member/register' ?>" id="signUp">SIGNUP>></a>
                                        </td>
                                    </tr>
                                    <tr>
                                       <td width="103px"  id="memberME">
                                            <input type="checkbox" name="checkRemember" id="checkRemember"  />
                                             <label for="checkRemember"  >Remember Me</label>
                                        </td>
                                        <td width="100px" id="forgotPasswd" colspan="2">
                                            
                                            <label  >Forget Password</label>
                                        </td>
                                        
                                    </tr>
                                </table>
                            </form>
                            <?
                        } else { ?>
                            <table width="303px" border="0" cellspacing="0" cellpadding="0">
                                <tr height="35px">
                                    <td colspan="3" >
                                        <span class="white" style="color:black;text-shadow:none">Welcome, </span><span class="white"> <?= $member->member_name ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label id="showWishList" class="white  pointer">MY WISHLIST (0) >></label>
                                        
                                    </td>
                                    <td>
                                        <label class="white  pointer">MY ACCOUNT >></label>

                                    </td>
                                    <td>
                                        <label class="white pointer" id="logout"> LOGOUT >></label>
                                    </td>
                                </tr>
                            </table>
                            <?
                        } ?>

                        
                    </td>
                </tr>
            </table>
        </div>

        <div id="pageContent">



