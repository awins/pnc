<? 
    $result = $tpl['result'];
    $category_id = $tpl['category_id'];
    $sub_category = $tpl['sub_category'];
    $page_number = $tpl['page_number'];
    $pages_count = $result['pages_count'];
    $items_count = $result['rows_count'];
    $start = (($page_number - 1) * $this->Material->row_per_page) ;
    $limit = $this->Material->row_per_page * ($page_number ) ;
    
    $item_start = ($page_number <= $pages_count) ? $start + 1 : 0;
    $end = $start + $this->Material->row_per_page + 1;
    if ($end > $items_count ) {
        $end = $items_count;
    }

    $shopping_cart = $tpl['shopping_cart'];

    $img_path = IMG_PATH  . 'materials/' ;
    $img_dir = IMG_DIR  . 'materials/' ;
//    $rand = '?' . rand();
?>

<input type="hidden" id="page_number" value="<?=$page_number ?>" />
<input type="hidden" id="page_count" value="<?=$pages_count ?>" />


     <div class="item-container" >
        <div class="display-item" id="display-item" >
            <div id="no-collection-msg" > NO COLLECTION SELECTED </div>
            <img id="cart-banner" src="<?= IMG_PATH . 'page/background/corner.png?1' ?>"  />
            <img id="new-banner" src="<?= IMG_PATH . 'page/background/new-banner.png' ?>"   />
        </div>
        <div class="detail-box" >
            <?
            for ($i=$item_start-1;$i< $end-1;$i++) { 
                $row = $result['data'][$i];
                ?>
                <div style="display:none" class="detail-item" id="detail_<?= $row->id  ?>" >
                    <div style="width: 270px; float:left">
                        TITLE:&nbsp;<?= strtoupper($row->title . ', ' . $row->item_name . ', ' . $row->category_name) ?>
                        <br>
                        ARTICLE CODE:&nbsp;#<?= $row->code ?>
                        <br>
                        PRICE:&nbsp;Rp.&nbsp;<?= number_format($row->reg_price) ?>
                        <br>
                        STOCK:&nbsp; <span id="stock_val_<?= $row->id ?>"> <?= $row->stock ?> </span> &nbsp; PCS
                    </div>

                    <div  id="product-detail"  >
                        <a href="<?= INDEX_URL . 'material/detail/' . $row->id ?>" >
                            <img style="max-width:100px"  src="<?= IMG_PATH . 'page/background/Gallery-Banner-Product Details Button-White.png' ?>">
                        </a>
                    </div>
                </div>
                <?

            $row = $result['data'][$i];
            $front =  'F_G_' . $row->id . '.png';
            $rear =  'R_G_' . $row->id . '.png';
            if (!file_exists($img_dir . $front)) {
                $front = 'no-photo.png';
            }
            $front = $img_path . $front;// . $rand ;

            if (!file_exists($img_dir . $rear)) {
                $rear = 'no-photo.png';
            }
            $rear = $img_path .  $rear  ;//. $rand ;
            ?>

            <input type="hidden" id="display_front_<?= $row->id  ?>" rel="<?= $front ?>" />
            <input type="hidden" id="display_rear_<?= $row->id  ?>" rel="<?= $rear ?>" />
            <input type="hidden" id="is_new_<?= $row->id  ?>" rel="<?= $row->is_new ?>" />

                <?
            } ?>
        </div>
        <div class="cart-container">
            <div id="addToCart" >
                &nbsp;
            </div>
            <div id="addToWishList" >
                &nbsp;
            </div>
            <div id="showCart" >
                &nbsp;
            </div>
        </div>
    </div>

    <div class="gallery-header" >
        <? 
        for ($i=8;$i>=1;$i--) { ?>
            <div class="tabCategory <?= ($i == $category_id) ? 'selected' : NULL ?>" id="category_<?= $i ?>" >
                &nbsp;
            </div>
            <?
        } ?>
        
    </div>

    <div class="gallery-main">
        <div class="thumbnail-container" >
            <?
            for ($i=$item_start-1;$i< $end-1;$i++) { 
                $row = $result['data'][$i];
                $src =  'T_G_' . $row->id . '.png';
                if (!file_exists($img_dir . $src)) {
                    $src = 'no-photo.png';
                }
                $thumbnail = $img_path .  $src ;//. $rand  ;
                ?>
                <div class="thumbnail"  >
                    <div class="thumbnail_img <?=  (in_array($row->id, $shopping_cart)) ? 'in-Cart' : NULL ?>" id="thumbnail_<?= $row->id ?>" style="background-image: url('<?= $thumbnail ?>');"  >
                        <img src="<?= IMG_PATH . 'page/background/corner.png?1' ?>"  >
                    </div>
                    <div class="info" ><?= $row->brand_name ?></div>
                    <div class="info">
                        <a href="<?= INDEX_URL . 'material/detail/' . $row->id ?>" >
                            info
                        </a>
                    </div>
                    
                </div>
                <?
            } ?>
        </div>
        <div class="gallery-banner" >
            <? 
            foreach ($sub_category as $key => $value) 
            { ?>
                <div class="sub-category <?= ($value[0]['id'] == $category_id) ? 'selected' : NULL ?> "   >
                    <div class="category_head" id="category_head_<?= $value[0]['id'] ?>" ><?= $key ?></div>
                    <?
                    foreach ($value as $val ) 
                    { ?>
                        <div  class="category_item" rel="<?= $val['id'] ?>" id="category_item_<?= $val['item_id'] ?>"><?= $val['name'] ?></div>
                        <?    
                    } ?>

                </div>
                <?
            } ?>
        </div>
    </div>
    <br class="clear" />

    <div class="gallery-footer">
        <div class="w160" >&nbsp;</div>
        <div class="change_view  selected" id="change-front" >&nbsp;</div>
        <div class="change_view" id="change-rear" >&nbsp;</div>
        <div class="w46" id="change-undo" >&nbsp;</div>
        <div class="w46" id="change-clear" >&nbsp;</div>
        <div class="w100" >&nbsp;</div>
        <div id="page-prev" class="<?= ($page_number == 1) ? 'disabled' : NULL ?>"  >&nbsp;</div>
        <div id="page-info" >

            <?= $item_start  . '-' . (($end == 0 ) ? 0 : (floatval($end) - 1)) . ' OF ' . (($items_count == 0 ) ? 0 : (floatval($items_count) - 1)) . ' COLLECTIONS' ?>
        </div>

        <div id="page-next" class="<?= ($page_number == $pages_count ) ? 'disabled' : NULL ?>" >&nbsp;</div>

    </div>
