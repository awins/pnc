<div id="header-content">
    TODAY'S PROMO
</div>


<div id="box-content" >
	<div id="box-detail">
		<!-- HEAD -->
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-head-1.png">

		</div>
		<div class="detail-description">
			<span class="center-align desc-title">HEAD</span>
			Celcom's highly acclaimed Service is now made available online, 24 hours a day.
			<span class="right-align">more</span>
		</div>
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-head-2.png">

		</div>

		<!-- TOP -->
		<div class="detail-description">
			<span class="center-align desc-title">TOP</span>
			Celcom's highly acclaimed Service is now made available online, 24 hours a day.
			<span class="right-align">more</span>
			
		</div>
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-top-1.png">
		</div>
		<div class="detail-description large">
			10%			
		</div>

		<!-- PANTS -->
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-pants-1.png">
		</div>
		<div class="detail-description">
			<span class="center-align desc-title">PANTS</span>
			Celcom's highly acclaimed Service is now made available online, 24 hours a day.
			<span class="right-align">more</span>
		</div>
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-pants-2.png">
		</div>

		<!-- SHOES -->
		<div class="detail-description  large">
			10%
		</div>
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-shoes-1.png">
		</div>
		<div class="detail-description">
			<span class="center-align desc-title">SHOES</span>
			Celcom's highly acclaimed Service is now made available online, 24 hours a day.
			<span class="right-align">more</span>
		</div>


		<!-- ACCESSORIES -->
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-accessories-1.png">
		</div>
		<div class="detail-description">
			<span class="center-align desc-title">ACCESSORIES</span>
			Celcom's highly acclaimed Service is now made available online, 24 hours a day.
			<span class="right-align">more</span>
		</div>
		<div class="detail-image">
            <img src="<?= IMG_PATH ?>uploads/todays-promo-accessories-2.png">
		</div>

	</div>
	<div id="box-main">
      	<img src="<?= IMG_PATH ?>uploads/todays-promo-main.png">

      	<div id="box-header">
      		<div id="header-module" class="right-align">
      			20%
      		</div>
      		<div id="sub-header-module" class="right-align">
      			off for six hour offering!
      		</div>
      		<div id="content-module">
      			Dapatkan Penawaran eksklusif hari ini dengan 20% off untuk produk-produk istimewa hanya dalam waktu eman jam ke depan. hanya dengan 'one click promo' anda akan bergaya!
      			<br>
      			<br>
      			<br>
      			<span class="right-align">Pick en Choose Team<span>

      		</div>
      	</div>

	</div>


</div>

<div style="clear:both"></div>