<?
        $total_rows = $tpl['rows'];
        $per_page = 10;

        $total_page = floor($total_rows / $per_page);
        if ( $total_rows % $per_page > 0 ) {
            $total_page++;
        }
        $member = @$tpl['member'];       
?>
    <div>
        <div id="box-post-header">
            <div id="post-review-button" class="<?= (!is_null($member) ) ? 'active' : NULL ?>" >
                POST A REVIEW
            </div>
            <? 
            if (is_null($member) ) { ?>
                <div style="float: right;margin-right: 10px" >
                    You must <a href="#" style="color:red">login</a> to post review. Not a member? <a href="<?= INDEX_URL ?>member/register" style="color:red">Register</a> here.
                </div>
                <?
            } ?>
        </div>
        <div style="height: 15px;" >
            <div style="float: right;height: 15px;"  >
                <div class="pagination" id="pagination_first" >First</div>
                <div class="pagination" id="pagination_back" >&lt;</div>
                <?
                for ($i=1; $i <= $total_page ; $i++) { ?>
                    <div class="pagination active " id="pagination_<?= $i ?>" ><?= $i ?></div>
                    <?
                } ?>
                <div class="pagination" id="pagination_next" >&gt;</div>
                <div class="pagination" id="pagination_last">Last</div>
            </div>
        </div>
    </div>
    <div id="review-header" style="padding: 10px 30px 10px 5px;text-align:left;background-color: #F8F8F4;border-top: 1px solid #DFDFDC;border-left: 1px solid #DFDFDC;border-right: 1px solid #DFDFDC;color: #626364">
        <span style="font-weight: bold;font-style: italic;"><?= $total_rows .' Comments on ' . urldecode($tpl['material_title']) ?></span>
        <?
        if ($total_rows == 0) { ?>
            <br><br>
            <span style="">There is no review for this product yet. Be the first to review it, login or register if you are not a member</span>
            <br>
            <?
        } ?>

    </div>
    <div id="box-review" style="max-height: 530px;margin-bottom: 10px;text-align:left;background-color: #F8F8F4;overflow: auto;border-bottom: 1px solid #DFDFDC;border-left: 1px solid #DFDFDC;border-right: 1px solid #DFDFDC;color: #626364"> 
        &nbsp;
    </div>
