<? 
    $result = $tpl['result'];
     
    $items_count = $result['rows_count'];
    $shopping_cart = $tpl['shopping_cart'];
    $size_range = $tpl['size_range'];

    $img_path = IMG_PATH  . 'materials/' ;
    $img_dir = IMG_DIR  . 'materials/' ;
//    $rand = '?' . rand();
    $spec = NULL;
    $info = NULL;
    
?>
    <input type="hidden" id="brand_id" value="<?= $result['data'][0]->brand_id ?>" />
    
    <div class="item-container" >
        <div class="display-item" >
           <?
            for ($i=0;$i< $items_count ;$i++) { 
                $row = $result['data'][$i];
                $front =  'F_G_' . $row->id . '.png';
                $rear =  'R_G_' . $row->id . '.png';
                if (!file_exists($img_dir . $front)) {
                    $front = 'no-photo.png';
                }
                $front = $img_path . $front;// . $rand;

                if (!file_exists($img_dir . $rear)) {
                    $rear = 'no-photo.png';
                }
                $rear = $img_path .  $rear ;//. $rand ;
                ?>
                <img style="display:none" class="display_front_img" id="display_front_<?= $row->id  ?>" src="<?= $front ?>">
                <img style="display:none" class="display_rear_img" id="display_rear_<?= $row->id  ?>" src="<?= $rear ?>">
                <input type="hidden" id="stock_holder_<?= $row->id  ?>" value="<?= $row->stock  ?>" />
                <input type="hidden" id="spec_holder_<?= $row->id  ?>" value="<?= $row->spec  ?>" />
                <input type="hidden" id="info_holder_<?= $row->id  ?>" value="<?= $row->other_info  ?>" />
                <?
            } ?>

        </div>
        <div class="detail-box" >
            <?
            for ($i=0;$i< $items_count;$i++) { 
                $row = $result['data'][$i];
                ?>
                <div style="display:none" class="detail-item" id="detail_<?= $row->id  ?>" >
                    <div>
                        TITLE:&nbsp;<?= strtoupper($row->title . ', ' . $row->item_name . ', ' . $row->category_name) ?>
                        <br>
                        ARTICLE CODE:&nbsp;#<?= $row->code ?>
                        <br>
                        PRICE:&nbsp;Rp.&nbsp;<?= number_format($row->reg_price) ?>
                        <br>
                        STOCK:&nbsp; <span id="stock_val_<?= $row->id ?>"> <?= $row->stock ?> </span> &nbsp; PCS
                    </div>

                </div>
                <?
                if ($i == 1) {
                    $spec = $row->spec;
                    $info = $row->other_info;
                }
            } ?>
            <div  id="product-detail" style="font-size: 14px;font-weight: normal"  >
                <a href="<?= INDEX_URL . 'gallery' ?>" >
                    BACK TO GALLERY
                </a> 
            </div>
        </div>
        <div class="cart-container">
            <div id="addToCart" >
                &nbsp;
            </div>
            <div id="addToWishList" >
                &nbsp;
            </div>
            <div id="showCart" >
                &nbsp;
            </div>
        </div>
    </div>

    <div class="detail-header" >
        <div class="tabMenu" id="product_review" >
            PRODUCT REVIEW
        </div>

        <div class="tabMenu " id="brand_info" >
            BRAND INFORMATION
        </div>

        <div class="tabMenu selected" id="product_info" >
            PRODUCT INFORMATION
        </div>
        
        
    </div>

    <div class="gallery-main" id="box-product_info" filled="true">
        <div class="detail-title"  >
            COLOR RANGE
        </div>
        <div class="top-container" >
            <div class="thumbnail-container"  >

                <?
                for ($i=0;$i< $items_count ;$i++) { 
                    $row = $result['data'][$i];
                    $src =  'T_G_' . $row->id . '.png';
                    if (!file_exists($img_dir . $src)) {
                        $src = 'no-photo.png';
                    }
                    $thumbnail = $img_path .  $src ;//. $rand  ;
                    ?>
                    <div class="thumbnail"  >
                        <div class="image ">
                            <img class="thumbnail_img <?=  (array_key_exists($row->id, $shopping_cart)) ? 'in-Cart' : NULL ?>" id="thumbnail_<?= $row->id ?>" src="<?= $thumbnail ?>"  >
                        </div>
                        <div >Brand: <?= $row->brand_name ?></div>
                        <div >Title: <span id='title_<?= $row->id ?>'><?= $row->title ?></span></div>
                        <div >Article Code: <?= $row->code ?></div>
                        <div >Reg. Price: <?= $row->reg_price ?></div>
                        <div >Disc. Price: <?= $row->disc_price ?></div>
                    </div>
                    <?
                } ?>
            </div>
            <div class="thumbnail-nav">
                <div id="thumbnail-prev" >
                    &lt;&nbsp;PREVIOUS
                </div>
                <div id="thumbnail-next">
                    NEXT >
                </div>
            </div>
        </div>
        <div class="detail-title"  >
            SIZE RANGE
        </div>
        <div class="detail-value" id="detail-size" style="margin-left: 30px; font-size: 14px; font-weight: bold"  >
            <? foreach ($size_range as $val) { 
                echo $val->size . '&nbsp;&nbsp;&nbsp;';
            } ?>
        </div>
        <div class="detail-title"   >
            STOCK AVAILABILITY : <span id="detail-stock">-</span> PIECES
        </div>
        <div class="detail-title"  >
            SPESIFICATION
        </div>
        <div class="detail-value" id="detail-info" >
            <?= $spec ?>
        </div>
        <div class="detail-title"   >
            OTHER INFO
        </div>
        <div class="detail-value"  id="detail-spec" >
            <?= $info ?>
        </div>

    </div>

    <div class="gallery-main" id="box-brand_info" style="display: none" filled="">
        &nbsp;
    </div>

    <div class="gallery-main" id="box-product_review" style="display: none" filled="">
        &nbsp;
    </div>

    <br class="clear" />

    <div class="gallery-footer">
        <div class="w160" >&nbsp;</div>
        <div class="change_view  selected" id="change-front" >&nbsp;</div>
        <div class="change_view" id="change-rear" >&nbsp;</div>
        <div class="w46" id="change-undo" >&nbsp;</div>
        <div class="w46" id="change-clear" >&nbsp;</div>
        <div class="w100" >&nbsp;</div>
        <div >&nbsp;</div>
        <div >

            &nbsp;
        </div>

        <div >&nbsp;</div>

    </div>
