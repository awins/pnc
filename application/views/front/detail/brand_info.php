        <?
            $data = $tpl['data'];
        ?>

        <div class="detail-title"  >
            BRAND LOGO
        </div>
        <div class="top-container" >
            <? 
            $logo = IMG_PATH . 'brands/' . $data->brand_image; 
            if (!file_exists($logo)) {
                $logo = IMG_PATH . 'brands/no-photo.png';
            } 
            ?>
            <img src="<?= $logo ?>" />
        </div>
                
        <div class="detail-title"  >
            BACKGROUND
        </div>
        <div class="detail-value" id="detail-info" >
            <?= $data->background ?>
        </div>
        <div style="width: 702px" >
            <div style="float: left; width: 402px ">
                <div class="detail-title"   >
                    ADDRESS
                </div>
                <div class="detail-value" style="width: 402px"  id="detail-spec" >
                    <?= $data->address ?>
                </div>
            </div>
            <div style="float: left; width: 300px ">
                <div class="detail-title"   >
                    <span style="min-width: 50px;display: inline-block" >CONTACT:</span> <span style="font-size: 10;font-weight: normal"><?= $data->contact ?></span>
                </div>
                <div class="detail-title"   >
                    <span style="min-width: 50px;display: inline-block" >TELP:</span> <span style="font-size: 10;font-weight: normal"><?= $data->phone ?></span>
                </div>
                <div class="detail-title"   >
                    <span style="min-width: 50px;display: inline-block" >E-MAIL:</span> <span style="font-size: 10;font-weight: normal"><?= $data->email ?></span>
                </div>
                <div class="detail-title"   >
                    <span style="min-width: 50px;display: inline-block" >WEBSITE:</span> <span style="font-size: 10;font-weight: normal"><?= $data->website ?></span>
                </div>
            </div>
        </div>