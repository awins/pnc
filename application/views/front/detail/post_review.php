<?
$status = @$tpl['status'];

if ($status == 200 ) { ?>
    <div class="review-message" style="color: blue" >
		Thank you, review sent successfully. <span id="call-review"> Click here to show review </span>
	</div>
	<?
	return;
} ?>

<div class="detail-title"  >
    POST REVIEW
</div>
<?
if ($status == 100 ) { ?>
    <div class="review-message" style="color: red">
		Send review failed. Please fill all the fields correctly
	</div>
	<?
} ?>

<div id="box-review" >
    <form id="form_review"  method="post">
    	<input type="hidden" name="post_review" value="1" />
    	<input type="hidden" name="rate" id="rate" value="0" />
    	<input type="hidden" name="recaptcha_match" value="11" />
	    <div style="vertical-align: top">
		    <label style="vertical-align: top;display: inline-block;width: 100px">Comment</label>
		    <div style="display: inline-block"> 
			    <textarea style="height: 100px;width: 300px" id="comment" name="comment"><? if ($status == 100) echo  set_value('comment')  ?></textarea>
	            <?= form_error('comment') ?>
	        </div>
		</div>
		<br />
		<div>
		    <label style="vertical-align: top;display: inline-block;width: 100px">Rate It</label>
		    <div id="r1" style="display: inline-block"> 
				<? 
				for ($i=1; $i <= 5; $i++) { ?>
		        	<div id="star_<?= $i ?>" class="rating_star empty"></div>  
		        	<?
				}  ?>
		    </div>  
		</div>
		<br />
		<div >
			<label style="vertical-align: top;display: inline-block;width: 100px">&nbsp;</label>
		    <div style="display: inline-block"> 

				<div id="recaptcha-box" style="display: inline-block">
					&nbsp;
				</div>
	            <?= form_error('recaptcha_response_field') ?> 
	            <?= form_error('recaptcha_match') ?>

				<div id="submit-review" >
					Submit Comment
				</div>
			</div>
		</div>
		<!-- <div>
			<label style="vertical-align: top;display: inline-block;width: 100px">&nbsp;</label>
			<div id="submit-review" >
				Submit Comment
			</div>
		</div> -->
	</form>

</div>